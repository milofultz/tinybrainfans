<!doctype html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="X-UA-Compatible" content="ie=edge"><meta name="description" content="Big-O notation shows the speed of an algorithm at a large scale in the worst-case scenario using fuzzy estimates."><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - Big-O Notation</title><link rel="stylesheet" href="tinystyle.css"><link rel="stylesheet" href="prism.css"></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">Big-O Notation</h1><p>Big-O notation shows the time and space complexity of an algorithm at a large scale in the <strong>worst-case scenario</strong>. If you are looking for an item in a list, then the Big-O notation will always assume the item is the last one in the list, and be how much time and space it takes to find that.</p>
<h2>Common Notations</h2>
<p>Big-O notation is usually represented as O(n), with mathematical formulas affecting the n inside. O represents the algorithm, n represents the number of elements. I'll be using time as my main vector here, but the same applies for space, too.</p>
<table>
<thead>
<tr>
<th>Time</th>
<th>Notation</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr>
<td>Constant</td>
<td><code>O(1)</code></td>
<td>This always takes the same length of time, regardless of size. The code does not depend on the size of the problem.</td>
</tr>
<tr>
<td>Logarithmic</td>
<td><code>O(log(n))</code></td>
<td>log(n) being the inverse of exponentiation. Reduces the problem in half each time through process</td>
</tr>
<tr>
<td>Linear</td>
<td><code>O(n)</code></td>
<td>This takes the same amount of time as there are elements in the list. Simple iterative or recursive programs.</td>
</tr>
<tr>
<td>Log-linear</td>
<td><code>O(n \* log(n))</code></td>
<td>Usually the result of a sort and an iteration.</td>
</tr>
<tr>
<td>Quadric</td>
<td><code>O(n^2)</code></td>
<td>Time goes up exponentially with the amount of elements.</td>
</tr>
<tr>
<td>Cubic</td>
<td><code>O(n^3)</code></td>
<td>Time goes up even more exponentially.</td>
</tr>
<tr>
<td>Exponential</td>
<td><code>O(b^n)</code></td>
<td>Time goes up an exponent per element. Nested loops or recursive calls.</td>
</tr>
<tr>
<td>Factorial</td>
<td><code>O(n!)</code></td>
<td>Time goes up by factorial.</td>
</tr>
</tbody></table><h2>Rules of Thumb</h2>
<h3>How complexity changes is easier to think about</h3>
<p>Instead of looking at one test case and saying &quot;in this case, there are X elements and it takes Y long, so the complexity is [...]&quot;, consider how the complexity <em>changes</em> over varying inputs. It is very difficult to understand what the complexity is without some greater context.</p>
<h3>Constants can always be removed from a Big-O notation</h3>
<p>Since Big-O only cares about the really biggest cases, as the numbers of elements in a list go up, even if there is a constant that is being added or multiplied, it won't affect the speed enough to matter. Exception being if the constant is of a very very large size, but even then, if the number of elements grows large enough.</p>
<p>You can also think of this as the idea that constants found with multiplication and division can always be reduced down to 1. If an algorithm is <code>n / 2</code> space, you could reduce this constant to <code>n / 1</code> or <code>n</code>; or if an algorithm is <code>4n</code> space, you can reduce this to <code>1n</code> or <code>n</code> space.</p>
<h3>Use the largest exponent</h3>
<p>If there is a formula that determines the length of time an algorithm will take, like <code>log(n)^3 + 15n^2 + 2n^3</code>, then Big-O will see the largest possible number or exponent (<code>n^3</code>) and use that: <code>O(n^3)</code>.</p>
<p>Big-O is 'rounding' to the nearest and simplest notation that is closest to the real outcome.</p>
<p>The only exception to this is the log-linear complexity. Since it is so common, it seems to stick around.</p>
<h3>Add or Multiply: Discerning the time per operation</h3>
<p>In general, it is best to go as deep as you can within loops and subprocesses to discern it's complexity before trying to find the complexity of the whole algorithm.</p>
<p>In a program, if you have a loop over your list that will operate <code>n</code> times, you would write this as <code>f(n) = n = O(n)</code>. If within that loop, you loop over the whole list again twice, we can call the complexity of this inner loop <code>O(n * 2)</code>. Combining this inner <code>O(2 * n)</code> complexity that is happening on each iteration within the outer <code>O(n)</code> complexity:</p>
<ul>
<li><code>f(n) = O(n) * O(2 * n)</code></li>
<li><code>O(n) * O(2 * n) = O(n * 2n)</code> - Since <code>2</code> is a constant, we can remove it</li>
<li><code>O(n * n) = O(n^2)</code></li>
</ul>
<h2>Time and Space</h2>
<p>When talking about Big O, these are the two complexities that are worried about: how long it will take to do the thing, and how much space is needed (always speaking in the worst-case scenario).</p>
<h2>Time</h2>
<p>The time complexity of an algorithm is determined by the number of and speed of each of the operations that occur within the algorithm.</p>
<p>For instance, if we had an algorithm that was meant to count the number of items in a list, we would have to always traverse the list one time. This would lead to a time complexity of <code>O(n)</code>, where <code>n</code> is the number of items in the list.</p>
<p>Let's say we had an algorithm that determined the sums of each value in a list paired with every other value in the list:</p>
<pre><code class="language-python">totals = []

for first in nums:
  for second in nums:
    totals.append(first + second)
</code></pre>
<p>The operations in this equation are as follows, with the time cost of the operation in parentheses:</p>
<ol>
<li>Iterate through nums (<code>n</code>)</li>
<li>Iterate through nums again (<code>n</code>)</li>
<li>Sum two numbers (<code>1</code>)</li>
<li>Add to a list (<code>1</code>)</li>
</ol>
<p>So this example will always have to traverse the list of <code>n</code> items <code>n</code> times, plus two constant operations. Since we see the a complexity greater than constant time (<code>n</code>), we can leave out the constant complexities (see the riles of thumb above). We're left with an <code>n * n</code> complexity, so this has a Big O of <code>O(n^2)</code>.</p>
<p>Another way to do this algorithm is:</p>
<pre><code class="language-python">totals = []

for i in range(len(nums)):
  for j in range(i, len(nums)):
    totals.append(nums[i] + nums[j])
</code></pre>
<p>Since we know we will have always added any numbers together that occur before <code>i</code>, we can focus only on the numbers ahead of and including the current index <code>i</code>. This is a much tighter complexity now, so let's go through the operations:</p>
<ol>
<li>Iterate through nums as <code>i</code> (<code>n</code>)</li>
<li>Iterate through nums from <code>i</code> to end (<code>n - i</code>)</li>
<li>Sum two numbers (<code>1</code>)</li>
<li>Add to a list (<code>1</code>)</li>
</ol>
<p>How do we notate the second step there? We know that the time needed for second iteration will diminish every pass of the <code>i</code> iteration step, since it is <code>n - i</code>, and since <code>i</code> is increasing by one every pass, we can reduce this to <code>n / 2</code>.</p>
<p>So like before, we are going to drop the constant operations (steps 3 and 4) since we see a more dominant complexity of <code>n</code>. We are left with <code>n * n/2</code> as the complexity. However, we <em>still</em> can drop the constants in this equation, namely the <code>/2</code> part; while it <em>does</em> make a difference in actuality, it doesn't make a difference in terms of how it is notated or talked about, since the difference on the final resulting time is not an important enough difference in scale of performance to matter.</p>
<p>So our final result is <code>n * n</code> or <code>O(n^2)</code>.</p>
<h3>Space</h3>
<p>The space complexity is determined by the memory needed to complete the algorithm (in the worst case, of course).</p>
<p>Let's start with an example of finding all positive non-zero duplicate integers in a list, where each number can only have one duplicate (e.g. <code>[1,2,3,3,4,5,5]</code>).</p>
<pre><code class="language-python">seen_numbers = set()
duplicates = []

for num in nums:
    if num in seen_numbers:
        duplicates.append(num)
    else:
        seen_numbers.add(num)
</code></pre>
<p>We're not worrying about time of operation here, but how much memory we need to complete the operations. Let's look at our two data structures and how much space they need, calling <code>n</code> the number of items in <code>nums</code> and <code>k</code> the duplicate numbers:</p>
<ol>
<li><code>seen_numbers</code>: This will hold <code>n - k</code> items</li>
<li><code>duplicates</code>: This will hold <code>k</code> items</li>
</ol>
<p>So our space is <code>(n - k) + k</code>, which resolves to <code>O(n)</code> space, since our space complexity will grow when the input list grows.</p>
<p>Another way we can do this same algorithm is as follows:</p>
<pre><code class="language-python">nums.sort()

duplicates = []

for i in range(len(nums) - 1):
    if nums[i] == nums[i + 1]:
        duplicates.append(nums[i])
</code></pre>
<p>Now we only have one data structure we've created, which holds <code>k</code> items. At worst, this could hold <code>n / 2</code> items, in the case that every number in the list had a duplicate. Since we can remove the constant from the equation, we are left with <code>n</code> space, as the size of the array will grow linearly with the size of the input.</p>
<p>One last way to do this:</p>
<pre><code class="language-python">nums.sort()

i = 0

while i &lt; len(nums) - 1:
    # Skip first duplicate number of the two
    if nums[i] == nums[i + 1]:
        i += 1
    # Remove every number except the duplicate
    nums.pop(i)

# Remove last element if it wasn&#x27;t a duplicate
if i &lt; len(nums):
    nums.pop(-1)
</code></pre>
<p>We don't create any new data structures in this, but we do create a variable <code>i</code>. This would make our space complexity a constant <code>O(1)</code>, since it doesn't scale at all with how big our input is..</p>
<h2>References:</h2>
<ol>
<li><a href="https://www.youtube.com/watch?v=zUUkiEllHG0&amp;list=PLDV1Zeh2NRsB6SWUrDFW2RmDotAfPbeHu&amp;index=3" target="_blank">https://www.youtube.com/watch?v=zUUkiEllHG0&amp;list=PLDV1Zeh2NRsB6SWUrDFW2RmDotAfPbeHu&amp;index=3</a></li>
<li><a href="https://en.wikipedia.org/wiki/Logarithm" target="_blank">https://en.wikipedia.org/wiki/Logarithm</a></li>
<li><a href="https://en.wikipedia.org/wiki/Natural_logarithm" target="_blank">https://en.wikipedia.org/wiki/Natural_logarithm</a></li>
<li>Intro to Programming and Computation: Chapters 9.1–9.3.1, 9.3.3, and 9.3.5</li>
<li><a href="https://www.youtube.com/watch?v=7lQXYl_L28w&amp;list=PLUl4u3cNGP63WbdFxL8giv4yhgdMGaZNA&amp;index=38&amp;t=0s" target="_blank">https://www.youtube.com/watch?v=7lQXYl_L28w&amp;list=PLUl4u3cNGP63WbdFxL8giv4yhgdMGaZNA&amp;index=38&amp;t=0s</a></li>
<li><a href="https://eli.li/2022/01/26/notes-on-big-o-notation" target="_blank">https://eli.li/2022/01/26/notes-on-big-o-notation</a></li>
<li><a href="https://www.crackingthecodinginterview.com/" target="_blank">https://www.crackingthecodinginterview.com/</a></li>
<li><a href="https://www.bigocheatsheet.com/" target="_blank">https://www.bigocheatsheet.com/</a></li>
</ol>
<section id="incoming"><details open><summary>Incoming Links</summary><ul><li><a href="heap.html">Heap</a></li></ul></details></section><p class="last-modified">Last modified: 202212070107</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>Built using <a href="http://codeberg.org/milofultz/swiki" rel="noopener">{{SWIKI}}</a></li><li><a href="http://codeberg.org/milofultz/" rel="noopener">Codeberg</a></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener">Mastodon</a></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a><a href="https://fediring.net/">Fediring</a><a href="https://fediring.net/random" style="color: red;text-decoration: green wavy underline;">&nbsp;!&nbsp;</a><a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
