---
title: Garlic
description: Garlic should be delicious and easy to work with.
---

## Peeling

To make peeling easier, crush the garlic clove slightly with the side of a knife (or a rock). Once the clove gives a little bit, the peel should easily come off.

## Garlic Rock[1]

If you are lazy like me, you can avoid mincing garlic or having a garlic press by using a rock. I found a palm-sized rock with a flat side while I was hiking one day, came home and washed it thoroughly, and now I use it to crush garlic whenever I need minced, peeled, or pressed garlic. I haven't washed mine in ages, but I'm sure you can wash it after use if you are worried about it.

I learned this from my cousin a while ago and I prefer this to other methods for a few reasons:

* it really pulverizes the garlic in a way that mincing doesn't
* it's very satisfying crushing something with a rock
* it's very fun

## References

1. https://tipnut.com/crush-garlic-rock/
