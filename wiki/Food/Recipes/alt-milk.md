---
title: Alt Milk
description: Alternative milks are easy, taste good, and are WAY cheaper than store bought.
---

## Ingredients

- Choice of grain and water from below
- 1 Tbsp **{{maple syrup}}** (or some other sweetener, like a single pitted date)
- Pinch of **{{salt}}**

### Oat

- 1 cup **{{rolled oats|oats}}**
- 3 cups **{{water}}**

Blend time: 45 seconds

### Rice

- 1 cup **{{uncooked white rice|rice}}**
- 2 cups (or enough to cover the rice) **{{water}}** for soaking
- 3 cups **{{water}}**

Blend time: 1 minute

#### Soak Methods

* Hot soak: 2 hours with hot not boiling **{{water}}**
* Cold soak: 14 hours with room temp **{{water}}**

## Cookware

- Blender
- Nut milk bag, towel, or fine strainer
- Mixing bowl for strained liquid
- Jar or bottle for storage

## Instructions

1. If not using oats, soak your **grains** in your **hot water** according to the instructions above. Drain the **soaking water** off the **grain** before using.
2. Put **all ingredients** in the blender (except your **soaking water** if used) and blend on highest setting for amount listed above.
3. Strain the pulp from the liquid in the strainer and set it aside. Two passes is recommended, but one was fine for me.
4. Store the resulting milk in your jar or bottle and refrigerate for at most a week.

## How To Use the Pulp

* as a mix of flour/grain and water for other recipes, like {{bread|no-knead bread}} or {{waffles}}
* for rice pudding (or similar)
* with spices to make crackers[6]
* dry out and make flour[5]

## References

1. https://minimalistbaker.com/make-oat-milk/
2. https://minimalistbaker.com/guide-making-dairy-free-milk/
3. https://minimalistbaker.com/make-rice-milk/
4. https://zerowastechef.com/2014/06/04/no-cook-rice-milk/
5. https://teddit.net/r/ZeroWaste/comments/9z6pe7/what_do_i_do_with_the_pulp_of_rice_milk/
6. https://donutfollowthecrowd.com/rice-pulp-crackers-vegan/

