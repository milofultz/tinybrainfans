---
title: No-Knead Bread
description: No-knead bread is probably the best balance of effort to taste you can have with bread.
---

No-knead bread is the best balance of taste and effort you can have with bread baking. Originally made to handle the hard water of Florida, this recipe is pretty foolproof and allows a lot of leeway for time, additions, etc.

The use of parchment paper is just to make things easier, but it is not really required. Check out the Jim Lahey recipe[2] for another method using towels. I personally don't like it because it's too easy to have really nasty towels, and once you go down that road, you might as well get a banneton and a couche.

## Ingredients

### The Dough

- 680g/3 cups **water**
- 1 Tbsp **granulated yeast**
- 18g/1 Tbsp **salt**
- 900g/6.5 cups **all-purpose flour**

### Making The Bread

- **Cornmeal**, if not using a dutch oven

## Cookware

### The Dough

- Mixing bowl or Cambro-type container that can fit in your fridge
- Cover for mixing bowl/container (plastic bag or lid to fit)
- Wooden kitchen tool handle (opt., I use the handle of my dough whisk)
- Refrigerator

### Making The Bread

- Oven
- Dutch oven, baking stone, or cookie sheet
- Kitchen scale (opt.)
- Kitchen shears (opt.)
- Butcher's block or board for shaping the bread
- Parchment paper if you don't want to use cornmeal
- Serrated knife (opt.)
- *If not using dutch oven:*
  - Pizza peel for resting bread and putting bread in oven (opt.)
  - Spray bottle with water or broiler tray
- Cooling rack, or somewhere the bread can cool and breathe after baking

## Instructions

### The Dough

1. Add **water** to the container, and then add the **yeast**. Mix for a bit until distributed.
1. Add **flour** to the container and put the **salt** on top of the flour mound. Use your whisk or your hands to mix the salt into the flour.
1. With the wooden tool handle, do an initial mix of the ingredients until mostly incorporated. Once it gets kind of difficult to mix, mix with your hands until the flour is fully incorporated. It should be a wet rough dough. *During this process, make sure you check the dough for flour pockets, where the flour is still dry and unincorporated. I tend to do one last pass with my hands, digging in and pinching everywhere to see if I missed any.*
1. Cover your container with a lid or enclose the opening within a plastic bag. You don't want to closure to be airtight because gases will be released during fermentation. Leaving a corner up on the lid or just using a loose plastic bag is fine.
1. Let the dough sit for 2 hours, or until the dough flattens on top.

At this point, you can put the dough in your refrigerator for use later. I've had this dough last for up to a week or two.

### Making The Bread

1. Start preheating your oven to 450 degrees Fahrenheit. Add your dutch oven, baking stone, or cookie sheet now. If not using a dutch oven, add the broiler tray.
1. Pull the dough out of the refrigerator and place on a kitchen scale, if you want a specific weight of dough. Tare the scale.
1. Dust the top of the dough with some flour so your hands don't stick to the dough, and then pull a piece out. Either with your shears or just a strong pinch, get a pound of dough from your container. *Your scale should say -1 pound at this point.*
1. Shape the bread. Either on the butcher's block/board or just in your hand, create a "gluten cloak". This is very hard to describe, so it's worth checking out this video for shaping without a board[4] or other dough shaping videos for with a board[5].
1. Once it is shaped, put your shaped dough on parchment paper (if not using a dutch oven, you can also use a light layer of **cornmeal** on top of your pizza peel). The dough should rest for 40-90 minutes; longer will be better.
1. Right before putting the loaf in the oven, score your loaf to allow gas expansion during baking. With the knife, you can do many patterns of 1/4" deep cuts: a single line, an X, parallel lines, a fan of lines, a box, whatever. If you don't have a knife, just push your finger down lightly on the top of the center of the loaf until you reach the bottom (you don't want to create a hole).
1. Put the bread in the oven.
    * If using a dutch oven, lift your bread using the sides of the parchment paper and drop it into your dutch oven, putting the lid on afterwards.
    * Otherwise, put dough into the oven on top of your stone or cookie sheet. If you have a spray bottle, spray the side of your oven around 10-15 times to create a good amount of steam; with the broiler tray, add a cup of hot water. Close the oven as quickly as possible to keep the steam in.
1. Let bake for 30-40 minutes. Remove the parchment after 20-25 minutes or so. You want the bread to turn a blonde to walnut color. *Generally aim for more dark than you think, the oven light will lie to you.*
1. Turn off the oven, remove the bread, and allow it to cool on the rack for at least an hour. It's still cooking at this point!
1. At this point you are done. When storing the bread, store it in a plastic bag with the cut side facing the counter. This will ensure the bread doesn't turn rock hard over time.

## References

1. https://artisanbreadinfive.com/2013/10/22/the-new-artisan-bread-in-five-minutes-a-day-is-launched-back-to-basics-updated/
2. https://cooking.nytimes.com/recipes/11376-no-knead-bread
3. https://cooking.nytimes.com/recipes/1022147-updated-no-knead-bread
4. https://yewtu.be/watch?v=bIb8fC9BdWs&t=125s
5. https://yewtu.be/watch?v=hWXA8xFYu9A
6. [All-Rye No-Knead](https://yewtu.be/watch?v=cQxUtpAItFM)

