---
title: Muesli
description: Muesli is a great simple breakfast food.
---

Muesli is traditionally "a cold oatmeal dish based on rolled oats and ingredients such as grains, nuts, seeds and fresh or dried fruits"[1]. My own version of this is about as stripped down, quick, and simple as you can get, but the real recipe is "add things in whatever quantities you want and then pour some kind of alt milk on it".

## Ingredients

This is for one bowl. Could be scaled, but I like my even portions per bowl. Not enough time saved by batching anyway.

- 2/3 cup **{{rolled oats|oats}}**
- 1-2 tablespoons **{{raisins}}**
- 1 teaspoon **{{brown sugar}}**
- **{{milk|Alt milk}}**, enough to almost cover the evenly distributed dry ingredients

### Substitutions

This is a very DIY do-whatever-you-want recipe. Similar to granola, just try adding different nuts or subbing out sweeteners and see what happens! You can't lose here.

## Cookware

- Bowl
- Spoon

## Instructions

1. Mix dry **oats**, **raisins**, and **brown sugar** into a bowl.
2. Pour **alt milk** over dry ingredients until satisfied.

## References

1. https://en.wikipedia.org/wiki/Muesli

