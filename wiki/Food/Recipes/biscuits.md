---
title: Biscuits
description: Biscuits!
---

This is a rip of the Minimalist Baker recipe[1], with the modifications I like.

## Ingredients

- 1 cup **{{alt milk}}**
- 1 Tbsp **{{vinegar}}**
- 250g **white {{flour}}**
- Some **{{flour}}** for dusting
- 1 Tbsp **{{baking powder}}**
- 1/2 tsp **{{baking soda}}**
- 3/4 tsp **{{salt}}**
- 4-8 Tbsp **alt {{butter}}, chilled/frozen** (Earth Balance, or something like that)
- 2 Tbsp **{{olive oil}}**

## Cookware

- Refrigerator/freezer
- Measuring cups/spoons
- Glass vessel for mixing wet ingredients
- Mixing bowl or Cambro-type container that can fit in your fridge
- Cheese grater
- Cutting board or wood/Boos block
- Rolling pin/wine bottle
- Bench scraper/knife
- Cookie sheet and non-stick sheet
- Brush for oil
- Oven

## Instructions

1. Preheat oven to 450°.
1. Mix **alt milk** and **vinegar** to make a pseudo-buttermilk. Set aside.
1. Mix and whisk **all dry ingredients**.
1. Grate your **chilled butter** into little chunks on top of your **dry ingredients**. Once it is all grated, mix together with your hands until it is fully combined and as small of pieces as possible. Do it as quickly as you can, so that the **butter** doesn't melt, think pie crust making.
1. Make a well and pour in the **"buttermilk"** *only 1/4 cup at a time* (you may not need it all, especially if you have upped the amount of **butter**) and mix it until just combined. The goal we want is "slightly tacky but moldable dough"[1].
1. Turn the dough onto your floured cutting board/wood block. Dust the top with some flour and then fold the dough over itself 5-6 times.
1. Roll the dough out into a rectangle-ish shape with your rolling pin/wine bottle. Use your knife or bench scraper to try and make it close to a rectangle at the end so you have even sized biscuits.
1. Cut the dough into 8 equal sized biscuits and place *right* next to each other on the cookie sheet. Push your fingers into the top of the biscuits lightly (to help manage the oven spring), and then brush some **olive oil** on top.
1. Put in the oven for 10-15 minutes or until golden brown.
1. Eat with some mushroom gravy[2].

## References

1. https://minimalistbaker.com/the-best-damn-vegan-biscuits/
2. https://minimalistbaker.com/vegan-biscuits-and-gravy/

