---
title: Toum
description: Toum is a naturally vegan garlic aioli/mayonnaise.
---

Toum is a naturally vegan garlic aioli/mayonnaise.

## Ingredients

- 1/2 cup / ~24 medium sized cloves **{{garlic}}**, peeled
- 1 tsp **{{salt}}**
- 1/4 cup **{{lemon juice}}** or **{{vinegar}}** or **{{kombucha}}** (I'll call this the **tart liquid**)
- 1 1/2 cup **{{olive oil}}**

## Cookware

- Blender
- Measuring cups/spoons
- Glass vessel for pouring wet ingredients
- Rubber spatula

## Instructions

1. Add **garlic** and **salt** to blender. Pulse/blend as fine as possible.
1. Scrape down the sides and repeat the above step as needed. (Our blender couldn't get it too fine because it would stick to the side, so we repeated a few times).
1. Add a teaspoon of **tart liquid** and pulse until you have a paste (same advice as before, you may have to scrape a bit; no need to go too crazy though).
1. Start your blender at a low speed. While running, alternate adding a little **olive oil** and a little **tart liquid** very slowly. Stop blender and scrape sides as needed until you have enough liquid to keep it going and not throw up on the sides. *Go slowly!*
1. Repeat the previous step until all wet ingredients are gone. Then you are done *Remember, go slow!*

## References

1. [Original recipe](https://www.cheftariq.com/recipe/lebanese-garlic-sauce/)

