---
title: Waffles
description: Sourdough, yeast, and baking soda waffles.
---

## Ingredients

* One of the below variations of flour, water, and rising agent
* 100g **soy/alt milk** (add enough to make it a batter)
* 1.5 tsp **vinegar** (opt. if using sourdough)
* 10g **olive oil**
* 1/2 tsp **salt**
* 1 Tbsp **cornmeal** (opt.)

### 100% Hydration Sourdough Discard

This is essentially the recipe from Zero Waste Chef[1] for pancakes, but it works well for waffles and is totally vegan, too, which is nice.

* 230g/1 cup 100% hydration **{{sourdough}} discard** (usually fairly spent)
* 1/8 tsp **baking soda**

### 60% Hydration Rye Sourdough Discard

* 24g 60% hydration **{{sourdough}} discard**
* 108g **{{water}}**
* 68g **bread flour**
* 40g **whole wheat flour**

### Instant Yeast

* 1 tsp **instant {{yeast}}**
* 120g **{{water}}**
* 80g **bread flour**
* 40g **whole wheat flour**

### Baking Soda

* 1/2-1 tsp **baking soda**
* 120g **{{water}}**
* 80g **bread flour**
* 40g **whole wheat flour**

## Cookware

* Mixing bowl
* Dough whisk (opt.)
* Spatula
* Waffle iron
* 1/4 cup measuring cup, or something around that for scooping batter into the iron

## Instructions

1. If using **vinegar**, mix **vinegar** and **soy milk** and let stand for 5 minutes or so until it "curdles" similar to buttermilk[2].
1. If using the **stiff starter** or **standard yeast**, mix everything together and let it rest for 8 hours/overnight. Otherwise, mix ingredients and use immediately.
2. Plugin and heat up the waffle iron.
3. Add 1/4 cup of batter at a time and let it cook. Repeat until batter is gone.

## References

1. https://zerowastechef.com/2016/04/01/sourdough-pancakes/
2. https://www.thespruceeats.com/make-your-own-buttermilk-p2-995500

