---
title: Quick Pickles
description: Quick pickles are a great way to preserve food and make it taste better.
---

Pretty much any vegetable will work for a quick pickle, but my favorites are:

* Red onion
* Carrots
* Garlic
* Pepperoncinis
* Beets
* Hard boiled eggs

Spices and additions for this are also a bit choose-your-own-adventure, but my favorites are:

* Chili flakes
* Garlic
* Mustard seed
* Peppercorns
* Dill

## Ingredients

*NOTE: All of these should be adjusted to taste. This is a very generic recipe and I highly recommend searching around to get an idea for how people do it for certain foods or types of veggies.*

- **Food to pickle**, enough to pack your jar fully with your **spices** and **additions**
    - For most vegetables, chopped in to smallish pieces so the pickling liquid can get all the way in there
    - For beets and other more dense vegetables, prepare them by boiling until tender, and then chop into pieces
    - For anything else, do some research and see what other people do.
- **Spices** and **additions** (for amounts, look up recipes, there are a zillion variations, but you can also just eyeball)
- Equal parts **{{vinegar}}** and **{{water}}**, combined (stronger flavors should move towards full vinegar; use whatever vinegar you think will be best; water should be unchlorinated, so let rest on the counter for an hour or so to dechlorinate)
- 3-4% of food's weight in **{{salt}}** (adjust to taste, check other recipes)
- 3-4% of food's weight in **{{sugar}}** (again, adjust to taste)
- Some pickled cucumber recipes call for ~1 tsp of **loose-leaf {{black tea}}** to aid the crunch, but I've never done it

## Cookware

- Pot for boiling
- Optional: large tempered or heat-safe bowl, if wanting a less crunchy pickle
- Mason-style jar for storing, cleaned and sanitized
- Refrigerator, if wanting long-term storage

## Instructions

1. Pack and evenly distribute the **food to pickle** and **spices** and **additions** in the jars, leaving a small amount of room at the top. If you want the end result less crunchy, add only the **spices** and **additions** and hold the **food to pickle** for later.
1. Add **liquid ingredients**, **salt**, and **sugar** to pot, and stir until dissolved. Bring to boil.
1. Optional: if you wanted a less crunchy pickle, add them to the brine now and boil them for a couple minutes. Note that this makes packing quite a bit more laborious
1. Pour the brine over the packed jars, leaving about a half inch of room at the top. You may not use all the brine.
1. Remove the air bubbles by tapping the bottom of the jars against a counter or table. Leaving these may result in a more quickly spoiled pickle.
1. Seal the jars and let them cool. Once cooled, you can place them in the fridge for better flavor over time, or start chowing down.
1. Once they have been opened, they have a higher chance of spoiling, so be sure to dig in once you start. Store them out of sunlight, as this can grow bad bacteria.

## References

1. http://seriouscrust.com/pickled-green-beans/
2. https://www.budgetbytes.com/pickled-red-onions/
3. https://ccesaratoga.org/resources/preserving-without-sugar-or-salt
4. https://minimalistbaker.com/quick-pickled-carrots/
5. https://zerowastechef.com/2016/08/17/garlic-dill-pickles/
6. https://www.thekitchn.com/how-to-quick-pickle-any-vegetable-233882
7. https://grimgrains.com/site/lactofermentation.html
8. Beets
    1. https://www.spendwithpennies.com/quick-pickled-beets/
    2. https://www.afarmgirlsdabbles.com/quick-pickled-beets/
9. https://pickles.fandom.com/wiki/Special:AllPages

