---
title: Kvass
description: Bread flavored soda. You can reuse your stale bread and some sourdough starter, too!
---

Kvass is like a bread kombucha and is super refreshing. The darker you get the bread and the more molasses is used instead of sugar, the more beer-like your kvass will end up. I have subbed sugar for molasses and it still works out, it's a pretty foolproof recipe in terms of getting something tasty. This recipe can be scaled up or down as necessary.

## Ingredients

- 7 liters **{{water}}**
- 200g **{{bread}}** (darker the better, like rye)
- 460g **{{molasses}}** (blackstrap if you can get it)
- 30g **{{honey}}**
- 10g **{{yeast}}** or 100g **{{sourdough starter}}**
- 600g **{{sugar}}**
- Some **{{raisins}}**, **{{ginger}}** or other flavorings for the second fermentation (opt.)

## Cookware

- Oven, range and pan, or grill that can be outside (outside part is optional but highly recommended)
- Range
- Large pot
- Stirring spoon
- Fermentation vessel
- Airlock (or some other mechanism for allowing carbon dioxide to escape for first fermentation, I just burp the vessel every once in a while)
- Colander/strainer
- Secondary fermentation vessels (opt., preferably plastic but I've used glass kombucha bottles)

## Instructions

1. Toast the **bread** until it is *really* dark, like burnt dark. I toasted my bread twice at the highest setting and it was very burnt around the sides. The darker you go, the more dark in color and flavor your kvass will be.
1. Put around a quarter of your **water** on heat and dissolve **honey** and **sugar**. You don't want *all* your water hot, just this little bit.
1. Add the rest of your **water** and **molasses** to the mixture and stir it in.
1. Place the mixture into your primary fermentation vessel and add the **toasted bread** and **yeast/sourdough starter**.
1. Let it sit out of direct sunlight. Fermentation should start beginning after around 24 hours.
1. Let it ferment for around four days, give or take depending on taste and how sweet you want it.
1. Once to your liking, strain the bread from your kvass. Do this slowly, as you may also have sediment at the bottom and we want to keep that out, as well.
1. At this point, you can drink it.
1. If you want to do a secondary fermentation for carbonation and/or flavoring, bottle it in your secondary fermentation vessels with your **added flavorings**. If doing this, be sure to take care that you are burping your vessels often if using glass, or monitor your plastic containers for excess fermentation. When you're happy with the carbonation, refrigerate them.

## References

1. [Traditional Homemade Bread Kvass - PantsDownApronsOn](https://www.pantsdownapronson.com/bread-kvass-recipe/)

