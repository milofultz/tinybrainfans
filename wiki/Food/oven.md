---
title: Oven
description:
---

Most ovens are not calibrated well. If you need some specific temperatures for stuff like {{bread}}, consider getting an oven thermometer and see how accurate your oven is. Some ovens you can adjust[2], as well.

## Use The Preheat and Cooldown

> roasted veg and the like are for preheating, since you can over shoot a bit and let them hang out. Crackers are for cool down, since they tend to want a burst of heat and then not much else. I usually made the crackers with left over dough or castings from a {{sourdough}} starter + lots of seeds and spices and some oil. Role thin. Then role a bit thinner.[1]

## References

1. https://dog.estate/@eli_oat/108697153957992801
2. https://www.familyhandyman.com/project/how-to-adjust-oven-temperatures/

