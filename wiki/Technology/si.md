---
title: International System of Units (Metric System)
description:
---

| Prefix name | Prefix symbol | Value                                    |
|-------------|---------------|------------------------------------------|
| giga-       | G             | 109  1,000,000,000  one billion          |
| mega-       | M             | 106  1,000,000  one million              |
| kilo-       | k             | 103  1,000  one thousand                 |
| (none)      | (none)        | 100  1  one                              |
| centi-      | c             | 10−2  .01  one one-hundredth             |
| milli-      | m             | 10−3  .001  one one-thousandth           |
| micro-      | µ             | 10−6  .000001  one one-millionth         |
| nano-       | n             | 10−9  .000000001  one one-billionth      |
| pico-       | p             | 10−12  .000000000001  one one-trillionth |

## References

1. https://en.wikipedia.org/wiki/SI

