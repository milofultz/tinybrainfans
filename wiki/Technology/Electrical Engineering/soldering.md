---
title: Soldering
description: Soldering is the act of connecting different components through heating conductive material and fusing the components together with it.
---

## Solder Types

Using rosin-core solder is recommended for most electrical applications. There are many other types of solder, including plumbing solder, acid-core solder, and others. Use only the solder that is recommended for your specific application.

## References

1. https://www.hamradiolicenseexam.com/

