---
title: Electromagnetic Energy
description: Electromagnetic energy travels at the speed of light and includes radio, microwaves, light, x-rays, and gamma rays.
---

Electromagnetic energy travels at the speed of light and includes radio, microwaves, light, x-rays, and gamma rays.

The formula to determine the wavelength of a given frequency is `c = ƒλ`, where `c` is the speed of light (300m meters/second), `ƒ` is the frequency in hertz, and `λ` is the wavelength in meters. To find the wavelength, invert this formula to `λ = c/ƒ`.

## Polarization

n electromagnetic wave consists of an oscillating *electric field* and an oscillating *magnetic field, operating at right angles to one another. These move through space at the speed of light. The polarization of a wave is based on the orientation of it's electric field:

* Linearly polarized waves (`=`) do not change the orientation of the electric field;
* Circularly polarized or elliptically polarized waves (`+`) have a rotating electric field;
* Horizontally polarized waves (`┴`) have an electrical field that is parallel to the earth's surface;
* Vertically polarized waves (`=` with thicker bottom line) have an electrical field that is perpendicular to the earth's surface.

This orientation is established by the transmitting {{antenna's|antennas}} orientation. For instance, horizontal antennas will produce horizontal waves. However, this can change over time as it interacts with the atmosphere, ground, and various objects like mountains or buildings.

### Local vs. Distant Communications

Local communications via {{amateur radio|Ham Radio}} are best served with vertically polarized waves because there is less attenuation when sent along the surface of the Earth.

Distant communications use horizontally polarized waves since they are less attenuated when traveling due to ground and tropospheric reflections.

