---
title: Federal Communications Commission
description: The FCC is the agency that regulates and enforces rules for amateur radio in the United States.
---

Part 97 of the Code of Federal Regulations[1] states that the purpose of this part is to ensure they facilitate the

> [e]ncouragement and improvement of the amateur service through rules which provide for advancing skills in both the communication and technical phases of the art.

## References

1. https://www.ecfr.gov/current/title-47/chapter-I/subchapter-D/part-97?toc=1

