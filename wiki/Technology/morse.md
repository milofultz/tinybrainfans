---
title: Morse Code
description: Morse code is a language that is traditionally transmitted via radio frequencies.
---

Morse code is a language that is traditionally transmitted via radio frequencies and consisting of a series of short and long signals, known as dots (`•`) and dashes (`-`). It is usually sent as an {{RF carrier wave|HAM Radio}} (or continuous wave, hence it's other name CW), and thus needs a {{HAM radio}} or some other kind of transmitter, along with some kind of keyer.

## Common Codes/Alphabet

An alternative way to view these is in a binary tree[5], if that helps.

| Character | Morse Code |
|-----------|------------|
| A         | `•-`       |
| B         | `-•••`     |
| C         | `-•-•`     |
| D         | `-••`      |
| E         | `•`        |
| F         | `••-•`     |
| G         | `--•`      |
| H         | `••••`     |
| I         | `••`       |
| J         | `•---`     |
| K         | `-•-`      |
| L         | `•-••`     |
| M         | `--`       |
| N         | `-•`       |
| O         | `---`      |
| P         | `•--•`     |
| Q         | `--•-`     |
| R         | `•-•`      |
| S         | `•••`      |
| T         | `-`        |
| U         | `••-`      |
| V         | `•••-`     |
| W         | `•--`      |
| X         | `-••-`     |
| Y         | `-•--`     |
| Z         | `--••`     |
| 1         | `•----`    |
| 2         | `••---`    |
| 3         | `•••--`    |
| 4         | `••••-`    |
| 5         | `•••••`    |
| 6         | `-••••`    |
| 7         | `--•••`    |
| 8         | `---••`    |
| 9         | `----•`    |
| 0         | `-----`    |
| .         | `•-•-•-`   |
| ,         | `--••--`   |
| '         | `•----•`   |
| "         | `•-••-•`   |
| _         | `••--•-`   |
| :         | `---•••`   |
| ;         | `-•-•-•`   |
| ?         | `••--••`   |
| !         | `-•-•--`   |
| -         | `-••••-`   |
| +         | `•-•-•`    |
| /         | `-••-•`    |
| (         | `-•--•`    |
| )         | `-•--•-`   |
| =         | `-•••-`    |
| @         | `•--•-•`   |

## References

1. https://en.wikipedia.org/wiki/Morse_code
2. http://g4fon.net/
3. http://dxatlas.com/morserunner/
4. https://calculla.com/morse_codes
5. https://en.wikipedia.org/wiki/Morse_code#Alternative_display_of_common_characters_in_International_Morse_code

