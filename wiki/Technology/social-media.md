---
title: Alternative Website Frontends
description: These sites exist as ad- and tracker-free proxies for websites or complete alternatives.
---

## Why Walk Away From Social Media?

> Imagine attending a cocktail party but you do not choose which people you hear. The people you hear are the ones who the algorithm decides should be heard. When you speak you do not know who will hear you.
>
> You always have to wonder who built the algorithms and how voices are being valued. And if, perhaps, you are mostly hearing the people who stir up the most emotion.
>
> Time and time again we are, within these systems, being experimented on without our consent as companies test and tweak their algorithm continuously. It's probably wise to remember that keeping people docile, or at war with each other rather than the oppressor, is straight from an authoritarian playbook.[17]

## Alternatives

### Mastodon/Misskey/Pleroma[11-13]

These are all alternatives to Twitter, Facebook, etc., where you have a profile and write out statuses to connect with others. Instead of one large space, there are smaller instances or communities you can join. This allows better communication, friendships, and moderation for everyone involved. There is also an absence of algorithmically driven conversation, as it purely operates on feeds of posts from your followers with nothing else.

It can feel weird because it operates differently than conventional social media you are used to. You can get questions answered at the wiki[14].

### Lemmy[15]

Lemmy is an alternative to Reddit for sharing links.

## Ad- and Tracker-Free Proxies

These are all ways to get the same content you would normally see on any of these platforms but without the Javascript bloat of trackers and advertisements. This usually reults in *much* faster load times, much lower bandwidth, and less data tracking for gross companies.

You can automate a lot of these switches by using an extension to automate it for you[9], or something like Redirector for Firefox[8] to automatically replace portions of the URL, like `reddit.com` with `teddit.org`, etc.

### Youtube

- Invidious[1]
  - Yewtu.be (No video proxy) [3]
  - piped.kavin.rocks (Video proxy) [2]

### Reddit

- Teddit[4]

### Twitter

- Nitter[5]

### Imgur

- Imgin[18]
- Rimgo[19]

### Medium

- Scribe[7]

### Google

- DuckDuckGo (uses Bing for search, but is better than Bing)[10]

### Google Fonts

- Fontshare[22]

### Google Translate

- SimplyTranslate[21]

## References

1. https://invidious.io/
2. https://piped.kavin.rocks
3. https://yewtu.be/
4. https://codeberg.org/teddit/teddit#instances
5. https://github.com/zedeus/nitter
6. https://www.funkyspacemonkey.com/foss-front-ends-and-alternatives-for-twitter-instagram-reddit-youtube-and-more
7. https://sr.ht/~edwardloveall/scribe/
8. https://addons.mozilla.org/en-US/firefox/addon/redirector/
9. https://github.com/SimonBrazell/privacy-redirect
10. https://duckduckgo.com/
11. https://joinmastodon.org/
12. https://github.com/misskey-dev/misskey
13. https://git.pleroma.social/pleroma/pleroma
14. https://joinfediverse.wiki/Main_Page
15. https://join-lemmy.org/
16. https://p.xuv.be/fronting-social-surveillance
17. https://axbom.com/why-i-left-algorithm-based-social-media/
18. https://imgin.voidnet.tech/
19. https://codeberg.org/video-prize-ranch/rimgo
20. https://farside.link/
21. https://simple-web.org/projects/simplytranslate.html
22. https://www.fontshare.com/
