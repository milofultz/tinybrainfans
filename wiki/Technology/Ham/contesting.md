---
title: Contesting (Ham)
description: Contesting is an activity for hams to exchange information with lots of stations over a period of time.
---

Contesting is an activity for {{hams|Ham Radio}} where the goal is to score points by exchanging information with as many different operators as you can over a given period of time. It's fun, but also useful for emergency training scenarios where operating conditions are suboptimal.

It is generally best practice to send only the minimum information needed for proper identification and the contest exchange.

## ARRL Field Day

On the fourth full weekend in June, ARRL holds a contest where clubs and individuals set up temporary operations in the field across the United States for testing out emergency communications. This is a good way for beginner hams to get practice on their radios and get a deeper understanding of the infrastructure, as well as meetin new people.

## References

1. http://www.arrl.org/field-day

