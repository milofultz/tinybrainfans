---
title: HAM Radio
description: "HAM radio is the use of radio by normal people: for recreation, communication, or in emergencies."
---

HAM radio is the use of radio by normal people: for recreation, communication, or in emergencies.

## Radio Safety

Radio frequencies are non-ionizing radiation, so cannot change DNA or anything. However, the human body does absorb them and it does cause minor heating. We absorb radio waves more readily in the VHF range (30 MHz - 300 MHz).

When evaluating exposure, consider the duty cycle, or the percentage of time operating vs. not operating. If you are sending messages 50% of the time, your duty cycle is 50%. The FCC regulation on exposure limits is an average over a given amount of time, so certain modes lend themselves better than others.

You should be routinely evaluating your RF environment for your own records (and in case the {{FCC|Federal Communications Commission}} swings by). This can be done by performing calculations based on the [FCC OET Bulletin 65](https://www.fcc.gov/general/oet-bulletins-line#65), performing calculations based on computer modeling, or measuring field strength using calibrated equipment.

### Reducing Exposure

Exposure can be reduced by moving the antenna or moving/removing the amplifier.

### Station Ground[22]

There are three good reasons for grounding your ham shack:

* Safety ground protects you from hazardous voltage spikes;
* Lightning protection ground;
* RF ground will make some antennas perform better.

To achieve proper grounding, you should always use three-prong connectors, disconnect outdoor antennas when not in use, and use ground rods to properly ground for lightning strikes.

For improving antenna performance, all grounds should be connected together via a metal plate which is then bonded to Earth ground rods with heavy wire. Not doing this can result in ground loops which results in stray RF energy entering in your signal (usually a "hum"). The best type of conductor for this is a wide flat copper strap.

#### Lightning

* Ensure all connections are short and direct.
* Avoid sharp bends.
* Make connections using steel bolts, not solder, to better withstand the heat of a lightning strike.

## License

After passing the exam[8], the {{FCC|Federal Communications Commission}} will grant you an operator/primary station license. Anyone or any age can take the exam and be granted a license, except for representatives of a foreign government. As long as your name shows up on the ULS database[9], you are free to participate.

The license is valid for 10 years. If you forget to renew, you may be asked to retake the test.

If you are unable to be reached by email at any point after passing your exam, your license can be revoked or suspended.

## Station Identification

You must provide station identification by transmitting your callsign at least once every 10 minutes and at the end of each communication. When doing so, you can always use {{Morse code}}, or the way you normally communication in the mode you are currently using.

When using voice to communicate, you are encouraged to use the NATO phonetic alphabet[10], though many use alternate words. As long as you say it in English, and it's clear what it means, you have met the legal requirement.

When using another callsign in a message, always start with the other callsign first.

The rules **always** apply.

### Callsigns

Every country enforces specific formats for callsigns. The US formats always start with W, K, N, or AA through AL, consist of one or two letters followed by a digit and then one to three additional letters. They are issued sequentially, but you can opt for a "vanity" callsign if desired.

Technician and General class operators can only use 1x3 and 2x3 calls.

#### Amateur Radio Club

An amateur radio club with at least four members can apply for their own callsign. The trustee is the person who contacts the FCC regarding the club. When members use the club station, they use the club's callsign.

The ARRL, or American Radio Relay League, is the United States's national association for clubs. Not required, but offer some cool stuff[11].

### Self-assigned Indicators

The indicators come after a callsign, e.g. `W21C/P`. These used to be required by the FCC, but are not any longer.

When using a phone or voice medium, "slash", "slant", and "stroke" are all appropriate subs for `/`.

| Indicator | Meaning                                      |
|-----------|----------------------------------------------|
| `/P`      | portable (e.g., a handheld radio).           |
| `/M`      | mobile operation (e.g., in a car).           |
| `/MM`     | maritime mobile operation (e.g., in a boat). |
| `/R`      | a repeater.                                  |
| `/B`      | a beacon.                                    |

## Operating Another's Station

The privileges you can use are dependent on *your* license, not those who own the station.

The callsign is based on the *station license*. You must use *their* callsign when using their equipment.

If you are a higher class operator than the station you are using, you must append your callsign to the end of the station's, preceded by a slash (e.g. if operator `W1AI` is operating `W2EC`, then they would send their callsign as `W2EC/W1AI`).

The operator and the station licensee are held responsible for the proper opertion of the radio.

### Third Party Communications[13]

A third party communication is a message sent between two amateur stations on behalf of a third party. Any message, formal or otherwise, counts as a third party communication. Also included in this distinction is a non-ham operator operating your equipment.

Third party communications are not allowed, except for in these specific situations:

* Emergency or disaster relief
* Both amateur stations are in the same country
* Third party is also a currently licensed ham operator
* The message is passed between two countries where a "third party agreement" is in place[12]

These rules apply in all modes and on all amateur bands.

## Prohibited Transmissions[15]

Generally, you are prohibited from any of the following:

* Interfere with another radio station.

To avoid interference, first make sure you are authorized to transmit on the selected frequency, then listen to be sure that no one else is using the frequency, and then ask if the frequency is in use.  If a conversation in progress, someone will let you know.  Remember, you may not be able to hear all sides of a conversation.

The amateur radio service is allocated certain frequency ranges on a secondary basis, which means we must accept harmful interference from, and must not cause harmful interference to, stations in other (non-amateur) services which have access to those frequencies on a primary basis.

* Transmit false or deceptive signals.
* Transmit out of band.
* Transmit unidentified signals.
* Communicate with other countries when prohibited by either government.
* Communicate with non-amateur stations.
* Communicate to facilitate a criminal act.
* Transmit coded messages (messages encoded for the purpose of obscuring their meaning).
* Use indecent or obscene language.
* Receive compensation for transmitting.
* Broadcast for the benefit of the general public.
* Transmit music.
* Make one-way transmissions; exceptions include the following:
    * Brief transmissions necessary to make adjustments to the station.
    * Brief transmissions necessary to establishing two-way communications with other stations.
    * Transmissions necessary to providing emergency communications.
    * Information bulletins and Morse Code practice.
    * Beacon, auxiliary, and space stations.
    * Telemetry (transmission of measurements by radio, like a weather balloon sending data back to Earth).
    * Telecommand (transmission of commands to control something, like a radio-controlled aircraft or a satellite).
* Retransmit signals (except for repeater stations, auxiliary stations, and space stations).

Each station licensee and each control operator must cooperate in selecting transmitting channels and in making the most effective use of the amateur service frequencies.  No frequency will be assigned for the exclusive use of any station.

At all times and on all frequencies, each control operator must give priority to stations providing emergency communications.

## Emergency Communications

In an emergency, when normal means of communication are not available, the rules allow you to do *whatever it takes* to obtain or provide assistance in connection with the immediate safety of human life and protection of property. Emergency communications has top priority at all times and on all frequencies. If you hear a station in distress, you should acknowledge them, and offer any assistance you can.

These are the two major emergency communication organizations:

* [Radio Amateur Civil Emergency Service (RACES)](https://www.qsl.net/races/) consists of organizations administered by state or local governments, which use amateur radio operators, stations, and frequencies for emergency management or civil defense communications.
* [Amateur Radio Emergency Service (ARES)](http://www.arrl.org/ares) is an organization administered by the American Radio Relay League (ARRL) in the U.S., and Radio Amateurs of Canada (RAC) in Canada, that uses licensed amateur radio operators who volunteer their time and equipment to provide public service and emergency communications.

## Control Types

* Local control
* Remote control: remotely transmitting or receiving message through a remote station via internet of radio link;
* Automatic control: devices that allow usage without an operator on premises according to FCC rules (e.g. a repeater).

Telecommand is a transmission of commands to control something, like an RC car or satellite. If using ham frequencies to do this, you don't need to transmit your callsign, but it along with your name and address must be present on your equipment.

### Gateways

A gateway is a radio station that is connected to the internet. Many types of gateways exist, including:

* Linked repeater networks, which connect multiple repeaters in a row;
* the Internet Radio Linking Project (IRLP), which connects amateurs to repeaters via VoIP using DTMF;
* EchoLink, a free consumer software to allow communication through VoIP and ham.

A mesh network is often used as a self-configuring fault tolerant network, where as nodes enter or leave, the system reconfigures to best use the resources available to maintain or keep the network.

## Terminology

| Term             | Definition                                                                                                                   |
|------------------|------------------------------------------------------------------------------------------------------------------------------|
| Beacon           | An amateur station transmitting communications for the purposes of observing propagation or related experimental activities. |
| Control operator | A licensed amateur designated by the station licensee to be responsible for all transmissions from the station.              |
| Control point    | The location at which the control operator function is performed.                                                            |
| Repeater         | A station that receives and then rebroadcasts a signal.                                                                      |

## Procedural Signals[2, 18]

 | Signal | Meaning                                                                                                                            |
 |--------|------------------------------------------------------------------------------------------------------------------------------------|
 | CQ     | Calling any station (responses start with the callsign of the initial caller, followed by the callsign of the current transmitter) |
 | DX     | Distant station (from another country)                                                                                             |
 | 73     | Best regards                                                                                                                       |
 | 88     | Love and kisses                                                                                                                    |
 | Roger  | All received OK                                                                                                                    |
 | hi hi  | Ham version of LOL                                                                                                                 |
 | QRM    | Interference                                                                                                                       |
 | QRN    | Static, atmospheric noise                                                                                                          |
 | QRP    | Low power operation (5 watts or less)                                                                                              |
 | QRT    | Go off the air                                                                                                                     |
 | QRZ?   | Who is calling me? (pronounced Q-R-Zed)                                                                                            |
 | QSB    | Your signals are fading                                                                                                            |
 | QSL    | I acknowledge receipt                                                                                                              |
 | QSO    | A contact (pronounced Q-So)                                                                                                        |
 | QSY    | Change frequency to ...                                                                                                            |
 | QTH    | My location is ...                                                                                                                 |

## Frequency Ranges

| Band                       | Range            |
|----------------------------|------------------|
| Low frequency (LF)         | 30 kHz - 300 kHz |
| Medium frequency (MF)      | 300 kHz - 3 MHz  |
| High frequency (HF)        | 3 MHz - 30 MHz   |
| Very high frequency (VHF)  | 30 MHz - 300 MHz |
| Ultra High Frequency (UHF) | 300 MHz - 3 GHz  |

### Permissions

Depending on your title (Technician, General, or Amateur Extra are currently offered), you are only allowed to operate in certain bands.

## Locations

### Regions

The International Telecommunication Union in Geneva, Switzerland has named three regions for the radio spectrum:

* Region 1: Europe, Africa, and Northern Asia
* Region 2: North and South America
* Region 3: Southern Asia and Australia

Note: when talking internationally, the FCC limits you to "communications incidental to the purposes of the amateur service and to remarks of a personal character".[14]

### Maidenhead Locator System[x]

The Maidenhead Locator System is a geographic system that divides up the surface of the earth into grid squares that makes an easy shorthand for communicating your position over the air.

### Planes, Boats, Etc.

> Your FCC license (or authorization for alien reciprocal operation) allows you to operate from airplanes and water vessels within U.S. territory or waters, and from craft with U.S. documentation or registration on or over international waters, but only with permission from the master of the vessel or pilot in command.[8]

## Transmitter Power[16]

The FCC's power limits are based on peak enveloper power (PEP), which means the power into the antenna when the transmitted signal is the strongest. (In some modes, the transmitted power varies based on the information signal. For example, an SSB phone transmission has more power when the operator speaks louder.)

The maximum power permitted to amateur stations on most bands is 1500 watts. Some bands have lower limits.

Technician class operators are limited to 200 watts on the HF bands, and 1500 watts on most frequencies above 30 MHz.

The FCC requires all amateurs to use the minimum transmitter power necessary to carry out the desired communications.

## Amateur Satellites

An Earth station is an amateur station within 50 kilometers of Earth's surface that communicates with space stations or other Earth stations via space stations. A space station is an amateur station over 50 kilometers from Earth's surface that communicates with Earth stations or other space stations via Earth stations.

If your license allows you to transmit on the satellite's uplink frequency, you can use space stations and amateur satellites. They are only allowed to use specific bands based on their mode designator.

A satellite beacon is a transmission from a satellite with various telemetry, such as the health and status, location, and operating schedule. Satellite beacons use various different modes, including CW, RTTY, Packet, synthesized voice, etc. Anyone can receive these telemetry signals.

### Linear Transponder

A linear transponder repeats all signals in whatever frequencies are specified.

Always use the minimum amount of power necessary, or you may be in violation of FCC regulations. You can determine whether you are good by listening to the satellite's downlink frequency and ensuring your signal is roughly the same as the beacon's signal.

## Repeaters

Repeaters are antennas under automatic control that repeat incoming signals for better signal propagation.

These repeaters have different input and output frequencies so that it can take in signal and put out signal that won't interfere with each other. The repeater's **offset** is the difference between input and output frequencies of the repeater.

| Band        | Frequencies   | Typical repeater offset |
|-------------|---------------|-------------------------|
| 10 meters   | 28-29.7 MHz   | 100 kHz                 |
| 6 meters    | 50-54 MHz     | 1 MHz                   |
| 2 meters    | 144-148 MHz   | 600 kHz                 |
| 1.25 meters | 222-225 MHz   | 1.6 MHz                 |
| 70 cm       | 420-450 MHz   | 5 MHz                   |
| 33 cm       | 902-928 MHz   | 12 MHz                  |
| 23 cm       | 1240-1300 MHz | 20 MHz                  |

### Frequency Coordinators

A frequency coordinator is an individual or organization that recommends frequencies to auxiliary and repeater stations in an attempt to reduce interference and make optimum use of the available spectrum.  The frequency coordinators are chosen by the local and regional ham radio operators.

If your auxiliary or repeater station is causing interference, it is **your** responsibility to resolve the interference.

### Simplex vs. Duplex

When transmitting and receiving on one frequency, you are working *simplex*. When transmitting via a repeater, you are working *duplex*: transmitting on one frequency and receiving on another. Try to opt for simplex as often as possible.

### Repeater Tones

Since repeaters are often installed near other electronic equipment, repeaters often have a signal that will open the squelch of the receiver and then retransmit the incoming signals on the uplink to the downlink. Think of it like an "Alexa" or "Hey Siri" but for the repeater.

This often done using these types of signals, where "subaudible" means below 300 Hz:

* CTCSS (Continuous Tone-Coded Squelch System (CTCSS): This encodes the input message with a subaudible frequency;
* DCS (Digital-Coded Squelch): This encodes the input signal with a more complex subaudible digital code;
* DTMF (Gual-Tone, Multi-Frequency): This is the touch-tone system used for phone numbers, which uses a pair of audible tones to transmit numbers, and is used for sending commands over the air to the repeater.

## Band Plan

A band plan is a voluntary guideline that organizes certain activities and modes within the amateur radio spectrum. The ARRL established this and it is used by most hams in the United States.

There is a large list of band plans[17], but the most important one is 146.52 is the national simplex calling frequency for the 2 meter band.

## Receivers

Modern receivers are usually superheterodyne[26] receivers, "heterodyne" meaning mixing two signals together. The process of converting an RF signal into audio is as follows:

* Antenna collects the signal
* (opt. preamplifier)
* RF filter selects the signal to be received
* RF amplifier
* Mixer combines received signal with a local oscillator for the given frequency, producing the "sum" and "difference" signals
* IF (intermediate frequency) filter filters to only the IF
* IF amplifier
* Detector extracts the information from the IF signal and converts to audio
* Audio amplifier

### Shopping for a Receiver

The three key factors are sensitivity of signal to noise, selectivity of a given signal between many, and stability on a given frequency.

## Transverter

An upconverter/downconverter converts low frequencies to high/high frequencies to low. A transverter is both in a single unit.

## Signal Propagation

(Much of the language in this section is reminiscent of FM synthesis in music, and that may be helpful in remembering this stuff)

All signals contain a **carrier**, which is a (usually very very high frequency) signal of constant strength and frequency (a sine wave). The **baseband** is a signal containing data to be transmitted. When these two signals are combined, or **modulated**, they are ready to be transmitted.

When this signal is received, it needs to be **demodulated**, so that the carrier can be separated from the baseband.

The modulation creates **sidebands**, which are above and below the frequency of the carrier and contain the data being sent.

The **bandwidth** of the signal is the width of the frequency range occupied by the carrier and the sidebands.

### Modulation Types

The three types of modulation are:

* Amplitude modulation (AM), which changes the amplitude of the carrier;
* Frequency modulation (FM), which changes the frequency of the carrier; and
* Phase modulation (PM), which changes the phase of the carrier.

AM signals have three parts: a carrier in the center and two sidebands that mirror each other on either side.

FM and PM are essentially identical in their result. Repeaters commonly use FM and PM since they are more resilient to man-made noise.

### Multiple Signals

When multiple AM signals are received, they stack on top of each other, creating a "pileup". When multiple FM signals are received, then strongest one will be the only one heard, due to a quirk with FM. This is known as the "FM capture" effect.

### Signal to Noise

A high ratio of signal to noise is desired, as receivers can block out unnecessary noises from the incoming signals and make the incoming signal more legible. Most receivers have multiple options, so you want to choose a filter bandwidth that is as close as possible to your desired signal's bandwidth (e.g. as close to 3 kHz as possible for an SSB signal).

### Carrier Squelch

Squelch is a gating circuit that opens the radio's audio when signals over a certain strength are present, and closes it otherwise. This reduces/removes the annoying static when there are no transmissions.

### Multi-path

Radio waves can take multiple paths through any combination of the preceding or proceeding methods to get to its destination. Therefore the moving of a beam by as little as a few feet can change the signal drastically. The more paths a signal takes, the more error-prone it will have the chance of being.

### Natural and Weather Effects

See also [sporadic E propagation](https://en.wikipedia.org/wiki/Sporadic_E_propagation), [meteor scatter communications](https://en.wikipedia.org/wiki/Meteor_burst_communications), and [auroral propagation](https://www.exploratorium.edu/learning_studio/auroras/).

#### Ground wave vs. Skywave

Ground wave propagation is where the signal travels near the surface of the Earth. They tend to follow the curvature of the Earth and go a littlefarther than line of sight since the atmosphere slightly refracts radio waves.

Skywave propagation is bouncing your RF off of the ionosphere usually 1000 miles or more away. This works bets with HF and below, since higher frequencies than that are higher energy and don't reflect well off of the ionosphere. Under certain conditions, UHF/VHF work well with skywave.

There is a "skip zone" between what is available by ground wave and skywave.

##### LUF and MUF

The Lowest Usable Frequency and Maximum Usable Frequency are relating to skywave propagation. Frequencies below the LUF are absorbed by the ionosphere, and the frequencies above the MUF travel through the ionosphere instead of bouncing off of it.

The frequencies are difficult to predict and ever changing, with their numbers going down at night and going up during the day.

Sun activity can greatly affect the LUF and MUF. More sunspots means higher numbers; when there is less activity, the 10-, 12-, and 15-meter bands are often dead, with the 20-meter band being open most of the time.

#### Tropospheric Ducting[23]

Tropospheric ducting is a type of propagation where a specific weather event (where a mass of warmer air gets trapped on a mass of colder air in the troposphere) allows higher frequencies to bounce more strongly towards the Earth's surface than they otherwise would. Mainly affects VHF and UHF transmissions.

This allows ground wave transmissions to propagate way farther than usual, as far as 100-300 miles away.

#### Rain and Moisture

Rain and moisture can impact the transmission effectiveness for UHF transmissions and gets worse as the frequency increases.

#### Vegetation

The amount of vegetation between the transmitter and receiver can greatly affect UHF and microwave signals as well. In certain places, the best time to transfer is the winter for this reason.

## Radio Frequency Interference[24]

There are four main types of RFI:

* Spurious emissions, or undesired signals from a transmitter;
* Fundamental overload, where a strong signal at one frequency can cause a receiver to lose the ability to receive weaker signals at other frequencies;
* External noise sources, like motors, poor electrical connections, or lightning;
* Intermodulation

## Transmission Types

### {{Morse Code/CW|Morse Code}}

Morse code uses a very low bandwidth, around 150 Hz.

### Single-sideband (SSB)

Single-sideband is a form of AM where only one sideband is transmitted. To decipher this, the receiver has to reinsert the other pieces, being the carrier and the other sideband.

Of the two sidebands, hams tend to use USB (upper) rather than LSB when on VHF and UHF bands. This is because each transmission uses less bandwidth and more power can be focused on a smaller part of the modulated signal, meaning higher power. However, because these are AM signals, they are more susceptible to interference and are also harder to tune in, as there is no distinct carrier to hone in on. When there is difficulty honing on on the exact frequency, hams use a "clarifier" to incrementally move the receiver closer to the correct frequency.

SSB typically uses around 3 kHz of bandwidth.

### FM Voice

FM voice typically uses around 10-15 kHz of bandwidth.

### Amateur Television

Amateur television, also known as "fast-scan television", is television sent by amateur radio operators. Today's broadcasts use a digital [ATSC](https://en.wikipedia.org/wiki/ATSC_standards), but previously used analog [NTSC](https://en.wikipedia.org/wiki/NTSC), PAL, and others.

Fast-scan television typically uses around 6 MHz of bandwidth.

### Packet Radio

Packet radio is a data transmission method using radio frequencies. Short bursts of data are transmitted using {{ASCII}} and is transmitted using frequency or phase modulation, as they are less susceptible to noise.

It utilizes a checksum system to ensure no errors are present, sending a header in each packet that contains the callsign and can help the receiver detect errors. In addition, it also utilizes the Automatic Repeat reQuest (ARQ) protocol, which allows the receiver to request previous packets that were received with errors.

#### Automatic Packet Reporting System

The APRS is a global ham network for sharing real-time data. Most of these APRS nodes are connected to GPS receivers and transmit their locations to the network.

### PSK31[5]

PSK31 uses phase-shift keying for communication between two people. It's not good for transferring large amounts of data, but is very good for keyboard to keyboard data transfer. It works well, even in noisy conditions or poor equipment (in comparison to other modes), and uses very little bandwidth, using 15-20x less than a normal SSB transmission.

### DIGICOM

Digital communications is the transmission of digital voice and data. Two common modes are Digital Smart Technology Amateur Radio (D-Star) and Digital Mobile Radio (DMR).

DIGICOM is totally digital, so there are many repeaters hooked up to the internet, allowing for better and more efficient transferring of data. There are also hot spots which allow ham-to-DIGICOM connections. It allows group conversations also, using "group identification codes" to silo out group conversations.

#### D-Star

Transmitters and receivers on D-Star require one to sign in with their callsign first.

#### DMR

DMR repeaters use time multiplexing to pack two conversations into one 12.5 kHz band, so they end up falling somewhere between SSB and FM in terms of bandwidth usage.

Talkgroups allow you to join isolated conversations on a single repeater. Using a specific talkgroup ID, you can hear only that talkgroup and none of the others. Each repeater can only support two talkgroup conversations at a time, so you may have to wait in a queue for a space to open up.

Color codes are used between two repeaters that have overlapping coverage on a single frequency. Your radio's color code must match that of the repeater or it will ignore you.

A code plug is a config file for your radio that contains DMR information.

### WSJT[6]

"Weak Signal communication by Joe Taylor" is a program that includes various digital data modes used for communication in adverse conditions, including moon bounce, meteor scatter, and others. These modes are *very* slow, so not very suitable for chats between people.

#### FT8[7]

FT8 is a frequency shift keying mode that is useful in very bad conditions. With only 5 words per minute, it's not very efficient, but it can operate down to -20 dB, much lower than CW and SSB transmissions.

## Radio Direction Finding (RDF)

RDF is the process of searching for the source of a given signal. The FCC often uses this when identifying the source of illegal transmissions.

Fox hunting is a contest to be the first to find a hidden transmitter. This requires special hardware, including a directional antenna.

## Amateur Radio Nets[19,20]

A *net* is an on-air gathering of people for a given event or topic. Nets are often recurring, but can be activated as needed, like in the case of a disaster.

Nets have a Net control station, or NCS, to coordinate the communications, which would otherwise be impossible to manage or understand. This role is usually rotated around among the members of the net.

Many nets have a chick-in procedure so that they know who is there, and after that, participants stay on and do not transmit unless asked to do so.

If there is an emergency, preface your callsign with "emergency" or "priority".

## Best Practices

### Choosing a Transmit Frequency

* Select a frequency safely within the band. Radios are not 100% precise and you may end up bleeding over into another band;
* No part of your signal can fall outside the target band, so consider your sidebands and frequencies carefully;
* Allow enough separation with other signals so that they do not interfere with others (remember, this is **illegal**)

### Under/Overmodulation

Because electronic equipment is meant to perform best within its limits and not at the edges, performing at 100% of your radio's capacity can result in under or over exertion of your equipment. This can result in spurious emissions, distortion, lack of intelligibility, and more.

### Handling Interference Complaints

If your radio is causing interference on non-radio devices (TV's, etc.), it is likely that the device is not set up properly. However, you should always ensure your device is operating as expected. Keeping a {{station log}} can also help with this, as you can prove when you were or were not transmitting.

The FCC is the only jurisdiction that can do anything regarding radio interference, so if you are confronted by police based on a neighbor's complaints, you can let them know that it is a federal matter. If you are operating legally, you aren't required to handle the improper rejection of other people's devices.

Fixing these problems almost always lands on the one making the complaints, but there are many solutions. Talk to someone at ARRL and they will be happy to provide assistance or advice.

When *you* are experiencing interference from another person's device, particularly a Part 15 device[25] as noted by the FCC: first, ensure your device is working properly and within bounds; then notify your neighbor of the interference and the FCC regulations regarding it and work with them to discover the interfering device. You may again need to work with a tech specialist from the ARRL to handle the problematic devices and reporting them to the FCC.

## References

1. https://en.wikipedia.org/wiki/Amateur_radio
2. http://www.ac6v.com/jargon.php
3. https://en.wikipedia.org/wiki/Carrier_wave
4. http://www.aprs.org/
5. https://yewtu.be/watch?v=wbmXFzmXF00
6. https://physics.princeton.edu/pulsar/k1jt/
7. https://en.wikipedia.org/wiki/FT8
8. https://www.hamradiolicenseexam.com
9. https://www.fcc.gov/wireless/universal-licensing-system
10. https://en.wikipedia.org/wiki/NATO_phonetic_alphabet
11. http://www.arrl.org/find-a-club
12. http://www.arrl.org/third-party-operating-agreements
13. https://www.ecfr.gov/current/title-47/chapter-I/subchapter-D/part-97#97.115
14. https://www.ecfr.gov/current/title-47/chapter-I/subchapter-D/part-97/subpart-B/section-97.117
15. https://www.ecfr.gov/current/title-47/chapter-I/subchapter-D/part-97#97.113
16. https://www.ecfr.gov/current/title-47/chapter-I/subchapter-D/part-97/subpart-D/section-97.313
17. http://www.arrl.org/band-plan
18. https://www.amateur-radio-wiki.net/codes-and-alphabets/#Q-Code
19. https://en.wikipedia.org/wiki/Amateur_radio_net
20. http://www.arrl.org/arrl-net-directory
21. https://en.wikipedia.org/wiki/Maidenhead_Locator_System
22. http://w8ji.com/station_ground.htm
23. https://en.wikipedia.org/wiki/Tropospheric_ducting#Tropospheric_ducting
24. http://en.wikipedia.org/wiki/Radio_frequency_interference
25. https://www.arrl.org/part-15-radio-frequency-devices
26. http://en.wikipedia.org/wiki/Superheterodyne_receiver
27. [HAM Radio Software](https://github.com/DD5HT/awesome-hamradio)
