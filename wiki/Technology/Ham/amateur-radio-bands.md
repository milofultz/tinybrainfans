---
title: Amateur Radio Bands of the United States
description:
---

Both the frequency range and the wavelength in meters is used as identifiers for the {{amateur radio|HAM Radio}} bands.

*(For memorization's sake, the frequency in MHz multiplied by the wavelength is ~300)*

## Bands

| Frequency range | Band name (Sub-band) | Allocated frequencies       |
|-----------------|----------------------|-----------------------------|
| LF              | 2200 meters          | 135.7 - 137.8 kHz           |
| MF              | 630 meters           | 472 - 479 kHz               |
| MF              | 160 meters           | 1.8 - 2.0 MHz               |
| HF              | 80/75 meters         | 3.5 - 4.0 MHz               |
| HF              | 60 meters            | ~5.4 MHz                    |
| HF              | 40 meters            | 7.0 - 7.3 MHz               |
| HF              | 30 meters            | 10.1 - 10.15 MHz            |
| HF              | 20 meters            | 14.0 - 14.35 MHz            |
| HF              | 17 meters            | 18.068 - 18.168 MHz         |
| HF              | 15 meters            | 21.0 - 21.45 MHz            |
| HF              | 12 meters            | 24.89 - 24.99 MHz           |
| HF              | 10 meters            | 28.0 - 29.7 MHz             |
| VHF             | 6 meters             | 50.0 - 54.0 MHz             |
| VHF             | 2 meters             | 144.0 - 148.0 MHz           |
| VHF             | 1.25 meters          | 219-220 and 222-225 MHz    |
| UHF             | 70 centimeters       | 420 - 450 MHz               |
| UHF             | 33 centimeters       | 902 - 928 MHz               |
| UHF             | 23 centimeters       | 1240 - 1300 MHz             |
| UHF             | 13 centimeters       | 2300-2310 and 2390-2450 MHz |

### Technician Specifics

These are specifics for technician level {{hams|HAM Radio}}.

| Frequency range | Band name (Sub-band) | Allocated frequencies | Notes                                                     |
|-----------------|----------------------|-----------------------|-----------------------------------------------------------|
| HF              | 80 meters            | 3.525 - 3.6 MHz       | Technicians are limited to 200 watts, CW only.            |
| HF              | 40 meters            | 7.025 - 7.125 MHz     | Technicians are limited to 200 watts, CW only.            |
| HF              | 15 meters            | 21.025 - 21.2 MHz     | Technicians are limited to 200 watts, CW only.            |
| HF              | 10 meters            | 28 - 28.5 MHz         | Technicians are limited to 200 watts.                     |
| HF              | 10 meters            | 28.0 - 28.3 MHz       | CW, RTTY, and data only.                                  |
| HF              | 10 meters            | 28.3 - 28.5 MHz       | CW and SSB only.                                          |
| VHF             | 6 meters             | 50.0 - 50.1 MHz       | CW only.                                                  |
| VHF             | 2 meters             | 144.0 - 144.1 MHz     | CW only.                                                  |
| VHF             | 1.25 meters          | 219.0 - 220.0 MHz     | Fixed digital message forwarding systems (data emissions) |

Beyond these are 10 microwave and higher bands not mentioned in the question pools.

## References

1. http://www.arrl.org/files/file/Regulatory/Band%20Chart/Band%20Chart%208_5%20X%2011%20Color.pdf

