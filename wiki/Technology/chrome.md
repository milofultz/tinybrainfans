---
title: Google Chrome
description: Google Chrome is not a safe browser, but sometimes you have to use it.
---

Google Chrome is not a safe browser[2-3], but sometimes you have to use it.

## Disable chrome two-finger swipe back/forward[1]

```
$ defaults write com.google.Chrome AppleEnableSwipeNavigateWithScrolls -bool FALSE
```

## References

1. https://apple.stackexchange.com/questions/21236/how-do-i-disable-chromes-two-finger-back-forward-navigation
2. https://chromeisbad.com/
3. https://contrachrome.com/

