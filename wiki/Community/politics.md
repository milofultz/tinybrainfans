---
title: Politics
description: Politics arise when a group bigger than a single person needs to make a decision of what to do or how to handle a specific situation or type of situations.
---

> Politics arise when a group bigger than a single person needs to make a decision of what to do or how to handle a specific situation or type of situations.
>
> Politics are about:
>
> * what decisions do we need to make together, vs what is left on the backburner for future meetings, or left for everyone to decide for themselves?
> * what do we actually decide to do?
> * how do we make it so that those decisions are followed?
> * and, just as importantly, how do we answer those previous questions themselves?
>
> These questions are unavoidable for any group. This is politics.[1]

## References

1. https://idiomdrottning.org/intro-to-politics

