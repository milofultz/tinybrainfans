https://merveilles.town/web/statuses/107680057823386041

> do:
> - ask if something you did hurt, or how it hurt
> - apologize for the specific thing that hurt
> - resolve to do better
>
> don’t:
> - presume you understand
> - apologize conditionally
> - “that was not my intent”
