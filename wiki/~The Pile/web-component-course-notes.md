https://www.linkedin.com/learning/web-components-and-modularization-practical-approaches://www.linkedin.com/learning/web-components-and-modularization-practical-approaches/

## What Are Web Components

I know this

Web Components are defined by MDN as "a suite of different technologies allowing you to create reusable custom elements — with their functionality encapsulated away from the rest of your code — and utilize them in your web apps".

> It consists of three main technologies, which can be used together to create versatile custom elements with encapsulated functionality that can be reused wherever you like without fear of code collisions.
>
> * Custom elements: A set of JavaScript APIs that allow you to define custom elements and their behavior, which can then be used as desired in your user interface.
> * Shadow DOM: A set of JavaScript APIs for attaching an encapsulated "shadow" DOM tree to an element — which is rendered separately from the main document DOM — and controlling associated functionality. In this way, you can keep an element's features private, so they can be scripted and styled without the fear of collision with other parts of the document.
> * HTML templates: The <template> and <slot> elements enable you to write markup templates that are not displayed in the rendered page. These can then be reused multiple times as the basis of a custom element's structure.

Most of the time, people are talking about components via a {{framework|Frameworks (Javascript)}} like {{React}} or {{Vue}}, which require their own bundle of software to do what they do because of things like {{JSX}}, {{Babel}}, {{Webpack}} and a bunch of other stuff.

*Web components* are a standard set by W3C[3] that run in the browser natively and are agnostic to the environment they run in.

Throughout, the word "component" will be the global umbrella holding {{React}} components, Web components, {{Vue}} components, etc.

## Mental Model

"Atomic Design" by Brad Frost[4] talk about components as "organisms" that are made of "molecules" which are themselves made of "atoms". These "molecules" then make up templates which are used to create pages.

Another model is thinking about them as Legos.

### Design Process

1. Start on paper so you can get all messy and abstract.
2. Put it on a whiteboard and think in terms of sections within your component, using colors to delineate them.
3. Design it in a design application, making clear the different states and behaviors.

## References

1. https://www.linkedin.com/learning/web-components-and-modularization-practical-approaches://www.linkedin.com/learning/web-components-and-modularization-practical-approaches/
2. https://developer.mozilla.org/en-US/docs/Web/Web_Components
3. https://www.w3.org/wiki/WebComponents/
4. https://bradfrost.com/blog/post/atomic-web-design/

