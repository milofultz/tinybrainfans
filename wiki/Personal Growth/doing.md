---
title: Doing
description: The difference between where you are and where you want to be is experience.
---

The difference between where you are and where you want to be is experience.

> Andre 3000: "I haven’t been making much music man. My focus is not there. My confidence is not there. I’d like to, but it’s just not coming.
>
> Rick Rubin: "I think you start making a lot of things with no thinking. What it’s supposed to be or whose it for or what anyone else is gonna think. But just get in the habit of making a lot."
>
> "That’s what I gotta get back to."
>
> "Yea, just make a lot. And then, at some point in that process, you’ll be like, ‘hmmm, I really like this.’ And you didn’t know. Through that whole process, you don’t know when that’s gonna happen."
>
> "Yea."
>
> "It’s not a decision you make and it’s not an intellectual idea where, I have a vision and I’m gonna make this thing. It doesn’t happen like that... it rarely happens like that. It happens more just having fun, making things. No stakes."[1]

## References

1. https://mattambrogi.bearblog.dev/make-a-lot/

