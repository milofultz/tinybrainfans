---
title: Goal Setting
description: Goal setting is a way to hone your efforts towards a particular end result.
---

Setting goals is crucial to feeling fulfilled. If live has no inherent meaning, setting goals is the closest when can get to finding that meaning[2].

However, setting goals should not be a good/bad paradigm. It's a balance of desire to achieve the goal without the attachment to whether or not you **ever will**. If you can maintain this, whatever comes up, whether it allows you to work towards the goal or not, will not phase you.

## Five-Minute Rule

> The first five minutes are the hardest.[3]

Getting started on doing a thing is almost always the hardest part (for me, anyway). Spend five minutes moving towards your goal and if you're still not feeling it, then move on. More than likely though, you will realize the problem was actuating on the task itself.

## SOFA[4,5]

**S**tart **O**ften **F**inish r**A**rely is about giving up the attachment to finishing things.

> The point of SOFA club is to start as many things ~~as possible~~ as you have the ability, interest, and capacity to, with no regard or goal whatsoever for finishing those projects.
>
> The goal is acquiring many experiences. The side effects include entertainment and increased skill.
>
> Here's the secret sauce that makes the whole thing work: You can be finished with your project whenever you decide to be done with it.

## SMART Goals

> Ideally speaking, each corporate, department, and section objective should be:
>
> Specific – target a specific area for improvement.
> Measurable – quantify or at least suggest an indicator of progress.
> Assignable – specify who will do it.
> Realistic – state what results can realistically be achieved, given available resources.
> Time-related – specify when the result(s) can be achieved.[7]

## Theme Years

Notes on CGP Grey's "Your Theme" video[1]:

-   Setting intentions towards what kind of person you want to be is more effective than setting goals, especially if you are the kind of person to be discouraged by "failing".
-   Themes should be broad, directional, and resonant. Broad so that you aren't stuck in a pass/fail paradigm, directional that you know what choice to make when confronted with one, and resonant so that you earnestly want to pursue it in an almost hedonistic, visceral sense.
-   The word should mean something to _you_. It does _not_ need to mean anything to anyone else. Only _your_ definition matters.
-   These broad ideas or "goals" are good because they will get you to the crossroads to make decisions you want. Like meditation prepares you to consider choices once you center yourself, this gives you a large area to aim for.
-   It doesn't have to be a year. Years are just nice because of new years resolutions. Set a timeline that is good for you to be able to check in again at some point.
-   The potential problem with a year is that [12-week year](https://12weekyear.com/) problem. You have motivation for the first month and a half or so, and then usually right at the end. So a quarter kind of wraps that all together.

## References:

1. https://www.youtube.com/watch?v=NVGuFdX5guE
2. https://yewtu.be/watch?v=E7RgtMGL7CA&feature=youtu.be
3. https://shawnblanc.net/2016/03/five-minutes/
4. https://merveilles.town/web/statuses/108090211679488850
5. https://tilde.town/~dozens/sofa/
6. https://www.psychologytoday.com/us/blog/finding-new-home/202204/6-principles-form-healthy-habits
7. [SMART criteria - Wikipedia](https://en.wikipedia.org/wiki/SMART_criteria)
