---
title: Sandals
description: Sandals are easy to make, only needing a bike tire, paracord, and some tools.
---

Sandals are easy to make, only needing a {{bike|Bicycles}} tire, {{paracord}}, and some tools.

## Materials

* Bike tire (you can find one at your local bike service store that they are throwing out)
* Paracord, two 4 foot lengths with the ends burned to stop fraying
* Scissors
* Needle node pliers
* Sharpie
* Lighter
* Leather punch

## Instructions

1. Put the tire on the ground and place your foot on the inside of the tire (opposite the outside tread).
2. Trace one foot centered on the tire, mark directly under your ankle bone on both sides leaving 1 inch of the bead (the ridge above the sidewall of the tire) intact, and mark a spot right between your big toe and the adjacent toe. This should look like the shape of your foot plus two tabs under the ankle bones and a mark between where the big toe and index toe would go. Repeat for the other foot ~6 inches farther down the tire.
3. For each trace, cut out the shoes, ensuring that you have the tabs on either side of the ankle bones, punch out the marked hole near the toes with your punch, and punch out the holes on the ankle bone tabs. Use a punch if you can! Scissors etc. can cause long term damage.
4. For each length of cord, tie a knot at one end and push the other end through the hole by the toes so that the knot is on the same side as the bike tread.
5. Push the cord then from the inside of the outer ankle tab through to the outside.
6. Loop the cord under itself from the outside towards the back, and then push the cord from the inside of the inner ankle tab to the outside.
7. Loop the cord under itself again before tying it.

## Tying

The "starting cord" is the cord that starts with a knot and is running from the underside of the toes towards the outside tab. The "back cord" is the cord running from tab to tab at the rear of the foot.

1. Thread the end of the cord under the starting cord and back towards the inside of the foot.
2. Thread the end of the cord under the back cord, around the loop made for the tab, and then between the two cords going up towards the top of the foot.
3. Thread the end of the cord around the back of the foot towards the outside tab.
4. Thread the end of the cord under the starting cord, around the loop made for the tab, and then between the two back cords so the end is now going up towards the top of the foot.
5. Thread the end of the cord under the two cords that are running to the inside of the foot, over the main cord, back under towards your newest cord, and then through the newly made loop to create a knot.
6. Tie off the excess cord, either around your ankle or somewhere else. I wouldn't cut the excess off, as the knot under the foot is the first to go and without extra, you have to cut a whole new length of cord.

## References

1. https://www.instructables.com/Upcycled-Minimalist-Running-Huaraches-or-Lifestyle/

