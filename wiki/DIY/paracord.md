---
title: Paracord
description: Paracord is a synthetic cord containing smaller synthetic cord inside.
---

Paracord is a synthetic cord containing smaller synthetic cord inside. This synthetic cord can be removed and used as well, though it is extremely thin.

## Sizes

| Kind | Width | Inner Strands | Notes                 |
|------|-------|---------------|-----------------------|
| 550  | 4mm   | 7             | Most common, standard |
| 275  | 2.4mm | 5             |                       |

## References

1. [Bored Paracord](https://www.boredparacord.com/)
2. [Weavers of Eternity - Paracord projects](https://yewtu.be/channel/UCprDa_LLciUHLoxtoJzPsbg)
3. [Cord comparison chart](https://www.paracordplanet.com/cord-comparison-chart/)
4. [Parachute cord - Wikipedia](https://en.wikipedia.org/wiki/Parachute_cord)

