---
title: Shaving
description: The basics of getting started with shaving using a safety razor.
---

Using a safety razor is satisfying, effective, and cheap.

*NOTE: the TSA will confiscate your safety razor in a carry on bag, even if it has no blades in it. Just don't bring them with you when you travel unless you check it.*

What you will need to shave with a safety razor:

* Safety razor
* Razor blades
* Shaving soap or cream (either from the tube or from an aerosol can)
* Cup or bowl, if using shaving cream from the tube
* Shaving brush

Steps:

1. Lather your soap or cream in your cup or bowl[1]
1. Wet where you want to shave and use your brush to apply the lather
1. Place razor blade into your safety razor
1. Shave
1. Repeat steps 3-5 until complete

## Safety Razor

The world of safety razors is another one full of expensive options. But you can also get quality vintage razors for the same price as inexpensive sorta cheap modern razors. So check ebay for "a brass ball end Gillette Tech or a fat handle Gillette Tech"[4] or other older Gillettes. I got a 1950's Gillette brass ball end for $20 shipped and it is way better than the $30 new Viking one I got. The big difference is weight and build quality. There is a user on ebay `sealover811`[5] who sells refurbed vintage razors and they all seem very well done; it's where I got mine and I've been very happy with it.

For budget razors, a lot of people even recommend getting one from Dollar Tree[4]. No experience myself, but knowing this, you probably can't go wrong getting whatever you can find, especially to get started.

## Making Lather[1]

This doesn't apply if you are using an aerosol cream.

1. Soak the brush in warm water for around 10 minutes.
1. Squeeze out all excess water from the brush. You don't have to get it *all* out, but get as much as you can.
1. If using cream, add a healthy dollop of cream to the bottom of your cup or bowl. Otherwise, use the container your soap resides in and add a little bit of water to it before starting.
1. Use your brush to gently press into the cream or soap and stir to agitate and create lather. This is a similar process to beating egg whites in the sense that you are trying to create a fluffy and airy consistency from what is otherwise fairly wet. Use more vigor than you think, but you don't want to be ballistic, either.
1. Eventually, you should have something that is not wet, more like the consistency of whip cream.

## Shaving Technique

When shaving, you don't need *pressure*. You just need the right angle and the hair will come off.

> just the weight of the razor press against your skin & lightly drag it down/across/against the grain[3]

## References

1. [How to lather shaving soap](https://yewtu.be/watch?v=_4zESTQWDuw), and [this one too](https://yewtu.be/watch?v=SUzlE5Qmuw8)
2. [Reddit Wiki on Wet Shaving](https://teddit.net/r/wicked_edge/wiki)
3. [Kira's guide to safety razors](http://kira.solar/safety-razors.html)
4. [Best budget safety razor?](https://www.reddit.com/r/wicked_edge/comments/lddsa5/best_budget_safety_razor/)
5. [sealover811: ebay seller for used safety razors](https://www.ebay.com/str/sealover811)

