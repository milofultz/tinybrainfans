---
title: Lichen CMS Tutorial
description: How to set up Lichen on Nearly Free Speech
---

This is a tutorial on how to set up {{Lichen}}, a minimal CMS/{{static site server|Websites}} that uses {{Gemtext}} for the user to create pages, on Nearly Free Speech[4]. To run it locally, follow the instructions on the Lichen site[1].

## Getting Started

Make sure you have these things downloaded before getting started:

-   NearlyFreeSpeech account (using all recommended defaults should be fine)
-   Some kind of terminal (OSX's "Terminal" or iTerm2)
-   Any web browser

### NearlyFreeSpeech

1. Create a new account and make a new site.
2. Get your SSH login details from the site under the "Sites" tab (enable SSH if not yet enabled).
3. Using your terminal, SSH into your site. Follow the recommendations from NFS on how to log in (correct username, domain, etc.).
4. Clone the Lichen repo[3] in your `public` folder: `git clone <repo.url>` (you should automatically be in the `public` folder, but to check, try `echo $PWD`). If you type `ls -l`, you should see a folder called `lichen`.
5. Move all the files from the new `lichen` folder into the `public` folder: `mv ./lichen/* ./`. If you type `ls -l`, you should see a lot more files and folders.

### Lichen

1. Go into the `cgi-bin` directory: `cd cgi-bin`.
2. Verify that all `.cgi` files in the `cgi-bin` folder have execute permissions enabled. If you type `ls -l *.cgi`, all entries should have a series of letters and hyphens and the start of the line; these should all contain three `x`'s.
3. Go back to the `public` folder: `cd ..`
4. Ensure that the public files can be written and modified by your web server; this may require changing file or directory permissions: `chgrp -R web /home/public`.

At this point, if you go to the URL for your site (make sure you turn off maintenance mode[6]), you should see the stock Lichen homepage!

#### Creating Users

Now that we have the software installed, we are going to add edit functionality. We need to make a password file[2] for Lichen to read so we can edit pages. `htpasswd` is a way for Apache to create an encrypted password file on the server.

1. Create a new "AuthUserFile" and user using `htpasswd`. From the `public` folder: `htpasswd -c ../protected/.htpasswd newUserName`. Enter in the password for your new user; this is what you will use for editing pages in Lichen.
1. Find the "Apache Document Root" for your site on Nearly Free Speech under the "Sites" tab and the current site.
1. Edit the `.htaccess` file: `nano cgi-bin/.htaccess`
1. Change the `AuthUserFile` line, replacing the filepath with what you copied off of NFS followed by the path to the new `htpasswd` file. e.g. If your Apache Document Root is `/fabc/test_domain/`, you would change the line to `AuthUserFile /fabc/test_domain/protected/.htpasswd`.

Now we should be able to navigate to the URL for our site followed by `/cgi-bin/edit.cgi` and enter in our username and password, resulting in access to the CMS backend!

## References

1. https://lichen.sensorstation.co/
2. https://httpd.apache.org/docs/current/programs/htpasswd.html
3. https://git.sensorstation.co/lichen.git
4. https://www.nearlyfreespeech.net/
5. https://faq.nearlyfreespeech.net/full/maintenancemode#maintenancemode
