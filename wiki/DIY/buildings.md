---
title: Buildings
description: Resources for constructing buildings/spaces using minimal resources.
---

Eventually would love to have a frugal living meta to collate many of these ideas, plants, food, etc.

## Small Spaces

A tiny house is appealing because less space means less stuff, which means less maintenance, which means more time. Obviously, having less stuff also means you may not have what you need all the time, but this is more of a call for asking yourself what you actually need, anyway.

A-frames[10,11] have a similar appeal in that they are much simpler to build than a standard house and are also usually fairly small.

## References

1. https://richsoil.com/wofati.jsp
2. https://richsoil.com/electric-heat.jsp
3. https://wiki.lowtechlab.org/wiki/Garde-Manger/en#Description
4. https://zerowastechef.substack.com/p/put-waste-to-work-in-the-backyard
5. https://www.lowtechmagazine.com/2022/01/the-revenge-of-the-hot-water-bottle.html
6. https://www.youtube.com/channel/UCAL3JXZSzSm8AlZyD3nQdBA
7. https://www.youtube.com/watch?v=Y7AKosBrSNk
8. https://yewtu.be/watch?v=iCGXVk-cBVk
9. [CA artist-farmer builds $25K quiet home to savor simple life](https://yewtu.be/watch?v=6T8S258_Zog)
10. [The Backcountry Hut Company – Prefab Cabin Kit](https://www.thebackcountryhutcompany.com/)
11. [Den Outdoors - Small A-Frame House with Loft](https://denoutdoors.com/products/small-a-frame-house-with-loft)
12. [DOME HOMES](https://pacificdomes.com/dome-homes/)

