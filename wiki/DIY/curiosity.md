---
title: Curiosity
description:
---

* You don't have to spend money to have fun
* Learn from kids
* What can you do with what you already have in your house?
* Can this be a game?
* Can you do nothing?
* Where did the thing you are wearing come from? What is it made out of? Where did *that* come from?
* You don't have to go to a store or a restaurant
* Be active, or don't

## References

1. https://www.theguardian.com/commentisfree/2022/aug/17/england-public-trespassing-reclaim-our-countryside
2. https://matt-rickard.com/list-of-all-oblique-strategies

