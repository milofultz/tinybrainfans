---
title: Knife Sharpening
description: Sharpening your knives at home is probably not the cheapest, but it is a fun meditative activity, and very satisfying.
---

## Fundamentals

1. Abrasive (whetstone)
2. Sharpening angle
3. The burr
4. Consistency

### Abrasive

Whetstones come in three categories:

1. 800- Coarse (used for dull knives or repairs)
2. 800-2000 Medium/Medium Fine (sharpening starts here)
3. 2000+ Fine (polishing)

### Sharpening Angle

Most knives have two even angles that need to be sharpened. Some more uncommon knves have one flat edge and one sharp edge (Japanese knives usually). The angle is usually between 15-25 degrees. The shallower the angle, the sharper the knife, but the more prone to chipping/damage.

### Burr

The burr is a raised edge that occurs when sharpening one side of a knife. For a sharp knife, we want to remove the burr completely by sharpening one side of the knife until it is sharp and a burr forms, repeating on the other side, and repeating back and forth until the burr is minimized.

### Consistency

You must be consistent in how you are holding you knife when sharpening on both sides. If you are inconsistent, the burrs will not be regular and work you have done is likely to be undone.

## Technique

### Sharpening

Prepare the whetstone by letting it soak in water anywhere from 30 minutes to 8 hours.

When preparing the sharpen, take the whetstone from the soak and place it on a sturdy surface.

Hold the knife in your right hand: 3 back fingers on the handle, thumb on the heel/bottom of the blade, index on the back. This will help lock your wrist and maintain a good sharpening angle. With your left hand, place three fingers flat on the blade with the edge facing you.

To determine the proper angle to hold the knife while sharpening, place it flush against the stone, rotating it until the gap between the blade's edge and the whetstone is minimized.

Use pressure on the trailing strokes (away from you), release pressure on the leading strokes (towards you).

Begin making passes. Splash some water on your whetstone before beginning. Make trailing and leading strokes, starting at the base of your blade slowly moving towards the tip, with your non-knife hand moving with you to maintain pressure between the knife and the stone. When reaching the tip, raise your elbow on your knife hand to ensure you are getting full coverage on the tip itself. Check for a burr developing on the opposite side after each pass. When an even burr has developed, switch sides.

On the opposite side, maintain the same grip styles but with switched hands. Repeat this whole process.

### Polishing

Repeat the previous process but on the finer grit stone.

### Stropping

Using the same grip you had before, go from tip to base, with your non-knife hand holding a slight amount of pressure around the tip and ensuring your are raising your arm slightly to make good contact with the tip.

Repeat 10 times per side, moving down to 8 per side, 6, 4, and then finishing with 2 on each side. If needed, do one pass per side until desired polish is reached.

## Testing For Sharpening Angle Consistency

Run a magic marker or sharpie along the sides next to the edge before sharpening, polishing, or stropping. When making passes, look to see that you are removing the marks you made and they are being removed at a consistent width starting from the edge of the knife going inward.

## References

1. https://www.youtube.com/watch?v=TkzG4giI8To
2. https://www.gearpatrol.com/home/a540109/how-to-sharpen-kitchen-knives/
3. [Honing is not sharpening](https://yewtu.be/watch?v=YsfbStV7pqE&listen=false)
