---
title: Lichen CMS Tutorial (PHP Version)
description: How to set up a dev environment to set up Lichen on your computer.
---

This is a tutorial on how to set up a dev environment on your computer to run {{Lichen}}, a minimal CMS/{{static site server|Websites}} that uses {{Gemtext}} for the user to create pages.

*NOTES:*

- *This is from the perspective of a {{Mac|MacOS}} user, but hopefully most of what we'll be doing won't be so OS-specific.*
- *This is the older PHP version. For the up-to-date tutorial, please use the {{current tutorial|Lichen CMS Tutorial}}.*

## Getting Started

Make sure you have these things downloaded before getting started:

* Lichen[1], unzipped from the archive
* XAMPP[2] of the newest version (you don't need a VM, so if your ZIP file ends in `-vm`, download the other one), installed
* Some kind of code editor (e.g. Sublime Text or VS Code)
* Some kind of terminal (OSX's "Terminal" or iTerm2)
* Any web browser

Restart your machine after all installations are complete.

### XAMPP

Now let's check to make sure XAMPP is doing what we expect it should be doing.

1. In your Applications folder, you should see a folder called "XAMPP".
2. In that folder should be an app called `XAMPP manager-osx.app`. Open this.
3. You should see a dialog box with three tabs: "Welcome", "Manage Servers", and "Server Events". Click on "Manage Servers".
4. There will be a few rows in the table, including a {{MySQL}} {{database}}, an FTP server, and an {{Apache}} web server. We want to make sure that the Apache server is running. In the "Status" column, it should say "Running", and on the far left, it should have a little green light.
5. Click on the row of the Apache web server and click the "Configure" button on the right. You should see a "Port" number, which is `80` by default. It can be anything, but take note of what port it is using.
6. Open a browser and put the following in the address bar: `localhost:80`, `80` being the port number from the last step. If it is different, replace `80` with whatever is there.
7. This should load an XAMPP page being served from your computer.

### Lichen

The directory of where the files are being served from in the XAMPP steps can be found in the same "Configure" dialog where we found our "Port". We will need to find this directory to both load our Lichen files and change what files are being served.

1. In the XAMPP application, go to "Manage Servers".
2. Select the Apache web server and click "Configure".
3. Click "Open Conf File". Understand that what you are doing *very well can* mess up your server configuration, meaning you'll need to uninstall XAMPP and start over. Continue to open this file.
4. Search this file for `DocumentRoot`. Any line in a `.conf` file preceded by a hash mark is a comment, so you can ignore any comments with `DocumentRoot`. The resulting line should be something like: `DocumentRoot "/Applications/XAMPP/xamppfiles/htdocs`.
5. In our file explorer (Finder in MacOS), navigate to this directory. (For Mac, you can copy this directory and use `Cmd + Shift + G` to go directly).
6. Once you are there, create a new folder called `lichen` and copy the contents of your unzipped Lichen archive into this new folder.
7. In the `.conf` file we had open previously, We are going to edit the `DocumentRoot` line and the one below it which should match the following: `<Directory "/Applications/XAMPP/xamppfiles/htdocs">` to now point at our new `lichen` folder. E.g. if we started with the directory `/Applications/XAMPP/xamppfiles/htdocs`, we now want to change that directory in the `.conf` file to `/Applications/XAMPP/xamppfiles/htdocs/lichen`.
8. Save your `.conf` file, close it and restart the Apache web server.

At this point, you should be able to go to `localhost:80` in your browser and see the Lichen homepage!

#### lichen.htpasswd

Now that we have the software installed, we now need to make a password file[3] for Lichen and ensure the CMS can read it so we can edit pages. `htpasswd` is a way for Apache to create an encrypted password file on the server. *NOTE: We would put this into a different folder structure on an actual deployed server.*

1. First, we need to find the server's root directory. Open the XAMPP application, select Apache web server, click "Configure" and open the conf file.
2. In the conf file, search for `ServerRoot`. You should see a line like `ServerRoot "/Applications/XAMPP/xamppfiles"`. Copy this path for use later.
3. In your terminal, navigate to the XAMPP folder in Applications. Using XAMPP's default install directory, `cd /Applications/XAMPP`.
4. From here, we will use Apache's `htpasswd` utility[3] to create a password file for Lichen, as well as a user. Execute the following command, replacing `${SERVER_ROOT}` with the ServerRoot path found in the conf file, and replacing `username` with your username: `./bin/htpasswd -c ${SERVER_ROOT}/lichen.htpasswd username`. It will prompt you for a password and confirmation.
5. At this point, in the ServerRoot folder we copied above, we should see a file called `lichen.htpasswd`.

We now need to associate this password file with our installation of Lichen.

1. Open the folder where you installed `lichen`; it should be within your XAMPP's `htdocs` folder.
2. Open the `.htaccess` file found in the `cms` subfolder in your editor.
3. Find the line that starts with `AuthUserFile`. We want to change the text following this to `lichen.htpasswd`. It should now read `AuthUserFile lichen.htpasswd`. If you preface the filepath with a slash, it will read the root of your computer, which is not what we want.
4. Save and close the file. Restart the Apache web server.

Now we should be able to navigate to `localhost:80/cms/edit.php` and enter in our username and password, resulting in access to the CMS backend!

## References

1. https://lichen.sensorstation.co/
2. https://www.apachefriends.org/download.html
3. https://httpd.apache.org/docs/current/programs/htpasswd.html

