---
title: Moving
description: Moving sucks so having a good checklist of things to do when you move helps.
---

## Canceling/Transferring Utilities

* Gas
* Electric
* Water
* Internet/Cable
* Trash/Recycling

## Canceling Memberships

* Gyms
* Volunteering
* Union

## Change Of Address

* USPS
* Credit Cards
* Banks
* Insurance
  * Renters
  * Car
  * Health
  * Music Instrument
* Work/Employers/Clients
* Union
* Friends and family

## Packing

### Bedroom

* Clothing
* Bed
* Furniture

### Hobbies/Work

* Music gear
* Desk
* Computer accessories
* Books
* Longboards
* Bikes

### Kitchen

* Silverware
* Plates
* Bowls
* Mugs/cups
* Cooking utensils (spatulas, etc.)
* Mixing bowls
* Pots and pans
* Breadmaking
* Cookbooks
* Furniture (shelving, etc.)

### Living Room

* Furniture
* Books

### Bathroom

* Hygiene stuff
* Cleaning products

### Miscellaneous

* Camping
* Emergency Kit

## Selling/Getting Rid Of Things

* Craigslist
* Social media
* Friends/family/neighbors
* Goodwill or another thrift store (Not Salvation Army[1], if you can help it)

## Cleaning

* Magic eraser
* Barkeeper's friend
* Rags
* Drywall fixing stuff
* Broom/dustpan
* Vacuum
* Mop and bucket
* Trash bags
* Take video when complete and fully moved out to cover your ass

## Actually Moving

* Schedule moving truck
* Schedule hotels
* Ratchet straps
* Moving blankets

## New Place

* Take video of whole place before moving in anything to cover your ass

## References

1. https://www.huffpost.com/entry/the-salvation-armys-histo_b_4422938

