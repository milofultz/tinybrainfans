---
title: Websites
description: Websites should be simple to setup, easy to maintain, and fun.
---

Websites should be as simple as possible. For most people, they only need to be able to show text, images, and links to other things, all on a static site. These options are mostly going to be for those people.

## Github/Codeberg Pages[5-6]

If you upload raw HTML, CSS, and JS files to one of these {{git}} repos, it will render them as a static site at a specific URL. If your website doesn't need write privileges (comments, database, etc.), your site should work.

The advantage this has is that you don't have to worry about a domain or hosting or anything. Disadvantage is you need to understand the basics of {{git}} to use it.

Some static site generators include Jekyll[10,11] or gemkill[9] for basic sites, and swiki[4] for wiki-style backlinking.

(I made gemkill[9], an ultraminimal {{Jekyll}}-adjacent static site builder in {{Ruby}}. This lets you write in {{gemtext}}, make a frame for your HTML pages, run gemkill, and your site is built.)

### Scripting

If you are technically inclined, then you can utilize {{shell scripting|shell}} to make building the pages simpler. Opfez[14] on [merveilles.town](https://merveilles.town) had this on their site and I thought it was genius:

```shell
#!/bin/sh

for p in posts/*; do
  echo "generating $(basename $p)..."
  cat blog/_header.html $p blog/_footer.html > blog/$(basename $p)
done
echo "done!"
```

By putting their files into a `posts` folder and rendering them out to a different place, they can simplify maintenance on their site by abstracting common parts like the header and footer. You can also use whatever converter for something like {{markdown}} or {{gemtext}}.

## Lichen/WonderCMS[2-3]

These are both options that are "set it and forget it" once they've been installed on the hosting server. Obviously, that part is a bit tricky if you don't know how to do that, but if you have a technically inclined friend who can help, you should be off to the races.

Simplest explanation is that they are Wordpress or Squarespace if you were able to make them easier to use by stripping away the bloat.

Lichen uses {{gemtext}}, which is a format utilized by the gemini protocol. This is fairly constrained, but in a way that I find useful and nice. WonderCMS uses a WYSIWYG editor, so has more features, but I can only imagine runs into problems most WYSIWYG editors do.

To try out Lichen on your own computer, check out this {{tutorial|Lichen CMS Tutorial}}.

## Other Options

Publii[15] is "a desktop-based CMS for Windows, Mac and Linux that makes creating static websites fast and hassle-free, even for beginners".

Soupalt[13] is a static site generator built in OCaml. It seems ultra-powerful, but the tradeoff is seemingly in higher complexity.

> You can use soupault for making blogs and other types of websites, pretty much like any other SSGs (Jekyll, Hugo, Eleventy, etc.). However, you can also use it as a post-processor for existing websites—a use case other tools don’t support.

### Client-Side Includes

Using iframes[16] or a little bit of {{Javascript}} and web components[17], a simple static site can be created with templating (h/t Chris Ferdinandi).

*This method and example is from [Chris Fernandi](https://gomakethings.com).*

We can create a file that includes custom HTML elements (web components) that will dynamically load the pages using Javascript by extending the HTMLElement {{class|Classes (Javascript)}}. We put the page we want to load in the `path` attribute and any fallback content within the custom element.

**includeHTML.js**

```javascript
// Extend the HTMLElement class to create the web component
class IncludeHTML extends HTMLElement {

  /**
   * Get and render external HTML
   * @param  {String} path The path to the external HTML
   */
  async #getHTML (path) {

    // Get the page
    let request = await fetch(path);
    if (!request.ok) return;

    // Get the HTML
    this.innerHTML = await request.text();

  }

  constructor () {
    // Always call super first in constructor
    super();

    // Get the source HTML to load
    let path = this.getAttribute('path');
    if (!path) return;

    // Render HTML
    this.#getHTML(path);
  }
}

// Define the new web component
if ('customElements' in window) {
  customElements.define('include-html', IncludeHTML);
}
```

**included-file.html**

```html
<h2>Hi, I'm Chris Ferdinandi.</h2>

<p><strong>I help people learn vanilla JavaScript,</strong> and I believe there’s a simpler, more resilient way to make things for the web.</p>
```

**index.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Include Test</title>
</head>
<body>
  <h1>Hello.</h1>
  <p>This content will load as normal. But the included file below will be loaded once the Javascript module is loaded.</p>

  <include-html path="included-file.html">
    <!-- The below content is a fallback in case Javascript is not enabled, since custom elements will render as `div`s by default. -->
    <a href="included-file.html">Learn more about me.</a>
  </include-html>

  <script src="includeHTML.js"></script>
</body>
</html>
```

### Server-Side Includes

This is definitely a situation for more tech-focused people. Server-side includes allows modularity in a website by modularizing parts of a webpage (the header, the footer, the navigation, etc.) and letting the server put them in your webpages when they are loaded.

The user would need to log into the actual server that holds all these files to do any editing, so in this way it is very direct and simple, but is a real turn off for anyone who is daunted by this kind of workflow.

#### How it works

For example, if you have a `head` page, you would make a file called something like `ssi-head.html`:

**ssi-head.html**

```html
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>My Webpage!</title>
</head>
```

And then include it in your pages like in this `example.shtml` (if you want to know why `shtml`, see [7]):

```html
<!--#include file="ssi-head.html" -->
<body>
  Some content
</body>
```

When a user pulls up `example.shtml` on your site, it will automatically populate your file with the contents of `ssi-head.html`.

## Hosts

If you are running one of the above, you will likely need a server to serve your pages. I recommend using NearlyFreeSpeech[12] as their rates are good and I have heard many good things from people who I trust.

### Setting Up A Site On NFS

If using an external domain, point your domain's resource records to your server's IP addresses[18]. You should see two IP addresses at the bottom of your site on the NFS page; set the IPv4 (e.g. 123.234.34.45) using an `A` record, and your IPv6 using an `AAAA` record.

Set up your TLS via SSH on your server[19]. Log in via SSH and in your home directory, run `tls-setup.sh`.

## References

1. https://en.wikipedia.org/wiki/Server_Side_Includes
2. https://lichen.sensorstation.co/
3. https://www.wondercms.com/
4. https://codeberg.org/milofultz/swiki
5. https://pages.github.com/
6. https://codeberg.page/
7. https://www.andreas.com/faq-ssi.html
8. https://solar.lowtechmagazine.com//2018/09/how-to-build-a-lowtech-website/
9. https://codeberg.org/milofultz/gemkill
10. https://planet-geek.com/2015/04/06/geekitude/sorry-jekyll-im-done-with-you/
11. https://jekyllrb.com/
12. https://www.nearlyfreespeech.net/services/hosting
13. https://soupault.app/
14. http://tilde.town/~opfez/blog/1.html
15. https://github.com/GetPublii/Publii
16. https://gomakethings.com/4-ways-to-include-external-content-in-your-html/
17. https://gomakethings.com/html-includes-with-web-components/
18. https://serverfault.com/a/460853
19. https://faq.nearlyfreespeech.net/full/sslcertificates#sslcertificates

