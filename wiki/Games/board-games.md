---
title: Multiplayer Games
description:
---

## Go Variants

* [Phutball](https://en.wikipedia.org/wiki/Phutball)
* [Gomoku](https://en.wikipedia.org/wiki/Gomoku)

## Chess Variants

* [Hostage Chess](https://en.wikipedia.org/wiki/Hostage_chess)
* [Chess960](https://en.wikipedia.org/wiki/Fischer_random_chess)
* [Bughouse](https://en.wikipedia.org/wiki/Bughouse_chess)

