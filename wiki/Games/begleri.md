---
title: Begleri
description: Begleri is a small skill toy from Greece.
---

Begleri is a small skill toy from Greece. It is fantastic because with materials you have lying around, or with about $5 and a trip to the hardware store, you can make one.

## Length

There is **long game** and **short game**. If you have a long game begleri, you can tie a figure eight knot between the ends and get pretty close to a short game length.

## Beads

A popular and simple version is to use a hex nut. 550 {{Paracord}} is 4mm wide, so a #8 or #10 inner width[5] is good.

## References

1. https://en.wikipedia.org/wiki/Begleri
2. Make your own
    1. [Hex nuts and paracord, the simplest way](https://yewtu.be/watch?v=zaoSSRvdacc&list=PLVpANXnNebdN--mijDoBNdYX251SLDA3_&index=3)
    1. [Steel balls and paracord, using a monkey's fist knot (little kinder to your hand)](https://yewtu.be/watch?v=YLb7z9b3Qgs&list=PLVpANXnNebdNt3ac95JlXlj50PXTL8Gas&index=7)
    1. [Hex nuts and paracord using two types of paracord](https://yewtu.be/watch?v=z0Qv6As7NOs&listen=false)
    1. [Hex nuts and paracord, using lots of paracord but pretty](https://yewtu.be/watch?v=_y6jUc3UYac&list=PLVpANXnNebdN--mijDoBNdYX251SLDA3_&index=2)
    1. [Hex nuts and paracord, but as a bracelet](https://yewtu.be/watch?v=uF0jAHE6tnc&list=PLVpANXnNebdN--mijDoBNdYX251SLDA3_&index=6)
3. [Complete Beginners Guide to Begleri](https://begleritricks.com/guides/complete-beginners-guide-to-begleri)
4. [Getting started with the movements](https://yewtu.be/watch?v=-2DVEFePGgc), and [part 2](https://yewtu.be/watch?v=LL9yWjh5HcU&listen=false)
5. [Screw size chart](https://monsterbolts.com/pages/us-screw-sizes)

