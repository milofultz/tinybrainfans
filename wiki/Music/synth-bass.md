---
title: Synth Bass
description: Synth bass, or key bass, is using a synthesizer as the bass instrument.
---

## Songs and Bands

* Everything on Michael Jackson, particularly the albums "Off The Wall", "Thriller", and "Bad"
* Aretha Franklin's "Get It Right"
* Anything Timbaland produced
* Gap Band
* Zapp and Roger
* Anything from Chaka Khan
* Chaka Khan's "We Can Work It Out"
* Madonna's "Get Into The Groove"[1]
* Any other dance band from the 80's
* Anything produced by Jimmy Jam and Terry Lewis[2]
* Prince/The Time
* Earth Wind and Fire
* Pretty much all late 90's early 00's R&B

## References

1. [Madonna 'Into The Groove' bassline on Juju 60 - YouTube](https://www.youtube.com/watch?app=desktop&v=7Xe1zQ3Cv3w)
2. [Jimmy Jam and Terry Lewis - Wikipedia](https://en.wikipedia.org/wiki/Jimmy_Jam_and_Terry_Lewis)

