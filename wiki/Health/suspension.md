---
title: Suspension Exercises
description: Suspension trainers like the TRX or gymnastic rings make bodyweight exercise a bit more fun and engaging.
---

Suspension trainers like the TRX or gymnastic rings make bodyweight {{exercise}} a bit more fun and engaging.

## References

1. Basic rings exercises: [1](https://www.pullup-dip.com/us/gymnastic-rings-exercises), [2](https://www.pullup-dip.com/us/gymnastic-rings-exercises)
2. TRX/rings with a door anchor exercises: [1](https://yewtu.be/watch?v=f4qGG2i_c4k), [2](https://yewtu.be/watch?v=WUXR58li1s8)

