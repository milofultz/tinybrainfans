---
title: Exercise
description: Exercise is bodily activity that is usually used to maintain physical and mental wellness.
---

Exercise is bodily activity that is usually used to maintain physical and mental wellness.

Good advice from antranik[3]:

> If you cannot maintain proper form, [do] an easier variation. **If you feel pain, STOP. Never work through the pain. REST.**

## Bodyweight Exercise/Calisthenics Routine

The minimalist routine from Reddit's bodyweightfitness[2]:

> Complete 2-6 circuits of the following exercises, completing one set of each exercise comprises one circuit. Take little to no rest.
>
> Each set should be 1-2 repetitions short of failure. If you fail your last rep make a note of it and plan to do 1-2 fewer reps than that in future sets of that exercise. If completing a set felt too easy try adding more reps in future sets or, for push-up and rowing sets, progress to a more difficult variation of the exercise.
>
> 1. [Walking Lunges](https://www.youtube.com/watch?v=L8fvypPrzzs)
> 2. [Push-ups](https://teddit.net/r/bodyweightfitness/wiki/exercises/pushup)
> 3. [Rows](https://teddit.net/r/bodyweightfitness/wiki/exercises/row)
> 4. [Plank Shoulder Taps](https://www.youtube.com/watch?v=LEZq7QZ8ySQ)

The recommended routine from the same spot ([PDF cheatsheet](reddit_bodyweightfitness_RR.pdf)):

> Do this 3x a week, with at least one rest day (or skill day[x]) in between workout days.
>
> [I]n order to effectively increase or decrease the difficulty, you need to use different variations of a type of exercise [see below].
>
> When you get to the strength training, you will be greeted with progression exercises listed in order of increasing difficulty. Pick an appropriately difficult progression for your current level of strength, and perform 3 sets of 5 reps of that progression on your first session. In subsequent sessions you should try to add one rep per set until you are performing 3 sets of 8 reps with good form. From here you should move on to the next progression, but again at 3 sets of 5 reps. Note that this means that you only perform one of the exercises from each of the listed progressions in each session. Once you move up in the progression, there's no need to keep the easier exercises in your routine (except for using it as a warm-up if you feel like it). [...] Some of the exercises are static holds, such as the support holds or the "tuck front lever" in the rowing progression. Instead of dynamic reps, one set here consists of simply holding the position statically for 10-30 seconds. Move on to the next harder progression once you hit 30 seconds for all 3 sets.
>
> Warm-up: Dynamic Stretches (5-10min)
>
> Strength work (40-60 minutes)
>
> * First Pair
>     * 3x5-8 Pull-up progression
>     * 3x5-8 Squat Progression
> * Second Pair
>     * 3x5-8 Dip progression
>     * 3x5-8 Hinge Progression
> * Third Pair
>     * 3x5-8 Row Progression
>     * 3x5-8 Push-up progression
> * Core Triplet
>     * 3x8-12 Anti-Extension progression
>     * 3x8-12 Anti-Rotation progression
>     * 3x8-12 Extension progression
>
> These exercises are to be done in pairs and triplets to save time. Pairing two exercises means doing a set of the first exercise, resting 90 seconds, then doing a set of the second exercise, resting 90 seconds, and repeating until you've done 3 sets of that pair.
>
> Rest time: If 90 seconds is not enough, you can rest up to 3 minutes if you like.
>
> Tempo: Ideally, all these exercises are to be done in a "10X0" (1,0,X,O) tempo. If this looks confusing, don't worry. The numbers explain how long each phase should last, and go in the order of: On the way Down/Pause at the Bottom/On the way up/Pause at the top. So 10X0 means 1 'mississippi' second duration on the way down, no pause at the bottom, eXplode up and no pause at the top. When "exploding up", if the actual movement is slow, that's okay, it's the intent that matters.

For visual tips on technique, see this video from FitnessFAQs[15].

## Main Areas

- Core
- Chest
- Back
- Arms
- Legs

## Moderating Difficulty

There are many ways to add/relieve difficulty for bodyweight exercises. Here are some of the levers you have:

* Increasing/decreasing Body angle
* More/fewer limbs
* Destabilizing support (support limbs on balls or rings/suspension trainers)
* Extra weight
* Increasing/decreasing speed
* Making the exercise plyometric or static
* More/less repetitions

## Miscellaneous Exercise Programs and Methods

* Parkour[7]
* MovNat[8]
* LiveAdept[9]
* Methode Naturelle[10-11]

## References

1. https://en.wikipedia.org/wiki/Exercise
2. https://teddit.net/r/bodyweightfitness/wiki/minroutine
3. https://antranik.org/bodyweight-training/
4. https://nchrs.xyz/site/exercise.html
5. https://teddit.net/r/bodyweightfitness/wiki/routines/bwf-primer
6. https://www.calisthenics-gear.com/convict-conditioning-review/
7. https://en.wikipedia.org/wiki/Parkour
8. https://yewtu.be/channel/UChgSKQAuBPZIBAfokctA33g
9. https://yewtu.be/channel/UCGaWJABs8KOT4kUHlaNFgMg
10. http://www.alliancemartialarts.com/Georges%20Hebert%20Methode%20Natuelle.htm
11. https://www.movnat.com/the-roots-of-methode-naturelle/
12. [Which ab exercises are best and safe](https://yewtu.be/watch?v=_xdOuqokcm4)
13. [8-Minute Abs](https://yewtu.be/watch?v=sWjTnBmCHTY), [Arms](https://yewtu.be/watch?v=CxYT5cS4ljA&listen=false), [Buns](https://yewtu.be/watch?v=n538bSON2kA&listen=false), [Legs](https://yewtu.be/watch?v=B0lF0gDzaAc&listen=false), [Stretch](https://yewtu.be/watch?v=79sXwpZUeBw&listen=false)
14. ["Skill day"](https://teddit.net/r/bodyweightfitness/wiki/kb/skillday)
15. [RECOMMENDED ROUTINE - Reddit Bodyweight Fitness](https://yewtu.be/watch?v=VpobvFPR6hQ&t=7m49s)
16. [ExRx: lots of info on form, muscles used, etc.](https://exrx.net/)

