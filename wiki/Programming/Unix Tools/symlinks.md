---
title: Symlinks
description: Symbolic links allow you to create shortcuts that act exactly like the files they link to.
---

## What Is It

Without the `--symbolic` option, `ln` will create a "hard link"[2], which points directly to the block in memory where the data resides. A symlink, or "symbolic link" or "soft link", will point to the file, which is in itself a pointer to the block in memory. This ensures things that the file itself maintains one single source of truth.

## How To

To create a symlink, use `ln -s`. This will create a "shortcut" to the source file, allowing you to perform normal operations on the file using the symlink as you would if you typed out the full path to the source.

```shell
$ ls
$ # folder contains nothing
$ ln -s /path/to/file
$ ls
$ # folder contains "file", which is a symlink
$ ln -s /path/to/file shortcut-name
$ ls
$ # folder contains "file" and "shortcut-name", which are symlinks
```

## References

1. https://bryandugan.com/creating-symlinks-in-osx/
2. https://linuxhandbook.com/hard-link/

