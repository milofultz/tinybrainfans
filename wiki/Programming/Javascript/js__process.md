---
title: Process (Node)
description: Node has a global `process` variable, which provides access to useful methods and properties.
---

## Why not to use `process.exit(1)`

`process.exit(1)` will quit _without_ gracefully ending exiting processes. To allow a graceful exit, use `process.exitCode(1)`. This will allow ongoing processes to finish and _then_ send exit code `1` back to the executing shell.

```js
// Bad

const { exit } = require('node:process');

// This is an example of what *not* to do:
if (someConditionNotMet()) {
    printUsageToStdout();
    exit(1);
}

// Good

const process = require('node:process');

// How to properly set the exit code while letting
// the process exit gracefully.
if (someConditionNotMet()) {
    printUsageToStdout();
    process.exitCode = 1;
}
```

## References

1. [Exit Code > Process | Node.js v21.2.0 Documentation](https://nodejs.org/api/process.html#processexitcode)
