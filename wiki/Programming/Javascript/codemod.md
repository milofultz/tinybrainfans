---
title: Codemod
description: Codemod is a refactoring tool for code.
---

Codemod is a deprecated refactoring tool for code developed by Facebook.

## References

1. [Codemod](https://github.com/facebookarchive/codemod)
2. {{JSCodeShift}}

