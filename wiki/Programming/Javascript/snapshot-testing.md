---
title: Snapshot Testing
description: Snapshot testing is a way of testing the state of the UI through DOM structure.
---

Snapshot testing is a way of testing the state of the UI through DOM structure. It seems very fragile, as a change in the DOM could not affect the user experience of the app but nonetheless throw an error. Which would lead to a lot of false positives, as you aren't actually testing what matters.

Common tooling used for this is {{Jest}} and {{Enzyme}}.

> You can think about a snapshot test as a unit test with an auto generated assumption about your component.
>
> The advantages are that you can easily test complex structures without writing much code, that you get good warnings when something changed and that you can easily update this test.
>
> The disadvantages are that from just reading the test it is not always clear what is tested and what the expected behaviour is, that it could happen that the created snapshot is so complex that you overlook wrong assumptions that then end up as expected result and that it is so easy to update snapshots that wrong stuff can sneak in.
>
> So when using snapshot test its really important to make them more granular, so not always testing the whole component, but have some smaller test that test parts of it and to have good code review culture, to spot bugs in the snapshot.[2]

## References

1. [Snapshot Testing with Jest](https://jestjs.io/docs/snapshot-testing)
2. [When to use snapshot testing](https://stackoverflow.com/a/43775523)

