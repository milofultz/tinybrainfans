---
title: Yarn
description: Yarn is a hub from which you can install, manage, and maintain node/Javascript projects
---

Yarn is a hub from which you can install, manage, and maintain {{node}}/{{Javascript}} projects.

## Installation

Yarn needs to be installed globally via {{NPM}}.

```sh
$ npm i --global yarn
```

## Basic Commands (for Yarn 1)

| Command             | Args                                                                                                                                                                          | Effect                                                       |
| ------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| `add n [--dev]`     | `n`: Package name(s);<br />`--dev`: Add to dev dependencies                                                                                                                   | Adds package(s) to project                                   |
| `install`           |                                                                                                                                                                               | Install all packages found in project                        |
| `run n`             | `n`: Name of script                                                                                                                                                           | Run the script `n` found under `scripts` in package JSON     |
| `start`             |                                                                                                                                                                               | Run the script found under `scripts`/`start` in package JSON |
| `test`              |                                                                                                                                                                               | Run the script found under `scripts`/`test` in package JSON  |
| `upgrade <package>` | `package` is optional package name. Can be suffixed with `@` followed by a {{semver}}. Without a package, will upgrade all packages <br/> `--latest` to get to latest version | Upgrade                                                      |

## References

1. [Yarn](https://classic.yarnpkg.com/en/)
2. [Upgrading packages (Yarn 1)](https://classic.yarnpkg.com/lang/en/docs/cli/upgrade/)
