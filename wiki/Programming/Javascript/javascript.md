---
title: Javascript
description: Javascript is currently the language of the web. Used to make most apps and do most things.
---

Javascript has a lot of weird stuff going on[2,3].

## There Are Two Javascripts

> There are two Javascripts:
>
> One for the {{server|Node}} - where you can go wild.
>
> One for the client - that should be thoughtful and careful.[5]

## How to Prevent Caching[4]

To prevent caching of a Javascript `src` file in an {{HTML}} file, developers often append a question mark followed by some kind of timestamp or hash. This doesn't affect the resolution of the URL, but does ensure that a new file is cached when it is downloaded.

```html
<body>
  <!-- The below import will always use cached if possible, regardless of
  changes to `file1.js` -->
  <script src="file1.js"></script>
  <!-- The below import will cache `file2.js` as the below filename, ensuring
  that new changes will be loaded and old versions will not load, even if old
  versions of `file2.js` exist -->
  <script src="file2.js?20201031"></script>
</body>
```

## References

1. https://addyosmani.com/resources/essentialjsdesignpatterns/book/
2. https://github.com/lydiahallie/javascript-questions
3. https://www.udemy.com/course/understand-javascript/
4. https://stackoverflow.com/a/39591948
5. https://remysharp.com/2022/10/13/two-javascripts
6. [Vanilla Javascript libraries](https://vanillajstoolkit.com/libraries/)

