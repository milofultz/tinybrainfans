---
title: Templating
description: Injecting text into a template file is one of the my favorite uses of a computer for automating tasks.
---

## References

1. [Mustache](https://mustache.github.io/)
2. {{Pug}}
3. {{Jinja2}}

