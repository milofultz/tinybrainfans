---
title: tmux
description: tmux is a way to use multiple windows and splits in a terminal, as well as automate these and other processes.
---

The `M`/meta key needs to be defined in your terminal[6], as most keyboards don't have it. The ones that do have it bound to `esc`, which can cause problems.

## Cheatsheet

These commands are all prefaced by a `PREFIX`[4], which by default is `<c-b>`.

### Meta

| Command | Effect                    |
|---------|---------------------------|
| `?`     | Show commands             |
| `t`     | Show clock in pane        |
| `r`     | Reload tmux configuration |

### Sessions

| Command  | Effect                      |
|----------|-----------------------------|
| `(`, `)` | Go to next/previous session |
| `s`      | Choose session from list    |
| `$`      | Rename session              |

### Windows

| Command  | Effect                       |
|----------|------------------------------|
| `c`      | Create new window            |
| `1`-`9`  | Go to window                 |
| `n`, `p` | Go to next/previous window   |
| `l`      | Go to last visited window[7] |
| `w`      | Choose from list of windows  |
| `,`      | Rename current window        |
| `&`      | Kill current window          |

### Panes

| Command           | Effect                                                      |
|-------------------|-------------------------------------------------------------|
| `q`, then `1`-`9` | Show pane numbers and if number pressed, go to that pane    |
| Cursor keys       | Go to pane in that direction                                |
| `%`               | Split vertically (left/right)                               |
| `"`               | Split horizontally (top/bottom)                             |
| `o`               | Go to next pane                                             |
| `;`               | Go to last visited pane                                     |
| `{`, `}`          | Move pane to next/previous position                         |
| `C-o`, `M-o`      | Rotate panes forward/back (top-bottom, then left-right)     |
| `!`               | Move pane into new window                                   |
| `C-` cursor keys  | Resize pane                                                 |
| `M-1`-`M-5`       | Use predefined pane layouts                                 |
| `x`               | Kill current pane                                           |
| `z`               | Maximize pane to full screen/return pane to place in layout |

### Copy Mode

I just use Vim bindings while in here and it works.

| Command | Effect          |
|---------|-----------------|
| `[`     | Enter copy mode |
| `<C-c>` | Exit copy mode  |

## References

1. https://tmuxcheatsheet.com/
2. [Troubleshooting colors in vim, terminal, and tmux](https://askubuntu.com/questions/125526/vim-in-tmux-display-wrong-colors)
3. [Scripting](https://www.peterdebelak.com/blog/tmux-scripting/)
4. https://waylonwalker.com/tmux-prefix/
5. https://gist.github.com/andreyvit/2921703
6. https://stackoverflow.com/questions/196357/making-iterm-to-translate-meta-key-in-the-same-way-as-in-other-oses
7. https://stackoverflow.com/questions/31980036/how-to-switch-to-the-previous-pane-by-any-shortcut-in-tmux

