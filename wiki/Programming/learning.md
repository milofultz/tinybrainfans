---
title: Learning Programming
description: Resources for learning to program
---

> Things like math and logic and writing and physics and drawing and all kinds of things got way easier after I had started learning to code. The same goes for the spiritual or psychological experience. Coding (probably better known as meta-thinking, thinking about thinking) is an amazing foundation for other fields.[6]

Some good ways to get into coding kinda sideways is to find communities that use coding to do what they do, but where coding isn't *the point*. Most notable is mods for games. Many games these days have the ability to mod directly, or make levels, or even build them out completely, in the case of {{fantasy consoles}}. Essentially, learning coding for coding's sake is truly only for real nerds who have a shitload of motivation to do that. It is not the best way to get stoked. If you have a band, make a {{website|websites}}. If you have a crappy job that does a lot of data manipulation, learn {{Python}}[7] to try and automate some of it. Whatever it takes to get the wheels turning and give you a satisfying reason to learn it is great.

## References

1. [CodeCombat - Coding games to learn Python and JavaScript](https://codecombat.com/)
2. [Pikuma: Fundamentals of computer science and programming in assembly, for video games, etc.](https://pikuma.com/)
3. [Scratch: a super beginner friendly intro to the art of thinking like a programmer](https://scratch.mit.edu/)
4. [GitHub - toplap/awesome-livecoding: All things livecoding](https://github.com/toplap/awesome-livecoding)
5. [Oldschool intro to programming books](https://usborne.com/us/books/computer-and-coding-books)
6. [Everyone, learn how to code](https://idiomdrottning.org/everyone-code)
7. [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/)
8. [The Coding Train](https://thecodingtrain.com/)

