---
title: BASIC
description: BASIC is a high level programming language, mainly used for its simplicity.
---

BASIC is a high level {{programming}} language, mainly used for its simplicity.

## References

1. https://en.wikipedia.org/wiki/BASIC
2. https://hunt.reaktor.com/tutorial
3. https://archive.org/details/bitsavers_decBooks10Mar75_26006648/page/n1/mode/2up
4. https://annarchive.com/files/Basic_Computer_Games_Microcomputer_Edition.pdf
5. https://github.com/coding-horror/basic-computer-games
6. https://en.wikipedia.org/wiki/Tiny_BASIC
7. https://www.qbasic.net/en/qbasic-tutorials/beginner/qbasic-beginner-1.htm
8. https://www.youtube.com/watch?v=7r83N3c2kPw
9. http://www.ittybittycomputers.com/IttyBitty/TinyBasic/TBuserMan.htm
10. http://www.ittybittycomputers.com/IttyBitty/TinyBasic/
11. http://www.nicholson.com/rhn/files/Tiny_BASIC_in_Forth.txt
12. https://usborne.com/us/books/computer-and-coding-books
13. https://www.quitebasic.com/prj/math/henon/

