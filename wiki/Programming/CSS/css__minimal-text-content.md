---
title: Minimalist CSS
description: Small bits of CSS that do a lot.
---

## Boilerplate

This {{CSS}} code makes really beautiful text-centric websites that are responsive and look nice with only like 54 bytes or something? I use this as a basis all the time for making nice looking pages.

```css
main {
  max-width: 38rem;
  padding: 2rem;
  margin: auto;
}
```

A tiny bit more space yields a nice result, too[3]:

```css
html {
  max-width: 70ch;
  padding: 3em 1em;
  margin: auto;
  line-height: 1.75;
  font-size: 1.25em;
}
```

## References

1. https://jrl.ninja/etc/1/
2. {{Classless CSS}}
3. https://www.swyx.io/css-100-bytes
4. https://github.com/ajusa/lit

