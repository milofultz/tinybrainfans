---
title: Progressive Enhancement
description: Progressive enhancement is a way to make fast accessible websites.
---

Progressive enhancement is a way to make fast {{accessible|Accessibility}} websites by ensuring that people can use the site, even if the Javascript fails. This can also be seen as building the {{minimum viable product}} as quickly as possible, adding features only as necessary.

> always to be the browser’s mentor, not its micromanager[5]

It goes like this:

1. Make it work in raw {{HTML}} (see {{semantic HTML}}).
2. Add CSS to style it how you want it to **without** sacrificing the usability.
3. Add {{Javascript}} to _enhance_ the site, not replace it's functionality.

> Part of the appeal of PE is the strength of the end result. PE forces you to initially plan out your project as a functional system using only the most basic of Web technologies. This means that you know you’ll always have a strong foundation to fall back on as complexity is introduced to the project.
>
> PE goes well with the principle of “Release early, release often.” By starting with a strong foundation, you are able to release projects that work and then concentrate on adding the bells and whistles at a more appropriate time.
>
> PE projects are easier to maintain. With this focus on “first principles,” developers can concentrate on more complex sections of system interaction without worrying about the foundation.
>
> PE is also good for your users. It gives them the security of knowing they can visit your website using any of the thousands of user-agents available to them and still interact with your content as that agent allows.[2]

## References

1. https://blog.jim-nielsen.com/2022/select-the-right-tool/
2. https://www.smashingmagazine.com/2009/04/progressive-enhancement-what-it-is-and-how-to-use-it/
3. https://developer.mozilla.org/en-US/docs/Glossary/Progressive_Enhancement
4. [Building a resilient frontend using progressive enhancement](https://www.gov.uk/service-manual/technology/using-progressive-enhancement)
5. [The ideal viewport doesn’t exist](https://viewports.fyi/)
