---
title: Accessibility
description: Accessibility in software development.
---

Accessibility is for everyone.

> What if the work really did only impact those with disabilities? What if the work was only for those who are blind or deaf? What if the work were only so that one person could use their iPhone? I should hope we’d be doing it anyway.[7]

## Getting Started

Learn about some quick ways you can make your website more accessible on hidde.blog[38,39].

Use this [checklist](https://www.a11yproject.com/checklist/) to ensure you are doing your best to meet accessibility requirements. Consult best practices[5] for those with low-bandwidth internet.

## Contrast

There needs to be a contrast of at LEAST 4.5:1 between the background and foreground of colors for readability.

> In WCAG 2, contrast is a measure of the difference in perceived "luminance" or brightness between two colors (the phrase "color contrast" is never used). This brightness difference is expressed as a ratio ranging from 1:1 (e.g. white on white) to 21:1 (e.g., black on a white).

## Alt Text, Titles, and Captions in {{HTML}}[11-14, 35-37]

> Alternative (Alt) Text is meant to convey the “why” of the image as it relates to the content of a document or webpage.[11]
>
> -   Add alt text to all non-decorative images.
> -   Keep it short and descriptive, like a tweet.
> -   Don’t include “image of” or “photo of”.
> -   Leave alt text blank if the image is purely decorative
> -   It's not necessary to add text in the Title field.

### Alt Text or Caption?

Alt text is for those who cannot see the image at all. Provide a description so that non-sighted users can still understand what the picture is and why it is there. The caption is there for everyone and can be used to provide commentary or extra information you think may be useful.

### Write Alt Text As Poetry[41]

Make alt text like a conversation! How would you describe a photo to your friend over the phone?

> Alt text has existed since the 1990s, but it is often disregarded altogether or understood solely through the lens of compliance. The resulting alt text is often written in a reluctant, perfunctory style, but it has tremendous expressive potential.
>
> 1. Attention to Language
>
> Simply by writing alt text with thought and care, we shift the process. What words are we using? What are their connotations? What is the tone of our writing (the way in which we’re doing the writing)? What is the voice (who the reader hears)? How do these align with, or contrast, the tone and perspective of the image?
>
> 2. Word Economy
>
> People who are new to description have a tendency to over-describe images. While there are times for long and lavish descriptions, alt text usually aims for brevity. For most images, one to two sentences will do. Poetry has a lot to teach us about paring down language to create something that is expressive, yet concise.
>
> 3. Experimental Spirit
>
> We have so much to learn from poetry about being more playful and exploratory in how we write alt text. We are not interested in experimentation for experimentation’s sake — we want a kind of experimentation that moves towards better and more nuanced accessibility for alt text users. There are lots of complex and interesting questions that come up when translating visual information into text. We need to try out different ways of doing this, learning from each other's strategies and techniques.[41]

From "Against Access"[42], talking about a deaf-blind person and their experience interacting with the non-deaf-blind world:

> I tell them a story about the best interpreter I worked with before the Protactile era. He was a volunteer rather than a professional interpreter, and because of this, his commentary was so unvarnished that I picked up a ton through him. He also happened to be a racist and misogynistic Deaf man, but I was able to separate his bias from the information he gave me. I ask my interpreting students, “Are you an unabashed bigot? No? Then you have that much less to be worried about.” This interpreter wasn’t good at his job because he was bigoted; rather, he was good because he functioned as an open channel of information, and so everything in his brain was revealed, his bias along with it. “You don’t want his bigotry,” I tell my students, “but you want his talent for not thinking twice.”

## Accessibility and {{HTML}} Elements

You can go a _long_ way by just using the correct {{semantic elements|Semantic HTML}}. If it's a link, use an `a` tag, if it's a form, use the `form` tag, etc.

### Tab Order

**Only interactive elements should be tabbable!** Tab is a special key used with screen readers, it should **not** be used for everything.

The tab order is set by how they show up in the DOM. If you want users to be able to tab through something in a certain order, you should put the elements in the HTML in that order. The keyboard focus should also _visually_ make sense. When looking at the page as a user, does tabbing through make sense, or is it jumping around in unexpected ways?

You _can_ set the `tabindex` attribute on an element if you must, but it is strongly discouraged and should be avoided if at all possible.

#### `tabindex` Ordering[32]

`tabindex` with values of `0` and `-1` are special cases:

> tabindex="0" means that the element should be focusable in sequential keyboard navigation, after any positive tabindex values. The focus navigation order of these elements is defined by their order in the document source.
>
> A negative value (the exact negative value doesn't actually matter, usually tabindex="-1") means that the element is not reachable via sequential keyboard navigation. tabindex="-1" may be useful for elements that should not be navigated to directly using the Tab key, but need to have keyboard focus set to them. Examples include an off-screen modal window that should be focused when it comes into view, or a form submission error message that should be immediately focused when an errant form is submitted.

`tabindex` should be equal to or less than zero, NEVER above.

> Warning: You are recommended to only use 0 and -1 as tabindex values. Avoid using tabindex values greater than 0 and {{CSS}} properties that can change the order of focusable HTML elements (Ordering flex items). Doing so makes it difficult for people who rely on using keyboard for navigation or assistive technology to navigate and operate page content. Instead, write the document with the elements in a logical sequence.

Elements with `tabindex="-1"` can still be focused on using {{Javascript}} via `.focus()`, but again this should be avoided if at all possible.

### Interactive Elements

The browser will handle the user interaction natively and assistive tech will give special options for interactive elements.

-   `a` tags with an `href`, or elements with a `role="link"`
-   `button`, or elements with a `role="button"`
-   `form` and `input` elements (and all related, like checkboxes, etc.)

Often time, people use `div`s and `span`s to build custom buttons and things. They will **not** be focusable by default or interactive in the way that assistive tech users expect! This must be added manually via giving a `tabindex`[32], having a proper `role`, and adding event handlers for their equivalent keyboard interactions.

## SPAs

SPAs made with a framework like {{React}} put out standard HTML and thus should follow the same rules of accessibility for static sites. Be careful that you are using semantically correct HTML whenever possible and giving correct `aria` attributes when necessary.

### Focus and Title

In traditional/static websites, when moving to a different page (for instance, after following a link or submitting a form):

-   focus is placed at the start of the page
-   if running a screen reader, the title of the new page is announced
-   generally, the screen reader starts reading the content of the page from the top

If you have a single page application, these must be done explicitly, as dynamic content is being added/removed dynamically and no "new page" is being moved to in the eyes of the browser. When moving to a new page, you should ensure that:

-   the title is updated
-   focus is moved to an appropriate place, like that start of the page or the `h1` element

## Custom Components

If you are building custom components for the web, follow the ARIA Authoring Practices Guide[x] to make sure you are giving all users everything they need.

## Placeholders

Imagine your placeholders are gone. Do users with a cognitive disability have what they need to fill out your input? For instance, a date input that has an example date or the format as a placeholder is not accessible. You should include the format or instructions in the label if possible, and otherwise rely on `aria-labelledby` or `aria-describedby`.[44]

## References

1. https://webaim.org/resources/contrastchecker/
2. https://webaim.org/articles/contrast/
3. https://www.a11yproject.com/checklist/
4. https://www.w3.org/WAI/test-evaluate/#tools
5. https://seirdy.one/2020/11/23/website-best-practices.html
6. https://hidde.blog/more-common-a11y-issues/
7. https://sommerpanage.com/posts/2022-09-16-a11y_for_everyone/
8. https://testingaccessibility.com/
9. https://randoma11y.com/
10. [Make your website accessible and look ok](https://thomasorus.com/make-my-website-accessible-and-look-ok.html)
11. https://accessibility.huit.harvard.edu/describe-content-images
12. https://gomakethings.com/how-to-write-good-alt-text/
13. https://developer.mozilla.org/en-US/docs/Web/HTML/Element/figure
14. https://gomakethings.com/a-case-study-in-alt-text/
15. https://daisy.org/news-events/articles/art-science-describing-images-w/
16. https://inclusivepublishing.org/blog/the-art-and-science-of-describing-images-part-two-w/
17. https://inclusivepublishing.org/blog/the-art-and-science-of-describing-images-part-three-w/
18. https://www.smashingmagazine.com/2021/03/complete-guide-accessible-front-end-components/
19. https://hidde.blog/keyboard-shortcuts/
20. https://www.w3.org/WAI/WCAG21/quickref/?versions=2.1#character-key-shortcuts
21. [Best practices for making things work on older browsers](https://seirdy.one/posts/2020/11/23/website-best-practices/#old-browsers)
22. [Atlassian Design's Accessibility Guides](https://atlassian.design/foundations/accessibility)
23. [Accessible contrast finder](https://contrast-finder.tanaguru.com/)
24. [ARIA Spec for the Uninitiated](https://www.deque.com/blog/aria-spec-for-the-uninitiated-part-1/), [video](https://www.youtube.com/watch?v=O2F99bA32UU)
25. [W3C HTML Validator](https://validator.w3.org/nu/)
26. Derek Featherstone on [LinkedIn](https://www.linkedin.com/learning/instructors/derek-featherstone), and his [homepage](https://feather.ca/about/)
27. [Accessible Frontend Components](https://www.smashingmagazine.com/2021/03/complete-guide-accessible-front-end-components/)
28. [Understanding the Four Principles of Accessibility](https://www.w3.org/WAI/WCAG21/Understanding/intro#understanding-the-four-principles-of-accessibility)
29. [The 7 Principles of Universal Design](https://universaldesign.ie/What-is-Universal-Design/The-7-Principles/)
30. [Chrome Axe DevTools](https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd/related?hl=en-US)
31. [WebAIM's Guide to Conforming to WCAG2](https://webaim.org/standards/wcag/checklist), successor to [Section 508](https://webaim.org/standards/508/checklist)
32. [tabindex](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex)
33. [Focusability and tabbability of all elements](https://allyjs.io/data-tables/focusable.html)
34. [ARIA Authoring Practices Guide](https://www.w3.org/WAI/ARIA/apg/)
35. [Axess Lab | Alt-texts: The Ultimate Guide](https://axesslab.com/alt-texts/)
36. [Considerations when writing alt text | by Scott Vinkle | Shopify UX](https://ux.shopify.com/considerations-when-writing-alt-text-a9c1985a8204?gi=e8d7dd027ec8)
37. [WebAIM: Alternative Text](https://webaim.org/techniques/alttext/)
38. [Common accessibility issues that you can fix today | hidde.blog](https://hidde.blog/common-a11y-issues/)
39. [More common accessibility issues that you can fix today | hidde.blog](https://hidde.blog/more-common-a11y-issues/)
40. [A New Day: Making a Better Calendar - 24 Accessibility](https://www.24a11y.com/2018/a-new-day-making-a-better-calendar/)
41. [Alt Text as Poetry](https://alt-text-as-poetry.net/)
42. [Against Access by John Lee Clark](https://audio.mcsweeneys.net/transcripts/against_access.html) - On deaf-blind interpreters, alt text, etc.
43. [Web: How to document the screen reader user experience - Accessibility, Your Team and You](https://bbc.github.io/accessibility-news-and-you/guides/screen-reader-ux.html)
44. [Form Instructions | Web Accessibility Initiative (WAI) | W3C](https://www.w3.org/WAI/tutorials/forms/instructions/#in-line-instructions)
