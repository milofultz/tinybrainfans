---
title: Testing Accsesibility for the Web
description: Accessibility testing for web pages is a huge field, but a lot can be tested with very little.
---

Accessibility testing for web pages is a huge field, but a lot can be tested with very little. In general, if developing for accessible pages, aim for **perfect code first**. If you are using {{semantic HTML}} and matching visual order to DOM order, you have gone very far. In general, screen readers should be used for testing once you have ironed out the basics of the below items.

## Page Title

Your page titles should accurately reflect the page the user is on, from most specific to least specific. e.g. "Contact Us - Bob's Landscaping".

## Name, Role, and Value

Assistive technology will rely on three things to facilitate interaction with the user: name, role, and value. These elements can be found using the dev tools found in Chrome and other browsers under the "accessibility" tab of the elements browser. When a screen reader lands on an element, it will usually announce these three things in order.

### Name

The accessible name is what accessible technology uses to tell their users what they are focusing on. Deriving it is complex[1], but the name is more-or-less derived from the element's attributes in this order:

-   `aria-labelledby`
-   `aria-label`
-   the relevant naming attribute for the element: e.g. `title`, `alt` for an image, or `label` for a form element
-   using the preceding rules for inner elements
-   the inner content if the `role` allows[3]

Note that `aria-describedby` should not be used as a label. Users can turn it off as it is describing information _in addition to_ the accessible name. Accessible names are read first, followed by `aria-describedby`.

> While the aria-labelledby lists the ids of the labels or elements that describe the essence of an object, the aria-describedby lists the ids of the descriptions or elements providing more information that the user might need.[6]

### Role

The {{ARIA}} role is set automatically by each element[4]. For instance, a `button` element will have a role of `button`, an `img` will have a role of `img`, an `h1` is a `heading`, etc. These can also be set manually on an element via `role`, but should be avoided unless necessary.

The role defines the semantics of the element and how a user can interact with it. For example, if a button does not have a `button` role, they will not be able to focus on it with all the rest of the interactive elements, nor be able to tell that they can or should interact with it.

{{ARIA}} is supposed to map directly to HTML, but now it has advanced beyond HTML and so it might not map directly. W3 has a list of all element's implicit roles[4,8], but this can also be found more easily using the dev tools. If you want to search by role _first_, use the periodic table of semantics[4].

### Value

The value can be defined by the state or current condition of the component. The value is not always present or necessary. Some of the attributes include:

-   `checked`/`aria-checked`
-   `disabled`/`aria-disabled`
-   `hidden`/`aria-hidden` (note that this hides the element from screen reader users. Never use this on interactive elements or elements that should be hidden from all users)
-   `aria-pressed`
-   `aria-expanded`

### Description (interactive elements only)

Sometimes interactive elements have or can be given a description. This is additional information that is given to the user once the name, role, and value are given. This is added via `aria-describedby`, which has the `id` of an element that contains the description.

## Testing With A Screen Reader

In this, I will be referencing {{VoiceOver}} as I am using {{MacOS}} for development.

### Navigation

All elements should be navigated to by using the `VO` + arrow keys. This is known as "browse mode", and screen readers generally will manipulate the keyboard to operate in a way that most benefits users, which will remove some of the default keyboard behavior. This is important to do before the interactive elements, because mobile users who use a screen reader can't tab, so it is necessary to get to them using the arrow navigation.

A user should **only** be able to tab to interactive elements[7] (e.g. `a`, `input`, `button`, etc.). Note that this changes the mode of the screen reader to "forms mode", which gives all keyboard functionality back to what it natively expects.A

Use the rotor to ensure you can get around easily. You should be able to navigate through forms and headings easily here. If you don't see a heading or a form element in your rotor, you need to use proper {{semantic HTML}} or give your custom components the proper `role`.

When going through the elements, listen to what the screen reader says you can do and try doing that. For example, if it says "You are on a button. Press enter to press the button", you should test that pressing enter will in fact press the button.

## More Assistive Technology

-   Voice interaction (Dragon software, but both Windows and MacOS have it built in)
-   Switch-based user interface
-   Refreshable braille display

## References

1. [Accessible name calculation from WCAG](https://www.w3.org/TR/accname-1.1/)
2. [ARIA Roles](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles)
3. [Roles allowing name from content](https://www.w3.org/TR/wai-aria/#namefromcontent)
4. [Periodic table of semantics](https://gerardkcohen.github.io/periodic-table-of-semantics.html)
5. [Name Role and Value from W3C](https://www.w3.org/TR/UNDERSTANDING-WCAG20/ensure-compat-rsv.html)
6. [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby)
7. [List of interactive content in HTML](https://html.spec.whatwg.org/multipage/dom.html#interactive-content)
8. [List of all elements and their roles](https://www.w3.org/TR/html-aam-1.0/#html-element-role-mappings)
