---
title: MacOS
description: MacOS is the operating system for Apple computers.
---

## Reindex Spotlight

If you have problems where spotlight isn't finding applications or files for some reason, you can initiate the indexing manually via `sudo mdutil -E /`.

## References

1. [Reindexing for Spotlight](https://osxdaily.com/2012/02/02/reindex-spotlight-from-the-command-line/)

