---
title: Svelte
description: Svelte is a framework made to be fast and small.
---

Svelte is a {{framework|Frameworks (Javascript)}} made to be fast and small, with no external dependencies.

## References

1. [Svelte • Cybernetically enhanced web app](https://svelte.dev/)

