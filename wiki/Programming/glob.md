---
title: Glob
description: Glob is like regular expressions but for POSIX systems.
---

Glob is like {{regular expressions}} but for {{POSIX}} systems and is available in most {{shells|shell}}.

## Syntax

| Symbol  | Description                                           | Example                                        |
| ------- | ----------------------------------------------------- | ---------------------------------------------- |
| `*`     | Matches any number of any characters including none   | `*abc*` matches `abc`, `1abc2`, `xyzabc123`    |
| `?`     | Matches any single character                          | `?abc` matches `1abc`, not `abc`               |
| `[abc]` | Matches any character within the bracket              | `[123]abc` matches `1abc`, not `xabc` or `abc` |
| `[a-z]` | Matches any character within range (locale dependent) | `[0-9]abc` matches `8abc`, not `xabc` or `abc` |

### Extended Syntax

These are not standard, but are usually available for any given {{shell}}. The `{{bash}}` option is in parentheses. More than one `pattern` can exist within a set of parentheses for `extglob`, as long as they are separated by a pipe (`|`).

| Symbol                   | Description                                          | Example                                                                                |
| ------------------------ | ---------------------------------------------------- | -------------------------------------------------------------------------------------- |
| `**` (`globstar`)        | Matches any number of any directories including none | `folder/**/*.txt` matches `folder/abc.txt`, `folder/subfolder/abc.txt`, not `/abc.txt` |
| `*(pattern)` (`extglob`) | Matches zero or more occurrences of a given pattern  | `a*(b\|c)d` matches`abcd`, `ad`, `abbd`                                                |
| `?(pattern)` (`extglob`) | Matches zero or one occurrence of a given pattern    | `a?(b\|c)d` matches`ad`, `abd`, `acd`                                                  |
| `+(pattern)` (`extglob`) | Matches one or more occurrence of a given pattern    | `a+(b\|c)d` matches`abd`, `abcccccd`, not `ad`                                         |
| `@(pattern)` (`extglob`) | Matches one occurrence of a given pattern            | `a@(b\|c)d` matches`abd`, `acd`, not `abcd`                                            |
| `!(pattern)` (`extglob`) | Matches anything _except_ the given pattern          | `a!(b\|c)d` matches`aed`, `abcccccd`, not `ad`                                         |

## References

1. [glob - Greg's Wiki](http://mywiki.wooledge.org/glob#extglob)
2. [glob (programming) - Wikipedia](<https://en.wikipedia.org/wiki/Glob_(programming)
3. [Glob tester - tool for testing glob pattern](https://globster.xyz/)
