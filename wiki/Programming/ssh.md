---
title: ssh and scp
description: ssh (secure shell) is a way to connect to remote computers and interact with them. scp (secure copy) is used to send/request files.
---

## Sending a single or a few files

```bash
scp /path/to/src/file user@server:/path/to/destination
```

## Sending lots of files at once[1]

```bash
tar -C /path/to/src/dir -jcf - ./ | ssh user@server 'tar -C /path/to/dest/dir -jxf -'
```

* For `{{tar}}`, the flags are: `j` for filtering/compressing using the `bzip2` algorithm, `c` for creating a new archive, and `f` is for where it should send the new contents of the tar file; the hyphen signifies the contents of the new tar file should be piped.
* For the `ssh` section, the other `tar` flag is `x` for extract the archive.

## References

1. https://unix.stackexchange.com/a/10037

