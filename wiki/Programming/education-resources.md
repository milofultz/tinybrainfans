---
title: Resources for Learning
---

## References

1. https://teachyourselfcs.com/
2. https://www.theodinproject.com/
3. https://github.com/mtdvio/every-programmer-should-know
4. https://www.hillelwayne.com/post/learning-a-language/
5. https://www.amazon.com/Best-Software-Writing-Selected-Introduced/dp/1590595009

