---
title: Applescript
description: Applescript is a programming language used to create scripts in Mac computers.
---

Applescript is a programming language used to create scripts in {{Mac|MacOS}} computers.

## Generic Syntax

| Command                                               | Effect                                       |
|-------------------------------------------------------|----------------------------------------------|
| `-- This is a single line comment`                    | Single line comment                          |
| `# This is another single line comment`               | Single line comment                          |
| `(* This is a multi line comment *)`                  | Multi-line comment                           |
| `open location "http://www.zombo.com"`                | Open URL with the appropriate program[5,6]   |
| `do shell script "say \"we did it\""`                 | Run arbitrary shell script[3]                |
| `do shell script "..." with administrator privileges` | Run arbitrary shell script using sudo        |
| `set variableName to value`                           | Set a variable                               |
| `tell application "iTerm" to ...`                     | Tell application to do something (see below) |

### Destructure a list to new variables

```applescript
set newList to {100, 200, 300}
set { x, y, z } to newList # x=100, y=200, z=300
set a to item 2 of newList # a=200
```

### Conditionals[11]

```applescript
if true then
  # do stuff
else if false then
  # do other stuff
else
  # do another thing
end if
```

## Tell System To Do Something

### Dialog Box[9]

```applescript
set theDialogText to "The curent date and time is " & (current date) & "."
display dialog theDialogText
```

### Get User Input[10]

```applescript
set theResponse to display dialog "What's your name?" default answer "" with icon note buttons {"Cancel", "Continue"} default button "Continue"
--> {button returned:"Continue", text returned:"Jen"}
display dialog "Hello, " & (text returned of theResponse) & "."
```

## Tell Application To Do Something

```applescript
tell application "iTerm" to # put single action here

tell application "iTerm"
  # put multiple actions here
end tell
```

### Actions

- `activate` - Run program
- `open "Macintosh HD:Users:username:..."` - Open file at filepath[3]
  - To open files using standard notation (`/Users/username/...`), preface the filename with `POSIX file`: `open POSIX File "/Users/username/..."`
- `close window 1` - Close window
- `set size of front window to {640, 400}` - Set window size {x, y}
- `set position of front window to {0, 0}` - Set window position {x, y}
- `set bounds of front window to {300, 30, 1200, 900}` - Set window size and position {X-start, Y-start, X-end, Y-end}; corresponds directly to pixels of display resolution

## Droplets[13]

A droplet is an application that performs actions on files or folders that the user drops onto the application itself in the Finder. This is useful if you are scripting for people who aren't power users/terminal users or for simple tasks.

To signify that an application is a droplet, your code must include an `open` event handler.

```applescript
on open theDroppedItems
    -- Process the dropped items here
end open
```

## Test If Application Is Running

If you look in Activity Monitor, you can find processes by name. Use this to see if there are any running. I use this in a shell to start an app if not running and close if currently running.

```applescript
tell app "System Events" to count processes whose name is "Finder"
```

## Programmatically Click Around A Window

```applescript
-- Turn on/off VoiceOver

-- Start the application
activate application "System Preferences"
tell application "System Preferences"
    -- Set pref pane (see [14])
	set current pane to pane id "com.apple.preference.universalaccess"
    -- This is how you handle interaction, using "System Events"
	tell application "System Events"
		tell process "System Preferences"
            -- Ensure UI elements exist before trying to interact
            -- You can find these UI eleemnts using the method mentioned below[15]
			repeat until (exists row 3 of table 1 of scroll area 1 of window 1)
				delay 0.1
			end repeat
            -- You have to *select* rows or tables, not click them. Discovered this
            -- via getting the `properties` of a UI element
			select row 3 of table 1 of scroll area 1 of window 1
			repeat until (exists checkbox "Enable VoiceOver" of group 1 of window 1)
				delay 0.1
			end repeat
			click checkbox "Enable VoiceOver" of group 1 of window 1
		end tell
	end tell
    -- Close window when finished
	quit
end tell
```

## Finding UI Elements[15]

```applescript
tell application "System Events" to get UI elements of window 1 of process "AppName"
# Once you have something you want to look at further, you can select it
tell application "System Events" to get UI elements of table 1 of window 1 of process "AppName"
# Or you can go further out
tell application "System Events" to get UI elements of UI elements of window 1 of process "AppName"
# Find out properties of these elements to better interact with them
tell application "System Events" to get properties of table 1 of window 1 of process "AppName"
```

## References

1. http://downloads.techbarrack.com/books/programming/AppleScript/website/files_&_folders/opening_files.html
2. [Error Messages](https://developer.apple.com/library/archive/documentation/AppleScript/Conceptual/AppleScriptLangGuide/reference/ASLR_error_codes.html)
3. https://stackoverflow.com/questions/10126661/applescript-how-to-open-a-file-with-the-default-program
4. https://developer.apple.com/library/archive/documentation/AppleScript/Conceptual/AppleScriptLangGuide/introduction/ASLR_intro.html
5. https://developer.apple.com/library/archive/documentation/AppleScript/Conceptual/AppleScriptLangGuide/reference/ASLR_cmds.html#//apple_ref/doc/uid/TP40000983-CH216-SW54
6. https://apple.stackexchange.com/questions/293777/how-do-i-open-a-generic-url-from-applescript
7. https://wiki.keyboardmaestro.com/AppleScript
8. [Switch audio devices](https://apple.stackexchange.com/a/416521)
9. [Display dialog text](https://developer.apple.com/library/archive/documentation/LanguagesUtilities/Conceptual/MacAutomationScriptingGuide/DisplayDialogsandAlerts.html)
10. [Getting user input](https://developer.apple.com/library/archive/documentation/LanguagesUtilities/Conceptual/MacAutomationScriptingGuide/PromptforText.html)
11. [If else](https://computers.tutsplus.com/tutorials/if-and-if-else-applescript-conditional-statements--mac-45590)
12. [Applescript fundamentals](https://developer.apple.com/library/archive/documentation/AppleScript/Conceptual/AppleScriptLangGuide/conceptual/ASLR_fundamentals.html)
13. [Droplets](https://developer.apple.com/library/archive/documentation/LanguagesUtilities/Conceptual/MacAutomationScriptingGuide/ProcessDroppedFilesandFolders.html)
14. [AppleScript: Scriptable System Preference](https://macosxautomation.com/applescript/features/system-prefs.html)
15. [Finding UI elements to interact with using Applescript](https://apple.stackexchange.com/questions/193908/applescript-gui-scripting-and-setting-checkbox-radio-button-and-field-values#comment231462_193948)
16. [Using AppleScript to modify settings/system preferences - Stack Overflow](https://stackoverflow.com/questions/49214368/using-applescript-to-modify-settings-system-preferences)

