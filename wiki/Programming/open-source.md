---
title: Open Source Software
description: Open source software is complicated.
---

> I am not your supplier. So all your Software Supply Chain ideas? You are not buying from a supplier, you are a raccoon digging through dumpsters for free code. So I would advise you to put these rules in the same dumpster. And remember. I am not a supplier. Because THIS SOFTWARE IS PROVIDED “AS IS”[1]

## What Is OSS

> Open-source software (OSS) is computer software that is released under a license in which the copyright holder grants users the rights to use, study, change, and distribute the software and its source code to anyone and for any purpose.[2]

Open source software has been a boon for software development, allowing the velocity of development to increase quite quickly thorugh sharing and distribution of resources. And while this feels all well and utopian, this paradigm has serious drawbacks:

* OSS devs rarely get almost anything for their work, aside from praise (see almost any {{npm}} package)
* Companies stand to gain the most from using OSS but rarely do they put in much of any profits back into the packages they use, especially considering the chain of dependencies and that *they* should be paid, too
* OSS devs often feel obligated to maintain software they receive nothing from because people "need" it (and "don't you care about open source's mission?", etc. etc.)
* Projects based off of OSS often have single points of failure where the maintainer has no obligations to support it (or you) for any reason (see `leftpad`[3], `colors`[4], or `log4j`[5]) and also maintain no liabilities or warranties. If *one old package* gets messed up, you can have a **ton** of broken dependencies, often cascading into one another with unfixable issues.
* The package managers that de-facto maintain the open source ecosystem are huge megacorps like Microsoft, Facebook, and Google (see above re: having the most to gain)

## References

1. [I Am Not A Supplier](https://www.softwaremaxims.com/blog/not-a-supplier)
2. https://en.wikipedia.org/wiki/Open-source_software
3. https://www.theregister.com/2016/03/23/npm_left_pad_chaos/
4. https://www.revenera.com/blog/software-composition-analysis/the-story-behind-colors-js-and-faker-js/
5. https://en.wikipedia.org/wiki/Log4Shell

