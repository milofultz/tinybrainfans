---
title: Competitive Programming
description: Competitive programming is a tool used (erroneously) by many employers to test whether you are good at programming.
---

Competitive programming is a tool used (erroneously) by many employers to test whether you are good at programming. However, this doesn't mean that working on and solving those problems isn't {{fun|Programming}}.

Working on these problems will elucidate the faults in your {{schemas|PI AT PC Framework}} for {{problem solving}}, and create recognizable mental models that you can draw parallels to when solving novel problems.

## Best Practices

* The most important part of these challenges is **solving the problem**. Start off by solving it, and then optimize.
* Hashmaps and binary search are usually a safe bet.

## References

1. https://yewtu.be/channel/UC_mYaQAE6-71rjSN6CeCA-g
2. https://neetcode.io/
3. https://seanprashad.com/leetcode-patterns/
4. https://leetcode.com
5. https://www.codewars.com/
6. https://projecteuler.net/
7. https://adventofcode.com/
