---
title: AWS Deployment
description:
---

> Shamelessly taken from Eric Pearson [@nyloneric](https://gist.github.com/NylonEric/372516f2fe3c9f708d20e7e2f77007cb#file-2021-06-23-md), as I haven't documented my own process yet.

## Getting Started

https://aws.amazon.com/ec2/getting-started/

Start an EC2 instance with Amazon Linux and t2 micro.  Set up security policy on AWS EC2 I started with everything open and then restricted to port 80 UDP/TCP traffic for incomming. I also opened ICMP traffic for pinging purposes as well as ssh port 22 for logging into server.

Set up elastic IP so it will persist after reboot/termination.

## Logging in

Each Linux instance launches with a default Linux system user account. The default user name is determined by the AMI that was specified when you launched the instance.

- For **Amazon Linux 2** or the Amazon Linux AMI, the user name is **ec2-user**.
- For a **CentOS** AMI, the user name is **centos**.
- For a **Debian** AMI, the user name is **admin**.
- For a **Fedora** AMI, the user name is **ec2-user** or **fedora**.
- For a **RHEL** AMI, the user name is **ec2-user** or **root**.
- For a **SUSE** AMI, the user name is **ec2-user** or **root**.
- For an **Ubuntu** AMI, the user name is **ubuntu**.
- Otherwise, if ec2-user and root don't work, check with the AMI provider.

Start up EC2[1] and SSH from local: set permissions to PEM file: `chmod 400 hr-nylon-eric.pem`

`ssh -i hr-nylon-eric.pem ec2-user@ec2-35-172-51-196.compute-1.amazonaws.com`

Then navigate to and create folder for project:

```bash
git clone https://github.com/Idakka/Project-Atelier.git
sudo yum install git
```

## Setting up Node[7]

Install NVM to handle versioning in Node.

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
```

At this point, log out of the EC2 instance and back in to continue.

```bash
# install node
nvm install node
# initiate nvm with new node version
. ~/.nvm/nvm.sh
# ensure desired version of node is running
node -e "console.log('Running Node.js ' + process.version)"
# Install NPX if you need it
npm install -g npx
```

## Setup Database ({{MongoDB}})

```bash
# Install Mongo using their package management system:
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
# Create a list file for MongoDB
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
# Reload local package database
sudo apt-get update
# Install MongoDB packages
sudo apt-get install -y mongodb-org
```

Now the DB and software should be installed, so now we can start it up.

```bash
sudo systemctl start mongod
# Verify it is running
sudo systemctl status mongod
# Stop/restart
sudo systemctl stop mongod
sudo systemctl restart mongod
# Use the shell to test install worked correctly
mongosh
```

Copy any DB data to instance. You can use `scp`[9], or something like Filezilla[8] or Cyberduck. `scp` looks like this:

```bash
scp -i /directory/to/abc.pem user@ec2-xx-xx-xxx-xxx.compute-1.amazonaws.com:path/to/file /your/local/directory/files/to/download
```

## Router/Reverse Proxy:

```bash
sudo amazon-linux-extras install nginx1
sudo service nginx start
# message: Redirecting to /bin/systemctl start nginx.service
cat /etc/nginx/sites-available/default
```

This didn't work and I had to go find the `nginx.conf` file and edit it with nano: `sudo nano /etc/nginx/nginx.conf`. Most of this config was filled out, Just needed to add the location block and change to port 1234 for our server

```conf
server {
    listen 80;
    server_name tutorial;
    location / {
        proxy_set_header  X-Real-IP  $remote_addr;
        proxy_set_header  Host       $http_host;
        proxy_pass        http://127.0.0.1:3000;
    }
}
```

## Link config files

`sudo service nginx restart`

## Set Up App

Use NPM to install all project dependencies necessary.  NPM start in project folder, needed to install nodemon

## Keep The App Running

https://medium.com/hackernoon/tutorial-creating-and-managing-a-node-js-server-on-aws-part-2-5fbdea95f8a1#.mnlkymeti

```
npm i -g pm2
pm2 start tutorial/index.js
```

To make sure that your PM2 restarts when your server restarts `pm2 startup`

You will be given a custom string similar to this but specific to your machine, copy that:

```
sudo env PATH=$PATH:/home/ec2-user/.nvm/versions/node/v16.4.0/bin
/home/ec2-user/.nvm/versions/node/v16.4.0/lib/node_modules/pm2/bin/pm2
startup systemd -u ec2-user --hp /home/ec2-user
```

Save config with everything running (no nginx at this point) `pm2 save`

```
pm2 start nodemon server/server.js
```

That’s it! You can log out/in to SSH, even restart your server and it will continue to run on port 80.

- To list all processes use `pm2 ls`

## Make NGINX Persistent

```
systemctl start nginx
systemctl enable nginx
```

Having trouble restarting nginx on reboot, `sudo systemctl enable nginx` is supposed to solve it. needed to start nginx/make persistant and THEN redo the pm process

## Run It All

```bash
sudo systemctl start mongod
sudo service nginx start
```

## References

1. https://jasonwatmore.com/post/2019/11/18/react-nodejs-on-aws-how-to-deploy-a-mern-stack-app-to-amazon-ec2
2. https://jasonwatmore.com/post/2018/09/26/setup-nodejs-mongodb-production-server-on-ubuntu-1804
3. https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
4. https://stackoverflow.com/questions/33991816/ec2-ssh-permission-denied-publickey-gssapi-keyex-gssapi-with-mic
5. https://medium.com/@andrewcbass/install-redis-v3-2-on-aws-ec2-instance-93259d40a3ce
6. https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html
7. https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html
8. https://ganevniko.github.io/using_filezilla_to_upload_files_into_an_aws_ec2_instance
9. https://dearsikandarkhan.medium.com/files-copying-between-aws-ec2-and-local-d07ed205eefa

