---
title: Semantic Versioning
description: Semantic versioning is a way to standardize versioning for package managers.
---

Semver is used in popular package managers like {{npm}}/{{yarn}}. Semantic versioning uses the following formula:

`1.2.3` -> `MAJOR.MINOR.PATCH`

> Given a version number MAJOR.MINOR.PATCH, increment the:
>
> MAJOR version when you make incompatible API changes
> MINOR version when you add functionality in a backwards compatible manner
> PATCH version when you make backwards compatible bug fixes[1]

Or put another way:

-   MAJOR is for **any breaking changes**
-   MINOR is for **adding something** that doesn't break or change anything else
-   PATCH is for **fixing something** that doesn't break or change anything else

## Ranges

Greater than/less than is the way to most clearly specify ranges.

-   `>=1.0.0 <2.0.0` means at least major version 1 but nothing above major version 2.
-   `>=1.2.3 <4.5.6` means at least major version 1, minor version 2, and patch version 3, but nothing above major version 4, minor version 5, and patch version 6.

The following are sugar for the above so you don't have to write so much.

-   Hyphens specify an inclusive set (`1.2.3 - 2.3.4`). Any missing numbers or `x`/`*` is essentially replaced by a zero (`1.x` === `1.0.0`).
-   A tilde means don't go above the specified **minor**. "major and minor versions must match those specified, but any patch version greater than or equal to the one specified is valid"[4]
    -   `~1.2.3 := >=1.2.3 <1.3.0-0`
    -   `~0.2.3 := >=0.2.3 <0.3.0-0`
-   A caret means don't go above the level on the right of the left-most zero.
    -   `^1.2.3 := >=1.2.3 <2.0.0-0`
    -   `^0.2.3 := >=0.2.3 <0.3.0-0`
    -   `^0.0.3 := >=0.0.3 <0.0.4-0`

## References

1. https://semver.org/
2. https://github.com/npm/node-semver
3. [Semver cheatsheet](https://devhints.io/semver)
4. [Semver: Tilde and Caret - NodeSource](https://nodesource.com/blog/semver-tilde-and-caret/)
