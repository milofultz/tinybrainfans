---
title: Yarn Resolutions
description: This is a way to control the versions of dependencies.
---

For example:

```json
  "dependencies": {
    "package-a": "1.0.0",
    "package-b": "1.0.0"
  },
  "resolutions": {
    "**/package-d1": "2.0.0"
  }
```

This would render all versions of `package-d1` in all packages to the same version.

> A resolution contains on the left-hand side a glob pattern applied to the dependency tree (and not to the node_modules directory tree, since the latter is the result of yarn resolution being influenced by the resolution).
>
> -   a/b denotes the directly nested dependency b of the project's dependency a.
> -   \*\*/a/b denotes the directly nested dependency b of all the dependencies and nested dependencies a of the project.
> -   a/\*\*/b denotes all the nested dependencies b of the project's dependency a.
> -   \*\*/a denotes all the nested dependencies a of the project.
> -   a is an alias for \*\*/a (for retro-compatibility, see below, and because if it wasn't such an alias, it wouldn't mean anything as it would represent one of the non-nested project dependencies, which can't be overridden as explained below).
> -   ** denotes all the nested dependencies of the project (a bad idea mostly, as well as all other designations ending with **).

## References

1. [Selective Versioning](https://github.com/yarnpkg/rfcs/blob/master/implemented/0000-selective-versions-resolutions.md)
