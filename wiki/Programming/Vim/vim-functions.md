---
title: Vim Functions
description: Vim functions can be super powerful, if you learn how vimscript works.
---

## References

1. https://www.bobbywlindsey.com/2017/07/30/vim-functions/
2. https://learnxinyminutes.com/docs/vimscript/
3. https://devhints.io/vimscript
4. https://learnvim.irian.to/vimscript/vimscript_functions/

