---
title: Lisp
description: Lisp is a high-level programming language mainly consisting of s-expressions.
---

Lisp is a high-level {{programming}} language mainly consisting of s-expressions.

## References

1. https://en.wikipedia.org/wiki/Lisp_(programming_language)
2. https://github.com/Robert-van-Engelen/tinylisp
