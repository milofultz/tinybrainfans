---
title: Making a Specification
description: A specification is the contract you set out for a given piece of software.
---

Writing a specification for your software is a great way to ensure that you deliver _exactly_ what is desired and not what isn't, to avoid scope creep, and to solve lots of problems before you get to the coding process. This is a more elaborate and specific way to tackle more complex problems than the {{PI AT PC framework}} and a way to prepare for something like {{hammock driven development}}.

## References

1. [Software design description - Wikipedia](https://en.wikipedia.org/wiki/Software_design_description)
