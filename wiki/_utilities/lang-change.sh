#!/bin/bash

wiki_dir="$(dirname "$0")/.."
cd "$wiki_dir"

if [[ -z $1 ]] || [[ -z $2 ]]; then
  printf "from- and to-lang are required.\nUsage: %s [from-lang] [to-lang]\n" "$0"
  exit
fi

set -euo pipefail

from_lang="$1"
to_lang="$2"

query="\`\`\`${from_lang}"
output="\`\`\`${to_lang}"

matching_files=$(find "$wiki_dir" -name '*.md' -exec grep -l "$query" {} \;)

if [[ -n "$matching_files" ]]; then
    echo "$matching_files" | while read -r file; do
        sed -i '' -e "s/${query}/${output}/g" "$file"
    done
    printf "Edited the following files:\n\n%s" "$matching_files"
fi

# find ./ -type f -name "*.md-e" -exec trash {} \;

