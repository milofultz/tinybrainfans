---
title: Best Practices
description:
---

## Don't Work With Psychopaths

> I saw a tweet today, asking “If you could tell one piece of advice to yourself at 21, what would it be?” and without hesitation I thought, don’t work with psychopaths. You’d think you could handle it but you’d be wrong. I always thought I could, and I was wrong. You can’t outsmart a pathology.[1]

## Diary and Delta File

This is a two parter but I keep them together because it's easier.

### Diary

Keep a diary of what you do every day. This has a ton of benefits:

- Keeping you on track of what you did the previous day so you can make progress the next day
- Deeper knowledge of context of your work and why you did it
- A list of accomplishments and work done for promotion preparation/job searches (e.g. {{STAR Stories}})
- Real-time rubber ducking problems via conversation with yourself
- Questions that you don't have time to answer now but could benefit you later
- Links to cover your ass if shit hits the fan and the blame falls on you for something that was done

### Delta File

Maintain a written document, for your eyes only, where you capture observed behaviors (and consequences) that you find objectionable in yourself and others. I find this useful for keeping yourself sane in cases of being gaslit by your employer, as well as documenting behavior that *should* be documented for future reference. You never know when you will need these details and they could 100% save your ass someday.

> The purpose is to understand, change or act differently should you find yourself in the same position. It allows you to reflect over time on actions that you find problematic; sometimes you learn that you were naïve and when presented in the same situation you then understand why a person acted in a certain way, other times you can make a different choice and rise to the occasion — it helps you prepare for that moment.
>
> A short-term side benefit of the Delta File, is how it’s a great way to let go of tolerable crap you have to deal with. When you see something objectionable and there is nothing you or anyone can do about it, just writing it down and acknowledging it’s “a bad thing” can help you handle the emotional burden.

## References

1. [Don’t work with psychopath](https://scribe.rip/@livlab/dont-work-with-psychopaths-b1a848914455)
2. [The Delta File | Manager Tool](https://www.manager-tools.com/2009/08/delta-file)

