---
title: Responsibility
description: What is our responsibility as workers?
---

## Work Is Not Family

> We were proud of working there, and we loved being part of a big and impactful product. But no, this wasn’t a family gathering and nowhere in our contracts were we asked to do a song and dance when the company name is mentioned. Many a contract would have stayed unsigned if that had been the case.

## On Being Indispensable

Being in charge and having responsibility can come with a high cost when you aren't in charge of your time.

> The best advice I got starting out was, "be indispensable." The best advice I got four years later was, "don't be indispensable."[1]

## References

1. https://news.ycombinator.com/item?id=30671345
2. https://christianheilmann.com/2022/10/06/unless-youre-in-the-mafia-your-company-isnt-your-family/
3. https://developer-advocacy.com/

