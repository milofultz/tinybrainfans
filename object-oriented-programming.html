<!doctype html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="X-UA-Compatible" content="ie=edge"><meta name="description" content="Object-oriented programming is a way to structure programs using reusable classes and objects."><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - Object-Oriented Programming</title><link rel="stylesheet" href="tinystyle.css"><link rel="stylesheet" href="prism.css"></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">Object-Oriented Programming</h1><p><em>This page is using <a href="python.html">Python</a> syntax, but all this information is applicable for any language that has OOP features, like <a href="ruby.html">Ruby</a>, <a href="java.html">Java</a>, <a href="classes-javascript.html">Javascript</a>, etc.</em></p>
<h2>What Is It</h2>
<blockquote>
<p>Object-oriented programming is a programming paradigm that provides a means of structuring programs so that properties and behaviors are bundled into individual objects.[1]</p>
</blockquote>
<p>Object oriented programming is a way to organize data and actions according to what they represent, like a person. These &quot;prototypes&quot; of things are called <strong>classes</strong> and instances of these classes are called <strong>objects</strong>.</p>
<p>For instance, you could create a generic <code>Person</code> class that has properties like <code>name</code> and <code>age</code>, and methods like <code>say_hello</code>, which you could instantiate an object for each new &quot;person&quot; you have in your program.</p>
<h2>Classes</h2>
<p>A class is a blueprint of what should exist within an instance. Classes contain <strong>properties</strong> and <strong>methods</strong>, where properties are pieces of information, and methods are functions. However, classes contain no data on their own (they can contain default information that will be passed down to an instance, but the key of classes are their ability to create instances).</p>
<p>In the following example, the <code>Person</code> class has <code>name</code> and <code>age</code> properties and a <code>say_hello</code> method, but they don't yet have a <code>name</code> or <code>age</code> since they have not been instantiated yet. When they are instantiated, the properties will be defined, and the methods will have some information they can use.</p>
<p>When a class is instantiated, it runs a <em>constructor</em>, which in <a href="python.html">Python</a> is written as <code>def __init__(self, ...)</code> with all parameters passed in on instantiation as the arguments in the constructor.</p>
<pre><code class="language-python">class Person:
    def __init__(self, name: str, age: int):
        self.name = name
        self.age = age

    def say_hello(self):
        print(f&quot;Hi!!! My name is {self.name}!&quot;)

    def greet(self, persons_name: str):
        print(f&quot;Hello, {persons_name}!&quot;)
</code></pre>
<h3>Class Attributes</h3>
<p>Unlike properties, which are defined from instance to instance, class attributes are set to the value that will be used for all instances of said class. In our example above, we could set a class attribute of <code>species</code> to <code>human</code>, since we know that all instances of <code>Person</code> will be of the human species.</p>
<pre><code class="language-python">class Person:
    # Class Attribute
    species = &quot;human&quot;

    def __init__(self, name: str, age: int):
      # ... same as above
</code></pre>
<h3>Abstract Classes and Methods[2-4]</h3>
<p>An abstract class is a class that cannot be instantiated by itself, and can only be used as a parent class. An abstract method is similar in that the abstract method cannot be called on the class in which it was instantiated, but only in the child class. This abstraction is used to aid in maintenance of child classes or the function of methods or in a child class.</p>
<blockquote>
<p>If an abstract method is declared in a superclass, subclasses that inherit from the superclass must have their own implementation of the method.[2]</p>
</blockquote>
<p>For instance, if we defined an abstract class <code>Shape</code>, we would not be able to create an instance of <code>Shape</code> itself. We would need to create a child class, like <code>Circle</code>, and then instantiate that. Similarly, if we had a class of <code>Dog</code> and created an abstract method of <code>bark</code>, we would not be able to call <code>bark</code> on an instance of <code>Dog</code>; we would have to create a child class of <code>Dog</code> first, like <code>Labrador</code>, and define that method within that child class.</p>
<p>Python uses what is called <code>abc</code> or abstract base classes:</p>
<blockquote>
<p>Abstract base classes are a form of interface checking more strict than individual hasattr() checks for particular methods. By defining an abstract base class, a common API can be established for a set of subclasses. This capability is especially useful in situations where someone less familiar with the source for an application is going to provide plug-in extensions, but can also help when working on a large team or with a large code-base where keeping track of all of the classes at the same time is difficult or not possible.[4]</p>
</blockquote>
<h2>Instances</h2>
<p>An instance of a class will contain all new instance variables passed into it when instantiated, as well as all the class attributes defined by the class.</p>
<pre><code class="language-python">chelsea = Person(&#x27;Chelsea&#x27;, 23)
print(chelsea.age) i # prints 23
chelsea.say_hello() # prints &quot;Hi!!! My name is Chelsea!&quot;
bob = Person(&#x27;Bob&#x27;, 12)
print(bob.species)  # prints &quot;human&quot;
bob.say_hello()     # prints &quot;Hi!!! My name is Bob!&quot;
</code></pre>
<p>Instance values can be updated dynamically, as well. Let's say it was Bob's birthday today and we wanted to update their age by 1:</p>
<pre><code class="language-python">bob.age += 1
print(bob.age) # prints 13
</code></pre>
<h2>Inheritance</h2>
<p>One of the main concepts of object-oriented programming is the ability of child classes to <em>inherit</em> from a parent class. When a child class inherits from a parent class, it inherits all of the parent class's properties and methods.</p>
<p>Here, we have created an <code>Adult</code> child class using the parent class <code>Person</code>. We will receive all of <code>Person</code>'s properties and methods, and we have added a new method to reflect the unique properties of an <code>Adult</code>.</p>
<pre><code class="language-python"># In Python, this shows that `Adult` &quot;extends&quot; the `Person` class
class Adult(Person):
    def pay_taxes(self):
        self.age += 1

jane = Adult(&#x27;Jane&#x27;, 35)
jane.say_hello() # prints &quot;Hi!!! My name is Jane!&quot;
print(jane.age)  # prints 35
jane.pay_taxes()
print(jane.age)  # prints 36
</code></pre>
<h3>Extending Parent Methods aka Polymorphism</h3>
<p>The child classes can also <em>extend</em> the functionality of the parent class by changing or overriding the parent class's method; you can also alter the child class's method by calling the parent class's method and using it differently. If the parent class changes a method, this will be propagated down to all child classes as well, unless it has been changed in that child class.</p>
<p>This is called polymorphism[6] and refers to the ability to call the same method on different child, sibling, or parent classes and have it perform different operations and/or return different results.</p>
<pre><code class="language-python">class Adult(Person):
    def pay_taxes(self):
        self.age += 1

    def say_hello(self):
        print(f&quot;Greetings, I am {self.name}.&quot;)

    def greet(self):
        super().greet(&quot;there&quot;)

jane = Adult(&#x27;Jane&#x27;, 35)
jane.greet() # prints &quot;Hello, there!&quot;
</code></pre>
<h2>Method Overloading</h2>
<p><em>Note: this is not a feature in Python's implementation of OOP, so we will be using <a href="java.html">Java</a> in this example[5]</em></p>
<p>Method overloading is when multiple instances of a method are created with differing types or amounts of arguments. When a method is called, <em>which</em> of the multiple versions of a method will be invoked is resolved by determining the different elements of the invoked method.</p>
<pre><code class="language-java">class Adder {
  static int add (int a, int b) {
    return a + b;
  }

  static int add (int a, int b, int c) {
    return a + b + c;
  }

  static double add (double a, double b) {
    return a + b;
  }
}

class TestOverloading1 {
  public static void main (String[] args) {
    System.out.println(Adder.add(11, 11));
    // Output: 22
    System.out.println(Adder.add(11, 11, 11));
    // Output: 33
    System.out.println(Adder.add(11.1, 11.1));
    // Output: 22.2
  }
}
</code></pre>
<h2>References</h2>
<ol>
<li><a href="https://realpython.com/python3-object-oriented-programming/" target="_blank">https://realpython.com/python3-object-oriented-programming/</a></li>
<li><a href="https://www.freecodecamp.org/news/object-oriented-programming-in-python/" target="_blank">https://www.freecodecamp.org/news/object-oriented-programming-in-python/</a></li>
<li><a href="https://www.geeksforgeeks.org/abstract-classes-in-java/" target="_blank">Basic explanation of abstract classes and methods</a></li>
<li><a href="https://pymotw.com/3/abc/" target="_blank">https://pymotw.com/3/abc/</a></li>
<li><a href="https://www.javatpoint.com/method-overloading-in-java" target="_blank">https://www.javatpoint.com/method-overloading-in-java</a></li>
<li><a href="https://en.wikipedia.org/wiki/Polymorphism_(computer_science)" target="_blank">https://en.wikipedia.org/wiki/Polymorphism_(computer_science)</a></li>
</ol>
<section id="incoming"><details open><summary>Incoming Links</summary><ul><li><a href="classes-javascript.html">Classes (Javascript)</a></li></ul></details></section><p class="last-modified">Last modified: 202212070107</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>Built using <a href="http://codeberg.org/milofultz/swiki" rel="noopener">{{SWIKI}}</a></li><li><a href="http://codeberg.org/milofultz/" rel="noopener">Codeberg</a></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener">Mastodon</a></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a><a href="https://fediring.net/">Fediring</a><a href="https://fediring.net/random" style="color: red;text-decoration: green wavy underline;">&nbsp;!&nbsp;</a><a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
