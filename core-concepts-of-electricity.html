<!doctype html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="X-UA-Compatible" content="ie=edge"><meta name="description" content="These are the basics of electrical engineering; terminology and concepts, mainly."><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - Core Concepts of Electricity</title><link rel="stylesheet" href="tinystyle.css"><link rel="stylesheet" href="prism.css"></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">Core Concepts of Electricity</h1><p><span style="color: red;"><strong>Electricity can kill you!</strong></style> Use extreme caution at all times!</p>
<p>Keep antennas and feed lines far away from power lines and utility poles; at least ten feet away if it were to fall down.</p>
<h2>Current</h2>
<p>Current (<code>I</code>) is the flow of electrons, and is measured in amperes (<code>A</code>) (or amps). Materials with ample free electrons are good conductors for electric current.</p>
<p>Current in a single direction is direct current or DC.  Current in alternating directions is alternating current or AC. The following examples show the dotted line as the midline and the solid line as the current.</p>
<pre><code>DC:

 1  ───────────────────
 0  - - - - - - - - - -
-1

AC:

 1 ─┐ ┌─┐ ┌─┐ ┌─┐ ┌─┐
 0 -│-│-│-│-│-│-│-│-│-
-1  └─┘ └─┘ └─┘ └─┘ └─
</code></pre>
<p>Note that the current is not dropping <em>to</em> zero but <em>through</em> zero to -1 in alternating current.</p>
<p>The frequency of an alternating current is measured in hertz, which is how many oscillations occur within a second. If you are familiar with music, it is similar to a guitar string vibrating; the amount of times the string moves up and down after it is plucked constitutes the note's frequency, for instance the standard tuning note A being 440 hertz.</p>
<h2>Voltage</h2>
<p>Voltage (<code>E</code>) is the electrical pressure, or difference in electric potential, that causes electrons to flow. It is measured in volts (<code>V</code>). The higher the voltage, the more electrical pressure is present. This is also known as the electric potential.</p>
<h2>Resistance</h2>
<p>Resistance (<code>R</code>) is opposition to electric current (direct and alternating). It is measured in ohms (<code>Ω</code>).</p>
<p>Take a typical piece of electrical wiring. We will find two parts composing the wire:</p>
<ol>
<li>The <strong>conductor</strong>, or wire (usually copper), and</li>
<li>The <strong>insulator</strong>, or plastic that surrounds the wire.</li>
</ol>
<p>The conductor has very <em>low</em> resistance as to facilitate the transfer of electricity from end to end; this is because they have many free electrons to allow the current. The insulator has very <em>high</em> resistance as to protect the electricity from flowing where it shouldn't be flowing. This resistance to the flow of electricity results in the generation of heat.</p>
<p>A <strong>resistor</strong> is a component that provides a <em>specific amount</em> of resistance, <em>opposing</em> the flow of the current. Where insulators are trying to stop electrical flow completely, resistors are trying to limit it in a controlled way. The symbol in a schematic looks close to, but not exactly, <code>-\/\/\-</code>.</p>
<p>A variable resistor, or potentiometer, can change resistance based on the rotation of a knob or a slide switch (like a guitar's volume knob or stereo's EQ slider, respectively).</p>
<h3>Ohm's Law</h3>
<p>An ohm is the resistance of a circuit in which 1 ampere current flows when 1 volt is applied: <code>E = I × R</code></p>
<p>Where <code>E</code> is voltage in volts, <code>I</code> is current in amperes, and <code>R</code> is resistance in ohms. Because of this, the following are also true:</p>
<p><code>R = E / I</code></p>
<p><code>I = E / R</code></p>
<h2>Capacitance</h2>
<p>Capacitance (<code>C</code>) is the ability to store energy in an <strong>electric</strong> field, and is measured in farads (<code>F</code>).</p>
<p>A capacitor is a component that can store energy and can be used to smooth out voltage over time. This is done by its two opposing plates, one that is being charged (positively or negatively), and a non-conductive region that is between them. Since one side is being charged, this causes the other side to become oppositely charged, creating an electrical field between them. The potency of this electrical field is measured in farads (<code>F</code>), with one farad meaning that one &quot;coulomb&quot; of charge on each conductor will cause a voltage of one volt across the device.[3-5]</p>
<p>A variable capacitor allows the capacitance to be changed by moving the conducting plates closer or farther away from each other.</p>
<p><strong>WARNING!</strong> A capacitor can store power over a long period of time, even after being turned off, and thus must be handled with great care. The stored energy can hurt you; most notably, this can occur in old CRT TV's or computer monitors.</p>
<p>Reading a circuit with an ohmmeter, you can tell if there is a large discharged capacitor by an increasing resistance with time. This is because the capacitor is slowly charging, equalizing its own voltage with that of the ohmmeter's input.</p>
<h2>Inductance</h2>
<p>Inductance (<code>L</code>) is the ability to store energy in a <strong>magnetic</strong> field. It is measured in henries (<code>H</code>).</p>
<p>An inductor is a component that stores enerrgy in an electric field. Commonly used is a coil of wire, or in audio an electromagnet for a speaker. The variation of current makes the speaker move forward and back to create sound waves.</p>
<p>A variable inductor allows changing of inductance by variation of the core placement within a coil.</p>
<p>A transformer contains two inductors with mutual inductance, which means they share the same magnetic field. If both sides of a transformer contain different amounts of loops, then the resulting voltage will change. Most common is <em>step-down</em> converters used in wall-warts for converting high voltage wall power (120 volts) into a suitable amount for a small appliance or device (e.g. 9 volts for guitar pedals).</p>
<h2>Reactance and Impedance</h2>
<p>Reactance (<code>X</code>) is opposition to the flow of alternating current (AC) due to capacitance and inductance. It is measured in ohms (Ω).</p>
<p>Impedance (<code>Z</code>) is the combined opposition to the flow of alternating current from resistance and reactance. It is also measured in ohms.</p>
<h2>Energy and Power</h2>
<p>Energy is the capacity for doing work, whereas electrical power (<code>P</code>) is the rate at which the work is done, or the rate at which energy is used. The energy is the potential that <em>could</em> be used, but the power is what <em>is</em> being used. Electrical power is rated in watts (<code>W</code>).</p>
<p>The power formula is &quot;easy as PIE&quot;: <code>P = I × E</code>, where <code>P</code> is power, <code>I</code> is current in amperes, and <code>E</code> is voltage. Therefore the following are also true: <code>I = P / E</code>, and <code>E = P / I</code>.</p>
<h3>Power Supplies</h3>
<p>Power supplies and wall warts convert electrical energy from one form to another; usually from alternating current found in the wall into direct current that is similar to what is provided by batteries. They <strong>must</strong> be the correct voltage. Power supplies are also never 100% efficient, so get a power supply that can deliver <em>at least</em> enough power if not more.</p>
<p>Oscillators convert a direct current into an alternating current, modifying it to fit the desired frequency of the alternating current.</p>
<p>A filter allows only electrical signals of desired frequencies to pass, and blocks the rest.</p>
<h3>Batteries</h3>
<p>Batteries have their power measured in ampere-hours (<code>Ah</code>), which is a combination of current and the length of time that current can be sustained until depleted. The length that a battery can be used with a given device can be found by using the following formula with the device's current draw (<code>cd</code> in this example):</p>
<p><code>length of time = Ah / cd</code></p>
<h2>Decibel</h2>
<p>Power levels are compared using a unit called the &quot;bel&quot; (<code>B</code>), but usually the decibel (<code>dB</code>) is used, which is one tenth of a bel. A bel is a factor of 10 increase/decrease in gain.</p>
<p>When power is doubled, there is a 3dB difference (not exactly, but for all intents and purposes). When power is increased by a factor of 10, there is a 10dB difference.</p>
<h2>Analogy: Electricity as Water</h2>
<p>It is handy to have a more concrete grasp on electricity and how it flows via the analogy of water. It doesn't always offer an explanation, but it often does.</p>
<table>
<thead>
<tr>
<th>Electricity</th>
<th>Water</th>
</tr>
</thead>
<tbody>
<tr>
<td>Electrical current moving through a wire</td>
<td>Water moving through a hose</td>
</tr>
<tr>
<td>Electrical voltage creating electrical pressure to create a current</td>
<td>A faucet creating water pressure to create a water flow in the hose</td>
</tr>
<tr>
<td>Resistance to limit the electrical current</td>
<td>A kink in the hose to limit the water flow</td>
</tr>
<tr>
<td>Capacitance to regulate voltage within a circuit</td>
<td>A membrane sealed inside a hose that stretches if pressure is exerted on one side</td>
</tr>
</tbody></table><h2>Series versus Parallel</h2>
<p>Series is when current passes through a series of different components, whereas parallel is when current passes through multiple branches of different components.</p>
<pre><code>Series:

──────/\/\/───/\/\/─┐
                    │
────────────────────┘

Parallel:

     ┌─/\/\/─┐
─────┤       ├──────┐
     └─/\/\/─┘      │
────────────────────┘
</code></pre>
<p>In a series circuit, the current remains the same between both components, but the voltage may differ, depending on how the components affect it.</p>
<p>In a parallel circuit, the current is divided among the components, depending on the behavior of the components, but the voltage remains the same.</p>
<p>(Note that these are not necessarily to be thought of as inverse rules, as they are not the same thing being measured. In both instances, the current going through both components is the same, but the <em>amount of current present</em> will differ from what it initially received before reaching the circuit. The voltages in both instances are <strong>not</strong> the same.)</p>
<p>If the current is the water going through a hose, you can imagine that if it is going through a <strong>series</strong> of kinks in the hose (resistors), the same amount of water will flow through each kink, but the pressure may be different in each one. If it is going through a <strong>parallel</strong> set of kinks, then the amount of water going through each kink will be less, but the pressure will be the same in both kinks.</p>
<h2>Short and Open Circuits</h2>
<p>A short circuit is where a connection is made where it shouldn't be made. This can result in excess current, blown fuses, fires, exploding stuff. It is bad! Using a fuse or circuit breaker is a common way to protect from power overload via short circuits.</p>
<p>An open circuit is where a circuit is not complete, and so current cannot flow through.</p>
<h2>Equipment</h2>
<h3>Schematic Diagrams</h3>
<p>A schematic diagram is a way to show how different components of a circuit are connected and how the current should flow between these components. The map is not the territory, in that the diagram is only a representation and will not look like the actual circuit. There are standard symbols for common components[2].</p>
<h3>Meter</h3>
<p>There are different meters for each of the electrical quantities:</p>
<ul>
<li>Ammeter for amperes</li>
<li>Voltmeter for voltage</li>
<li>Ohmmeter for resistance</li>
</ul>
<p>And a multimeter can measure all of these on a single device.</p>
<p><strong>WARNING!</strong> When using these devices, be <strong>sure</strong> that the correct voltage settings are set, or else you may fry your device by allowing too much voltage through, and never measure resistance when the device is powered. If you use the wrong unit of measurement, this can also cause damage, like using the resistance setting when measuring voltage.</p>
<h4>Ammeter</h4>
<p>When connecting an ammeter, ensure it is connected in series so that it will get the actual current. If connected in parallel, you are not guaranteed the actual current, since it is going to two different places and will thus be less.</p>
<h4>Voltmeter</h4>
<p>When connecting a voltmeter, ensure it is connected in parallel so that the voltage measured is what is actually present. If measured in series, the voltage passing through a component can affect the reading on either side, so must go in parallel to ensure the voltage read is actually what is going through the component.</p>
<h3>Switches</h3>
<p>A switch is a component that has an intentional open circuit. Starting from the <strong>pole</strong> and moving to the <strong>throw</strong>, when the circuit is completed through the flipping of the switch, the current can flow from the pole to the throw.</p>
<p>The most common switch is a single pole single throw, where the throw can either be an open circuit or a closed one. Another common switch is a single pole double throw, where the current can be routed between two different outputs.</p>
<h3>Fuses and Breakers</h3>
<p>A fuse is a device that prevents overloading of a short circuit by burning out and closing the circuit before current can become so much that it would cause further problems. Fuses must use the correct amp rating to ensure that a proper maximum can be maintained and further current will be refused, causing the fuse to fail (correctly). Fuses are meant to be destroyed by the excess electricity and are disposable and meant to be replaced.</p>
<p>A breaker is essentially a fuse that can be reset and reused repeatedly. This is the circuit breaker that most people have in their modern houses. Breakers should be installed <strong>in series</strong> with all <strong>hot</strong> wires, so that it will trip when the current exceeds a specified value.</p>
<h3>Household Wiring</h3>
<p>The National Electrical Code standardized these colors for household wiring:</p>
<ul>
<li>Hot: black/red/bare</li>
<li>Neutral: white/gray</li>
<li>Ground: green/bare</li>
</ul>
<h3>Amplifier</h3>
<p>An amplifier increases the strength of an electronic signal. The amount of increase that is measured is called &quot;gain&quot;. This is usually the final stage in your receiver before it goes to your ears or the computer.</p>
<p>Some amplifiers have a &quot;SSB/CW-FM&quot; switch that allows for the amplifier to properly operate in the given mode.  Without this, it may exhibit weird or unwanted behaviors.</p>
<h2>References</h2>
<ol>
<li><a href="https://www.hamradiolicenseexam.com/study.jsp" target="_blank">https://www.hamradiolicenseexam.com/study.jsp</a></li>
<li><a href="https://en.wikipedia.org/wiki/Electronic_symbol" target="_blank">https://en.wikipedia.org/wiki/Electronic_symbol</a></li>
<li><a href="https://en.wikipedia.org/wiki/Capacitor" target="_blank">https://en.wikipedia.org/wiki/Capacitor</a></li>
<li><a href="https://yewtu.be/watch?v=3L0E9fGI3ag" target="_blank">https://yewtu.be/watch?v=3L0E9fGI3ag</a></li>
<li><a href="https://yewtu.be/watch?v=jOHRnZsbj40" target="_blank">https://yewtu.be/watch?v=jOHRnZsbj40</a></li>
<li><a href="https://www.fcc.gov/wireless/bureau-divisions/mobility-division/amateur-radio-service/examinations" target="_blank">https://www.fcc.gov/wireless/bureau-divisions/mobility-division/amateur-radio-service/examinations</a></li>
</ol>
<section id="incoming"><details open><summary>Incoming Links</summary><ul><li><a href="antennas.html">Antennas</a></li><li><a href="electrical-components.html">Electrical Components</a></li><li><a href="ilc-circuits.html">iLC Circuits</a></li></ul></details></section><p class="last-modified">Last modified: 202212070107</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>Built using <a href="http://codeberg.org/milofultz/swiki" rel="noopener">{{SWIKI}}</a></li><li><a href="http://codeberg.org/milofultz/" rel="noopener">Codeberg</a></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener">Mastodon</a></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a><a href="https://fediring.net/">Fediring</a><a href="https://fediring.net/random" style="color: red;text-decoration: green wavy underline;">&nbsp;!&nbsp;</a><a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
