#!/bin/bash

random_file="$(echo "$(find . -type f -maxdepth 1 -name '*.html')" | sort -R | head -n1)"
extension="${random_file##*.}"

if [[ "$extension" == "html" ]]; then
	echo 'Content-Type: text/html'
else
	echo 'Content-Type: text/plain'
fi
echo ''
cat "$random_file"

