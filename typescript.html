<!doctype html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><meta http-equiv="X-UA-Compatible" content="ie=edge"><meta name="description" content="Typescript is a superset of Javascript that is strongly typed and values specificity."><!-- Bing --><meta name="msvalidate.01" content="45CBBE1BD8265A2217DFDA630EB8F84A" /><title>Tiny Brain Fans - Typescript</title><link rel="stylesheet" href="tinystyle.css"><link rel="stylesheet" href="prism.css"></head><body class="theme">
<main id="main"><article id="content"><h1 id="title">Typescript</h1><p>Typescript is a superset of <a href="javascript.html">Javascript</a> that is strongly typed and values specificity.</p>
<h2>Casting</h2>
<p>You can cast one type as another in a couple different ways.</p>
<pre><code class="language-tsx">// In these examples, query selector will naturally return an HTMLElement. To
// access the `value`, we will need to cast it as an HTMLInputElement.

let input;

// using &#x27;as&#x27;
input = document.querySelector(&#x27;input[type=&quot;text&quot;]&#x27;) as HTMLInputElement;

// using &lt;&gt;
input = &lt;HTMLInputElement&gt;document.querySelector(&#x27;input[type=&quot;text&quot;]&#x27;);
</code></pre>
<p>When using TypeScript with JSX, only <code>as</code>-style assertions are allowed.</p>
<h3>Non-null Assertion Operator</h3>
<p>The bang (<code>!</code>) in Typescript, used preceding a property, is meant to tell the compiler that this value <strong>cannot</strong> be <code>null</code> or <code>undefined</code>[15], so don't complain that it <em>could</em> be.</p>
<blockquote>
<p>A new ! post-fix expression operator may be used to assert that its operand is non-null and non-undefined in contexts where the type checker is unable to conclude that fact. Specifically, the operation x! produces a value of the type of x with null and undefined excluded. Similar to type assertions of the forms <T>x and x as T, the ! non-null assertion operator is simply removed in the emitted JavaScript code.[16]</p>
</blockquote>
<blockquote>
<p>The operation a! produces a value of the type of a with null and undefined excluded.[18]</p>
</blockquote>
<pre><code class="language-typescript">// Compiled with --strictNullChecks
function validateEntity(e?: Entity) {
    // Throw exception if e is null or invalid entity
}
function processEntity(e?: Entity) {
    validateEntity(e);
    let s = e!.name; // Assert that e is non-null and access name
}
</code></pre>
<h2>Type Annotations</h2>
<p>Type annotations are done at the declaration of a variable with a colon followed by the type or interface.</p>
<pre><code class="language-tsx">// Primitives
const greeting: string = &quot;Hello there!&quot;;

// Arrays
const numbers: number[] = [1, 2, 3, 4];

// Generics
const uniqueCharacters: Set&lt;string&gt; = new Set([&#x27;a&#x27;, &#x27;b&#x27;, &#x27;c&#x27;]);

// Interfaces/Types
interface Person {
  name: string,
  age: number,
  sayHi: () =&gt; string,
}
const johnSmith: Person = {
  name: &quot;John Smith&quot;,
  age: 40,
  sayHi: () =&gt; return greeting;
};

// Functions
//           Param Types            Output type
const add = (a: number, b: number): number =&gt; {
  return a + b;
};
</code></pre>
<h3>Annotations with Object Destructuring</h3>
<pre><code class="language-tsx">// without annotation
const getNameAndAge = ({ person, job }) =&gt; {
    const { name, age } = person;
    return `${name}: ${age}, ${job}`;
};

// with annotation
const getNameAndAge = ({
    person,
    job,
}: {
    person: { name: string; age: number };
    job: string;
}) =&gt; {
    const { name: string, age: number } = person;
    return `${name}: ${age}, ${job}`;
};

// with type or interface
type Person = {
    name: string;
    age: number;
};

const getNameAndAge = ({ person: Person, job: string }) =&gt; {
    const { name: string, age: number } = person;
    return `${name}: ${age}, ${job}`;
};
</code></pre>
<h3>Using <code>any</code></h3>
<p>In general, you want to avoid using <code>any</code> as a type. <a href="eslint.html">ESLint</a> has it <a href="https://github.com/typescript-eslint/typescript-eslint/blob/master/packages/eslint-plugin/docs/rules/no-explicit-any.md" target="_blank">disallowed by default, since it defeats the purpose of Typescript altogether</a>. But what do you do if you actually don't know what data you will be receiving?</p>
<p>The issue is that using <code>any</code> will allow a false sense of security with TypeScript, as <code>any</code> types will allow compilation but sometimes will fail in the actual execution of the application, which is exactly what we <em>don't</em> want.</p>
<p>The solution here is to use the <code>unknown</code> type. This will <em>not</em> allow compilation and will enforce the strict typing that makes TypeScript what it is and <strong>not</strong> <a href="javascript.html">JavaScript</a>.</p>
<p><em>NOTE: My personal opinion is that you should always strive to find out what your data is that is going through your system, and if a type or interface does not exist for your own objects or with an external library you are using, one should be created to accommodate. Though I understand this is not always possible, it should be strived for.</em></p>
<h3>Typing <a href="react.html">React</a></h3>
<h4>onChange Events</h4>
<p>For React onChange handlers, you can use the <code>React.ChangeEvent&lt;&gt;</code> generic, filling it in with whatever type of element is being changed.</p>
<pre><code class="language-jsx">&lt;input
    onChange={(event: React.ChangeEvent&lt;HTMLInputElement&gt;) =&gt; {
        handleEmail(event);
        handleEmailValidation();
    }}
    placeholder=&quot;Enter Email&quot;
    type=&quot;email&quot;
/&gt;
</code></pre>
<h3>Unique Typing Troubleshooting</h3>
<h4><code>Property &#x27;***&#x27; does not exist on type &#x27;never&#x27;</code></h4>
<p>This means that the target variable has not been typed correctly and often will show up when something has not been given a type at all.</p>
<pre><code class="language-tsx">// incorrect
const thing = &quot;Oh yeah!&quot;;
const things = [1, 2, 3, 4];
const specificThing = { name: &quot;John&quot;, age: 9 };
// correct
const thing: string = &quot;Oh yeah!&quot;;
const things: number[] = [1, 2, 3, 4];
const specificThing: InterfaceName = { name: &quot;John&quot;, age: 9 };
</code></pre>
<h4>useState</h4>
<p>With <code>useState</code> in <a href="react.html">React</a> hooks, you will need to use a special syntax in creation of the hooks themselves. Since useState is a <a href="https://www.typescriptlang.org/docs/handbook/2/generics.html" target="_blank">generic function</a>, the typing happens within the <code>&lt;&gt;</code> characters.</p>
<pre><code class="language-tsx">type HashtagListener {
  ...
}

const [editedHashtagListeners, setEditedHashtagListeners] = useState&lt;HashtagListener[]&gt;([]);
</code></pre>
<h2>Interface and Type</h2>
<p>Interfaces and types are two ways to define a type of object, declaring the types of the contained properties.</p>
<h3>Differences</h3>
<h4>Type</h4>
<pre><code class="language-tsx">// Basic objects

type Person = {
    name: string;
    age: number;
    pets: string[];
};

const john: Person = {
    name: &quot;John&quot;,
    age: 40,
    pets: [&quot;Mary&quot;, &quot;Doug&quot;],
};

const household: Person[] = [john];

// Using list of specific keys

type AllowedKeys = &quot;name&quot; | &quot;age&quot;;

type Person = Record&lt;AllowedKeys, unknown&gt;;

const Human: Person = {
    name: &quot;Steve&quot;,
    age: 42,
};

// Using mapped objects[17]

type Language = &quot;en&quot; | &quot;es&quot;;
type WeekStartDays = 0 | 1 | 2 | 3 | 4 | 5 | 6;

type Translation&lt;Type&gt; = {
    [Property in Language]: Type;
};

type MessageTranslations = Translation&lt;string&gt;;
// Equivalent to:
// type MessageTranslations = {
//   en: string;
//   es: string;
// }

type WeekStartDayTranslations = Translation&lt;WeekStartDays&gt;;
// Equivalent to:
// type WeekStartDayTranslations = {
//   en: WeekStartDays;
//   es: WeekStartDays;
// }
</code></pre>
<h4>Interface</h4>
<pre><code class="language-tsx">interface Person {
    name: string;
    age: number;
    pets: string[];
}

const john: Person = {
    name: &quot;John&quot;,
    age: 40,
    pets: [&quot;Mary&quot;, &quot;Doug&quot;],
};

const household: Person[] = [john];
</code></pre>
<h3>Creating Fixed Values</h3>
<pre><code class="language-tsx">type Roles = &quot;owner&quot; | &quot;admin&quot;;

interface User {
    name: string;
    // value must be an array containing values within Roles
    roles: Roles[];
}
</code></pre>
<h3>Defining Unknown Property Keys</h3>
<pre><code class="language-tsx">type Roles = &quot;owner&quot; | &quot;admin&quot;;

interface User {
    name: string;
    channels: {
        // channel at `index` must be a string
        // value must be an array containing values within Roles
        [index: string]: Roles[];
    };
}
</code></pre>
<h3>Defining Types in External Packages[19, 20]</h3>
<p>If you are using a third-party package that either has no typings or has typings you want to change, you can create a <code>types</code> folder and add an &quot;ambient module&quot;. This defines the <em>shape</em> of the data and not the functionality.</p>
<p>Let's say you have a module called <code>simple-package</code> that you want to add typings to. It outputs a function that creates an object that holds two numbers.</p>
<pre><code class="language-typescript">declare module &quot;simple-package&quot; {
    export interface SimplePackageNumbers {
        first: number;
        second: number;
    }

    export function SimplePackage(): SimplePackageNumbers;
}
</code></pre>
<h2>References:</h2>
<ol>
<li><a href="https://stackoverflow.com/questions/52423842/what-is-not-assignable-to-parameter-of-type-never-error-in-typescript" target="_blank">https://stackoverflow.com/questions/52423842/what-is-not-assignable-to-parameter-of-type-never-error-in-typescript</a></li>
<li><a href="https://stackoverflow.com/questions/53598449/react-hooks-and-typescript-property-does-not-exist-on-type-never" target="_blank">https://stackoverflow.com/questions/53598449/react-hooks-and-typescript-property-does-not-exist-on-type-never</a></li>
<li><a href="https://stackoverflow.com/questions/41443242/how-to-correct-flow-warning-destructuring-missing-annotation" target="_blank">https://stackoverflow.com/questions/41443242/how-to-correct-flow-warning-destructuring-missing-annotation</a></li>
<li><a href="https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces" target="_blank">https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces</a></li>
<li><a href="https://lzomedia.com/blog/how-to-apply-type-annotations-to-functions-in-typescript/" target="_blank">https://lzomedia.com/blog/how-to-apply-type-annotations-to-functions-in-typescript/</a></li>
<li><a href="https://stackoverflow.com/questions/12989741/the-property-value-does-not-exist-on-value-of-type-htmlelement" target="_blank">https://stackoverflow.com/questions/12989741/the-property-value-does-not-exist-on-value-of-type-htmlelement</a></li>
<li><a href="https://stackoverflow.com/questions/61851004/describe-interface-fixed-values-in-array-element-of-typescript" target="_blank">https://stackoverflow.com/questions/61851004/describe-interface-fixed-values-in-array-element-of-typescript</a></li>
<li><a href="https://stackoverflow.com/questions/23914271/typescript-interface-definition-with-an-unknown-property-key" target="_blank">https://stackoverflow.com/questions/23914271/typescript-interface-definition-with-an-unknown-property-key</a></li>
<li><a href="https://basarat.gitbook.io/typescript/type-system" target="_blank">https://basarat.gitbook.io/typescript/type-system</a></li>
<li><a href="https://dev.to/mattzgg_94/get-started-with-using-typescript-and-tdd-to-solve-leetcode-problems-in-vs-code-26d" target="_blank">https://dev.to/mattzgg_94/get-started-with-using-typescript-and-tdd-to-solve-leetcode-problems-in-vs-code-26d</a></li>
<li><a href="https://www.udemy.com/course/understanding-typescript/" target="_blank">https://www.udemy.com/course/understanding-typescript/</a></li>
<li><a href="https://stackoverflow.com/questions/33256274/typesafe-select-onchange-event-using-reactjs-and-typescript" target="_blank">https://stackoverflow.com/questions/33256274/typesafe-select-onchange-event-using-reactjs-and-typescript</a></li>
<li><a href="https://www.cstrnt.dev/blog/three-typescript-tricks" target="_blank">https://www.cstrnt.dev/blog/three-typescript-tricks</a></li>
<li><a href="https://github.com/typescript-cheatsheets/react" target="_blank">Cheatsheet for Typescript with React</a></li>
<li><a href="https://stackoverflow.com/questions/42273853/in-typescript-what-is-the-exclamation-mark-bang-operator-when-dereferenci/42274019#42274019" target="_blank">Non-null assertion operator</a></li>
<li><a href="https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-0.html#non-null-assertion-operator" target="_blank">Typescript notes on the non-null assertion operator</a></li>
<li><a href="https://www.typescriptlang.org/docs/handbook/2/mapped-types.html" target="_blank">TypeScript: Documentation - Mapped Type</a></li>
<li><a href="https://stackoverflow.com/a/40238939/14857724" target="_blank">typescript - Safe navigation operator (?.) or (!.) and null property paths - Stack Overflow</a></li>
<li><a href="https://alexzywiak.github.io/writing-typescript-typings-files-for-third-party-modules/index.html" target="_blank">Writing Typescript Typings Files for Third Party Module</a></li>
<li><a href="https://www.mourtada.se/typescript-ambient-module-declarations/" target="_blank">Typescript ambient module declarations | Mourtada.se</a></li>
</ol>
<section id="incoming"><details open><summary>Incoming Links</summary><ul><li><a href="eslint.html">ESLint</a></li><li><a href="frameworks-javascript.html">Frameworks (Javascript)</a></li><li><a href="make-a-rule-eslint.html">Make a Rule (ESLint)</a></li><li><a href="typing.html">Typing</a></li><li><a href="vue.html">Vue</a></li></ul></details></section><p class="last-modified">Last modified: 202306170048</p></article></main><footer><nav><p><a href="index.html">Home</a></p><ul><li>Built using <a href="http://codeberg.org/milofultz/swiki" rel="noopener">{{SWIKI}}</a></li><li><a href="http://codeberg.org/milofultz/" rel="noopener">Codeberg</a></li><li><a href="http://milofultz.com/" rel="noopener">milofultz.com</a></li><li><a href="https://merveilles.town/@milofultz" rel="me noopener">Mastodon</a></li><li><a href="https://fediring.net/previous?host=milofultz.com">←</a><a href="https://fediring.net/">Fediring</a><a href="https://fediring.net/random" style="color: red;text-decoration: green wavy underline;">&nbsp;!&nbsp;</a><a href="https://fediring.net/next?host=milofultz.com">→</a></li></ul></nav></footer><script src="prism.js"></script></body></html>
